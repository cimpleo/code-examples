//auxiliary variable for saving articles
var counter_for_crud_operations = {};
counter_for_crud_operations.value = 0; 

function creteUniqueFileName(fileServerPath) {

  var fileServerPath = fileServerPath.split('/');
  var fileFullName = fileServerPath[fileServerPath.length - 1];            
  var nameAndExtensionCurrentFile = fileFullName.split('.');
  var fileExtension = nameAndExtensionCurrentFile[nameAndExtensionCurrentFile.length - 1];           
  var uniqueFileName = fileFullName.replace("." + fileExtension, "") + "." + (new Date().getTime()).toString() + "." + fileExtension;
            
  return uniqueFileName;

}

function getFileName(pathToFile) {          
  pathToFile = pathToFile.split('/');          
  return pathToFile[pathToFile.length - 1].replace('"', '');
};

function deleteFile(pathToFile, fileDeletePromise) {
  
  if (pathToFile == "img/default-pic") {
    fileDeletePromise.resolve();
  } else {

    var fileName = getFileName(pathToFile);
              
    var platform_path;

    if (device.platform != "Android") {
      platform_path = cordova.file.documentsDirectory;
    } else {
      platform_path = cordova.file.externalDataDirectory;
    }
              
    window.resolveLocalFileSystemURL(platform_path, function (dir) {
      dir.getFile(fileName, {create: false}, function (fileEntry) {
        fileEntry.remove(function (file) {
          fileDeletePromise.resolve();
        }, function () {                    
          //ignoring the error deleting files necessary for IOS devices from version 9.0, 
          //because there's delete function with the successful removal of the file still returns an error
          fileDeletePromise.resolve();
        }, function () {            
          fileDeletePromise.resolve();
        });
      }, function () {          
        fileDeletePromise.resolve();
      });
    });
  }
} //END deleteFile()
          
          
          