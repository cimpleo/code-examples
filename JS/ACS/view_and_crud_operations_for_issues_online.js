function hide_and_clear_progress_bar() {
    $('#crud-overlay-progress-bar').css({display: 'none'});
    $('#crud-overlay-progress-bar #progress-bar-marker').css({display: 'block'});
    $('#crud-overlay-progress-bar #progress-bar-marker').css({width: 250});
    $('#crud-overlay-progress-bar #progress-bar-count span').text('0');
};

function view_and_crud_operations_for_issues_online() {

    //Saving / deleting issue on click
    $('.panel-right').on('click', '.saving-functionality-ready', function () {
        $('body').css({overflow: 'hidden'});
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(450);

        //deleting issue
        if ($(this).hasClass('on')) {
            $(this).removeClass('on').addClass('off');
            $('.panel-right .panel-header').text('Save for offline');
            $('#toggle').click();

            setTimeout(function () {
                $('#crud-overlay, #crud-overlay #app-prelouder img, #crud-overlay #app-prelouder').css({display: 'block'});
            }, 250);

            //searching issue
            var all_saved_issues = JSON.parse(window.localStorage.getItem("saved_issues"));
            jQuery.each(all_saved_issues.issues, function (index, value) {
                if (window.localStorage.getItem("current-issue-label-id") == value.label_id) {

                    //Save iteration to remove
                    window.localStorage.setItem("delete-iter-for-issue", index);
                    //The path to the removable issue pictures also write
                    window.localStorage.setItem("issue-img-deleting", JSON.stringify(all_saved_issues.issues[index].picture));
                    //First, we get a list of articles to remove their files
                    window.localStorage.setItem("data_entries_for_deleting", JSON.stringify(all_saved_issues.issues[index].entries));
                    return false
                }
            });
            //END Check the current issue


            if (window.localStorage.getItem("issue-img-deleting") == '"img/default-img.jpg"') {
                delete_all_articles_files();
            } else {
                //Get the name of the file
                var file_name = getFileName(window.localStorage.getItem("issue-img-deleting"));

                var platform_path;

                if (device.platform != "Android") {
                    platform_path = cordova.file.documentsDirectory;
                } else {
                    platform_path = cordova.file.externalDataDirectory;
                }

                window.resolveLocalFileSystemURL(platform_path, function (dir) {

                    dir.getFile(file_name, {create: false}, function (fileEntry) {
                        fileEntry.remove(function (file) {
                            delete_all_articles_files();
                        }, function () {
                            //ignoring the error deleting files necessary for IOS devices from version 9.0,
                            //because there's delete function with the successful removal of the file still returns an error
                            delete_all_articles_files();
                        }, function () {
                            delete_all_articles_files();
                        });
                    }, function () {
                        delete_all_articles_files();
                    });
                });

            }


            function delete_all_articles_files() {

                //get the list of articles
                var all_deleting_articles = JSON.parse(window.localStorage.getItem("data_entries_for_deleting"));
                //Measure the length of deleting articles array
                var article_count = all_deleting_articles.length;

                if (article_count !== 0) {

                    deleteCurrentArticleFiles();

                    function deleteCurrentArticleFiles() {

                        var current_deleting_article_index = counter_for_crud_operations.value;

                        var articlePicturePath = all_deleting_articles[current_deleting_article_index].picture;
                        var articlePdfPath = all_deleting_articles[current_deleting_article_index].pdf;

                        //Create two promises
                        var articlePictureIsDeleted = new $.Deferred();
                        var articlePdfIsDeleted = new $.Deferred();

                        //Delete current article files
                        //func deleteFile() from auxiliary_functions.js
                        deleteFile(all_deleting_articles[current_deleting_article_index].picture, articlePictureIsDeleted);

                        $.when(articlePictureIsDeleted).then(function () {
                            // Current article picture deleted
                            deleteFile(all_deleting_articles[current_deleting_article_index].pdf, articlePdfIsDeleted);
                        }, function () {
                            //ignoring the error deleting files necessary for IOS devices from version 9.0,
                            //because there's delete function with the successful removal of the file still returns an error
                            deleteFile(all_deleting_articles[current_deleting_article_index].pdf, articlePdfIsDeleted);
                        });

                        $.when(articlePdfIsDeleted).then(function () {
                            // Current article files deleted
                            check_the_number_of_deleted_articles_files();
                        }, function () {
                            //ignoring the error deleting files necessary for IOS devices from version 9.0,
                            //because there's delete function with the successful removal of the file still returns an error
                            check_the_number_of_deleted_articles_files();
                        });

                    } //END deleteCurrentArticleFiles()

                    function check_the_number_of_deleted_articles_files() {

                        counter_for_crud_operations.value++;

                        if (counter_for_crud_operations.value !== (all_deleting_articles.length)) {
                            deleteCurrentArticleFiles();
                        } else {
                            delete_issue_complit();
                        }

                    } //END check_the_number_of_deleted_articles_files()

                } else {
                    delete_issue_complit();
                }
            } //END delete_all_articles_files()

            function delete_issue_complit() {

                counter_for_crud_operations.value = 0;

                var all_saved_issues = JSON.parse(window.localStorage.getItem("saved_issues"));
                all_saved_issues.issues.splice(window.localStorage.getItem("delete-iter-for-issue"), 1);
                window.localStorage.setItem('saved_issues', JSON.stringify(all_saved_issues));

                //clear memory
                all_articles_for_delete = null;
                var article_count = null;
                all_saved_issues = null;

                setTimeout(function () {
                    $('body').css({overflow: 'visible'});
                    //$('#crud-overlay #crud-text').text('Issue removed');
                    setTimeout(function () {
                        $('#crud-overlay, #crud-overlay #app-prelouder img, #crud-overlay #app-prelouder').css({display: 'none'});
                    }, 1);
                    //alert("Issue removed");
                }, 450);
            } //END delete_issue_complit()

            return
        }
        //END deleting issue

        //Saving issue
        var connection = navigator.connection.type;

        if (connection == "none") {
            alert("You are offline now, saving this issue is now impossible.");
            return false;
        }

        $(this).removeClass('off').addClass('on');
        $('.panel-right .panel-header').text('Delete issue');
        $('#toggle').click();


        $('#crud-overlay-progress-bar #progress-bar-marker').css({display: 'block'});

        setTimeout(function () {
            $('#crud-overlay-progress-bar').css({display: 'block'});
        }, 450);

        $(save_issue_for_offline());

        function save_issue_for_offline() {

            //the path to the issue image on the server
            var issue_picture = $(".content>.detail-wrap").attr("data-issue-picture");

            //create an object to store the journal issue
            //create object "issue Info" to save the new issue of the magazine
            var issueInfo = {};
            issueInfo.label_id = window.localStorage.getItem("current-issue-label-id");
            issueInfo.issue_local_name = $(".content>.detail-wrap").attr("data-issue-local-name");
            issueInfo.title = $(".content>.detail-wrap").attr("data-issue-title");
            issueInfo.url = $(".content>.detail-wrap").attr("data-issue-url");
            issueInfo.date = $(".content>.detail-wrap").attr("data-issue-date");
            issueInfo.picture = "";
            issueInfo.server_picture = issue_picture;
            issueInfo.prev_issue = $(".content>.detail-wrap").attr("data-issue-prev");
            issueInfo.next_issue = $(".content>.detail-wrap").attr("data-issue-next");

            //Writing in the journal are now stored localstorage release facility
            localStorage.setItem('saving-issue-now', JSON.stringify(issueInfo));

            //Then create a temporary variable to create an array of entries [] to issueObject,
            //in entries [] are making all the necessary data on the release of the articles
            window.localStorage.setItem("issue-entries", '{"entries":[]}');

            //if the article has default picture, then
            if (issue_picture == "img/default-img.jpg") {

                picture_device_path = "img/default-img.jpg";

                var issueInfo = JSON.parse(window.localStorage.getItem("saving-issue-now"));

                issueInfo.picture = picture_device_path;

                localStorage.setItem('saving-issue-now', JSON.stringify(issueInfo));

                window.localStorage.setItem("save_research_articles_flag", 'no');
                //Call the function of saving all the articles of issue
                save_all_issue_articles();

                //if the picture is not the default 'article, the swing image from the server and store its path
                //in variable picture_device_path
            } else {
                //picture id unix-time-stamp
                var date_img_id = (new Date().getTime()).toString();

                //break address on "/" get an array of elements
                var absolute_img_path = issue_picture;

                absolute_img_path = absolute_img_path.split('/')

                //get the name and file extension
                var img_full_name = absolute_img_path[absolute_img_path.length - 1];

                //split the file name and extension for the "." We obtain an array of 2 elements
                var two_parts_of_img_name = img_full_name.split('.');

                //get the last element in the array - it's just a file extension
                var img_file_extension = two_parts_of_img_name[two_parts_of_img_name.length - 1];

                //get the file name without the last point, removing the file extension and the point
                var img_file_name = absolute_img_path[absolute_img_path.length - 1];
                img_file_name = img_file_name.replace("." + img_file_extension, "");

                //collect piecemeal full filename
                var file_name = img_file_name + "." + date_img_id + "." + img_file_extension;

                var platform_path;

                if (device.platform != "Android") {
                    platform_path = cordova.file.documentsDirectory.replace("file://", "");
                } else {
                    platform_path = cordova.file.externalDataDirectory.replace("file://", "");
                }

                //local path to the saved file on a device
                var img_device_path = platform_path + file_name;

                function stop_issue_saving() {
                    setTimeout(function () {
                        hide_and_clear_progress_bar();
                    }, 1000);

                    $(".content .detail-panel .panel-right #save-click-block").removeClass('on').addClass('off');
                    $('.panel-right .panel-header').text('Save for offline');
                    $('#toggle').click();

                    return
                }

                window.resolveLocalFileSystemURL(img_device_path, errorCallback, successCallback);

                function successCallback() {
                    var fileTransfer = new FileTransfer();

                    fileTransfer.download(issue_picture, img_device_path,
                        function (entry) {

                            //take from localstorage saving object
                            var issueInfo = JSON.parse(window.localStorage.getItem("saving-issue-now"));

                            //Save the path to the image
                            issueInfo.picture = img_device_path;

                            //Writing in the journal are now stored localstorage release facility
                            //with issues picture
                            localStorage.setItem('saving-issue-now', JSON.stringify(issueInfo));

                            //Call the function of saving all the articles of issue
                            save_all_issue_articles_with_help_cache();

                        },
                        function (err) {
                            alert("Download error , please try again later");
                            stop_issue_saving();
                        });
                }

                function errorCallback() {
                    alert("Download error , please try again later");
                    stop_issue_saving();
                }

            } // END saving issue picture

            function save_all_issue_articles_with_help_cache() {

                //Take an array of articles articles_to_load.articles
                var articles_to_load = JSON.parse(window.localStorage.getItem("articles_to_load"));
                var research_articles_to_load = JSON.parse(window.localStorage.getItem("research_articles_to_load"));
                var all_saving_articles = articles_to_load.articles.concat(research_articles_to_load.research_articles);

                //first parameter = real width progress-bar marker in html-layout (detail_page.html, '#crud-overlay-progress-bar #progress-bar-marker')
                var progressBar = new progressBar(250, all_saving_articles.length);

                articles_to_load = null;
                research_articles_to_load = null;

                if (all_saving_articles.length > 0) {

                    saveArticle();

                    function saveArticle() {

                        var current_saving_article_index = counter_for_crud_operations.value;

                        var articleInfo = {};
                        articleInfo.tag = all_saving_articles[current_saving_article_index].tag;
                        articleInfo.id = all_saving_articles[current_saving_article_index].id;
                        articleInfo.title = all_saving_articles[current_saving_article_index].title;
                        articleInfo.author = all_saving_articles[current_saving_article_index].author;
                        articleInfo.picture = "";
                        articleInfo.server_picture = "";
                        articleInfo.path_to_pdf_on_server = all_saving_articles[current_saving_article_index].pdf;
                        articleInfo.pdf = "";
                        articleInfo.pdf_active_view_link = all_saving_articles[current_saving_article_index].pdf_active_view_link;

                        //Create two promises
                        var articlePictureIsDownloaded = new $.Deferred();
                        var articlePdfIsDownloaded = new $.Deferred();

                        //Array of paths for garbage collector
                        var currentFilesPathsForGarbageCollector = {};
                        currentFilesPathsForGarbageCollector.paths = [];

                        //Download article picture
                        downloadAticleFile(all_saving_articles[current_saving_article_index].picture, articlePictureIsDownloaded, false);

                        $.when(articlePictureIsDownloaded).then(function () {
                            // Current article picture saved
                            downloadAticleFile(all_saving_articles[current_saving_article_index].pdf, articlePdfIsDownloaded, true);
                        }, function () {
                            // Current article files not saved
                            downloadAticleFileErrorHandler();
                        });

                        $.when(articlePdfIsDownloaded).then(function () {
                            // Current article files saved
                            addCurrentArticleInSavingIssueEntries(articleInfo);
                        }, function () {
                            // Current article files not saved
                            downloadAticleFileErrorHandler();
                        });

                        function downloadAticleFileErrorHandler() {
                            counter_for_crud_operations.value = 0;

                            var currentErrorHandler = new errorHandler(); //from app_error_handlers.js
                            currentErrorHandler.downloadIssueFilesErrorHandler(currentFilesPathsForGarbageCollector.paths);

                            //Cleaning temporary variables
                            window.localStorage.setItem("issue-entries", '{"entries":[]}');
                            window.localStorage.setItem("saving-issue-now", '');
                            alert("Connection error, please try again later");

                            stop_issue_saving();
                        }

                        function downloadAticleFile(fileServerPath, fileDownloadPromise, createServerHttpPathForDownlPDF_boolean) {

                            //if the article has default picture, then
                            if (fileServerPath == "img/default-img.jpg") {
                                var picture_device_path = "img/default-img.jpg";
                                articleInfo.picture = picture_device_path;
                                articleInfo.server_picture = picture_device_path;
                                fileDownloadPromise.resolve();
                            } else {
                                //Save article with the NOT default picture
                                var fileName = creteUniqueFileName(fileServerPath);

                                if (createServerHttpPathForDownlPDF_boolean) {
                                    //Remove from the url-query "https" pdf-file to download the real pdf-file
                                    fileServerPath = "http" + fileServerPath.substr(5);
                                }

                                var platform_path;

                                if (device.platform != "Android") {
                                    platform_path = cordova.file.documentsDirectory.replace("file://", "");
                                } else {
                                    platform_path = cordova.file.externalDataDirectory.replace("file://", "");
                                }

                                //local path to saving a file on a device
                                var file_device_path = platform_path + fileName;

                                if (createServerHttpPathForDownlPDF_boolean) {
                                    file_device_path = file_device_path + ".pdf";
                                }

                                window.resolveLocalFileSystemURL(file_device_path, errorCallback, successCallback);

                                function successCallback() {

                                    var fileTransfer = new FileTransfer();
                                    fileTransfer.download(fileServerPath, file_device_path,
                                        function (entry) {

                                            if (!createServerHttpPathForDownlPDF_boolean) {
                                                articleInfo.picture = file_device_path;
                                                articleInfo.server_picture = fileServerPath;
                                            } else {
                                                articleInfo.pdf = file_device_path;
                                            }

                                            //for garbage collector in the case of article files saving error
                                            currentFilesPathsForGarbageCollector.paths.push(file_device_path);

                                            fileDownloadPromise.resolve();
                                        },
                                        function (err) {
                                            fileDownloadPromise.reject();
                                        });
                                }

                                function errorCallback() {
                                    fileDownloadPromise.reject();
                                }
                            } //END downloading current article picture
                        } //downloadAticleFile()

                        function addCurrentArticleInSavingIssueEntries(articleInfo) {

                            var issue_tmp_entries = JSON.parse(window.localStorage.getItem("issue-entries"));
                            issue_tmp_entries.entries.push(articleInfo);
                            window.localStorage.setItem('issue-entries', JSON.stringify(issue_tmp_entries));

                            //clear variables
                            articleInfo = {};
                            issue_tmp_entries = null;

                            counter_for_crud_operations.value++;
                            progressBar.show_progress(counter_for_crud_operations.value);

                            if (counter_for_crud_operations.value !== (all_saving_articles.length)) {
                                saveArticle();
                            } else {
                                save_issue_complit();
                            }

                        }//END addCurrentArticleInSavingIssueEntries(articleInfo)

                    } //END saveArticle()

                } else {
                    //if (all_saving_articles.length == 0)
                    progressBar.set_progress_bar_parameters(25, 90);
                    save_issue_complit();
                }

                function save_issue_complit() {

                    //clear article saving counter
                    counter_for_crud_operations.value = 0;

                    //Get the current object saved journal
                    var saving_issue_now = JSON.parse(window.localStorage.getItem("saving-issue-now"));
                    var issue_tmp_entries = JSON.parse(window.localStorage.getItem("issue-entries"));

                    //Adding to current object saved journal entries from the field all the articles
                    saving_issue_now.entries = issue_tmp_entries.entries;
                    //Save the temporary object "saving-issue-now"
                    localStorage.setItem('saving-issue-now', JSON.stringify(saving_issue_now));

                    //save the issue in an array of saved issues for offline
                    var all_saved_issues = JSON.parse(window.localStorage.getItem("saved_issues"));
                    all_saved_issues.issues.push(JSON.parse(window.localStorage.getItem("saving-issue-now")));
                    //issue saving
                    window.localStorage.setItem("saved_issues", JSON.stringify(all_saved_issues));

                    //Cleaning temporary variables
                    saving_issue_now = null;
                    issue_tmp_entries = null;
                    window.localStorage.setItem("issue-entries", '{"entries":[]}');
                    window.localStorage.setItem("saving-issue-now", 'null');

                    setTimeout(function () {
                        progressBar.set_progress_bar_parameters(0, 100);
                        setTimeout(function () {
                            hide_and_clear_progress_bar();
                            $('body').css({overflow: 'visible'});
                        }, 800);
                        //alert("Issue saved");
                    }, 450);

                } //END save_issue_complit()


                //Class Progress-bar
                function progressBar(progress_bar_marker_width, all_articles_count) {

                    var progress_bar_marker_width = progress_bar_marker_width;
                    var one_percent = 0;
                    var one_percent_progress_bar_marker_width = 0;
                    var percents_loaded = 0;
                    var changed_progress_bar_marker_width = 0;

                    set_one_percent(all_articles_count);
                    calculate_one_percent_progress_bar_marker_width();

                    function set_one_percent(all_articles_count) {
                        one_percent = all_articles_count / 100;
                    };

                    function calculate_one_percent_progress_bar_marker_width() {
                        one_percent_progress_bar_marker_width = progress_bar_marker_width / 100;
                    };

                    this.show_progress = function (articles_iter) {
                        percents_loaded = Math.floor(articles_iter / one_percent);
                        changed_progress_bar_marker_width = progress_bar_marker_width - (one_percent_progress_bar_marker_width * percents_loaded);

                        $('#crud-overlay-progress-bar #progress-bar-marker').css({width: '' + changed_progress_bar_marker_width + ''});
                        $('#crud-overlay-progress-bar #progress-bar-count span').text('' + percents_loaded + '');
                    };

                    this.set_progress_bar_parameters = function (progress_bar_marker_width_px, progress_bar_percents_loaded) {
                        $('#crud-overlay-progress-bar #progress-bar-marker').css({width: '' + progress_bar_marker_width_px + ''});
                        $('#crud-overlay-progress-bar #progress-bar-count span').text('' + progress_bar_percents_loaded + '');
                    };

                } // END Class Progress-bar


            } //END save_all_issue_articles()


        }; //End save_issue_for_offline()


        return
        //END Saving issue
    }); //end click

};//END view_and_crud_operations_for_issues_online()