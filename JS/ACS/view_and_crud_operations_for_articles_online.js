function view_and_crud_operations_for_articles_online() {

    //Click by notification about absence articles, button "ОК"
    $('body').on('touchstart', '#no-articles-overlay #no-articles-ok', function () {
        $('#no-articles-overlay').css({display: 'none'});
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(1000);
    });

    //Functional downloading and viewing pdf
    $('#page-main-content').on('click', '.article-item', function () {

        var connection = navigator.connection.type;

        if (connection == "none") {
            alert("You are offline now, viewing this article is now impossible in online mode.");
            return false;
        }

        var currentArticle = $(this);

        //touchstart-fix-overlay (to prevent accidental switching to another page)
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(450);

        window.localStorage.setItem("current-article-id", currentArticle.attr('data-article-id'));

        var current_article_id = window.localStorage.getItem("current-article-id");

        //obtain all saved articles
        var all_saved_articles = window.localStorage.getItem("saved_articles");
        all_saved_articles = JSON.parse(all_saved_articles);

        if (all_saved_articles.articles.length > 0) {
            jQuery.each(all_saved_articles.articles, function (i, val) {
                //if we find in "saved_articles" the "id" of the current article, then stop the cycle and offer to remove it
                if (val.id == current_article_id) {
                    //pdf viewer flag
                    window.localStorage.setItem("this-article-saved", "yes");


                    return false
                } else {
                    if ((i + 1) == all_saved_articles.articles.length) {
                        //save articles flag
                        window.localStorage.setItem("this-article-saved", "no");
                        return false
                    }
                }
            }); //end jQuery.each
        } else {
            //save articles flag
            window.localStorage.setItem("this-article-saved", "no");
        }

        //Get the server path to the PDF
        var article_pdf_url = currentArticle.attr('data-pdf-active-view-link');

        if (window.localStorage.getItem("this-article-saved") == "no") {
            //Save to local storage Other characteristics Article
            window.localStorage.setItem("current-article-tag", currentArticle.attr('data-article-tag'));
            window.localStorage.setItem("current-article-id", currentArticle.attr('data-article-id'));
            window.localStorage.setItem("current-article-title", currentArticle.attr('data-article-title'));
            window.localStorage.setItem("current-article-author", currentArticle.attr('data-article-author'));
            window.localStorage.setItem("current-article-picture", currentArticle.attr('data-article-picture'));
            window.localStorage.setItem("current-article-pdf", currentArticle.attr('data-article-pdf'));
            window.localStorage.setItem("current-article-pdf-active-view-link", currentArticle.attr('data-pdf-active-view-link'));
        }

        var pdf_active_view_link = article_pdf_url;
        view_pdf_online(pdf_active_view_link);

        function view_pdf_online(url) {

            var url = url;

            var img_path_for_crud_button_one;
            var img_path_for_crud_button_two;

            var custom_browser_header_height = 44;

            if (device.platform != "Android") {
                custom_browser_header_height = 31;
            }

            if (window.localStorage.getItem("this-article-saved") == "yes") {
                img_path_for_crud_button_one = "img/innapp_save_complit.png";
                img_path_for_crud_button_two = "img/innapp_save_complit_click.png";
            } else {
                img_path_for_crud_button_one = "img/innapp_save.png";
                img_path_for_crud_button_two = "img/innapp_save_click.png";
            }

            cordova.ThemeableBrowser.open(url, '_blank', {
                statusbar: {
                    color: '#ffffffff'
                },
                //Top panel
                toolbar: {
                    height: custom_browser_header_height,
                    color: '#333333'
                },
                backButton: {
                    wwwImage: 'img/left-pdf-arrow.png',
                    wwwImagePressed: 'img/left-pdf-arrow.png',
                    wwwImageDensity: 2,
                    align: 'left',
                    event: 'backPressed'
                },
                customButtons: [
                    {
                        wwwImage: 'img/innapp_home.png',
                        wwwImagePressed: 'img/innapp_home.png',
                        wwwImageDensity: 2,
                        align: 'left',
                        event: 'homePressed'
                    }
                ],
                closeButton: {
                    wwwImage: img_path_for_crud_button_one,
                    wwwImagePressed: img_path_for_crud_button_two,
                    wwwImageDensity: 2,
                    align: 'right',
                    event: 'savePressed'
                },
                backButtonCanClose: true
            }).addEventListener('backPressed', function (e) {
                //Back
                //touchstart-fix-overlay
                $("#touchstart-fix-overlay").fadeIn(100);
                $("#touchstart-fix-overlay").fadeOut(450);
                $('body').css({overflow: 'visible'});
            }).addEventListener('homePressed', function (e) {
                window.location.href = "home_page.html";
            }).addEventListener('savePressed', function (e) {
                $('body').css({overflow: 'hidden'});

                //touchstart-fix-overlay (to prevent accidental switching to another page)
                $("#touchstart-fix-overlay").fadeIn(450);


                if (window.localStorage.getItem("this-article-saved") == "yes") {
                    //save articles flag
                    window.localStorage.setItem("this-article-saved", "no");
                    //$('#delete-article-overlay').css({display: 'block'});
                    delete_current_article();
                } else {
                    //$('#save-article-overlay').css({display: 'block'});
                    save_current_article();
                }

            }).addEventListener(cordova.ThemeableBrowser.EVT_ERR, function (e) {
                console.error(e.message);
            }).addEventListener(cordova.ThemeableBrowser.EVT_WRN, function (e) {
                console.log(e.message);
            });
        };

    });//END Functional downloading and viewing pdf

    //Click NO SAVE #save-article-overlay
    $('body').on('touchstart', '#save-article-overlay #save_no', function () {

        $('body').css({position: 'static'});
        $('body').css({overflow: 'visible'});
        $('#save-article-overlay').css({display: 'none'});
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(1000);

    });
    //END Click NO SAVE


    function save_current_article() {

        var connection = navigator.connection.type;

        if (connection == "none") {
            alert("You are offline now, saving this article is now impossible.");
            $('#save-article-overlay').css({display: 'none'});
            $("#touchstart-fix-overlay").fadeIn(100);
            $("#touchstart-fix-overlay").fadeOut(1000);
            return
        }

        $('#save-article-overlay').css({display: 'none'});
        //$('#crud-text').text('Now article is saving, please wait...');
        $('#crud-text').css({display: 'none'});
        $('#crud-overlay, #crud-overlay #app-prelouder img, #crud-overlay #app-prelouder').css({display: 'block'});
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(1000);

        //Create two promises
        var articlePictureIsDownloaded = new $.Deferred();
        var articlePdfIsDownloaded = new $.Deferred();

        //Array of paths for garbage collector
        var currentFilesPathsForGarbageCollector = {};
        currentFilesPathsForGarbageCollector.paths = [];

        //Download article picture
        downloadAticleFile_forSingleArticle(window.localStorage.getItem("current-article-picture"), articlePictureIsDownloaded, false);

        $.when(articlePictureIsDownloaded).then(function () {
            //Download article pdf
            downloadAticleFile_forSingleArticle(window.localStorage.getItem("current-article-pdf"), articlePdfIsDownloaded, true);
        }, function () {
            // Current article files not saved
            downloadAticleFileErrorHandler();
        });

        $.when(articlePdfIsDownloaded).then(function () {
            // Current article files saved
            save_article_complit();
        }, function () {
            // Current article files not saved
            downloadAticleFileErrorHandler();
        });

        function downloadAticleFileErrorHandler() {

            var currentErrorHandler = new errorHandler(); //from app_error_handlers.js
            currentErrorHandler.downloadIssueFilesErrorHandler(currentFilesPathsForGarbageCollector.paths);

            alert("Connection error, please try again later");

            $('body').css({overflow: 'visible'});
            $('#crud-overlay, #app-prelouder img, #app-prelouder').css({display: 'none'});

        }

        function downloadAticleFile_forSingleArticle(fileServerPath, fileDownloadPromise, createServerHttpPathForDownlPDF_boolean) {

            //if the article has default picture, then
            if (fileServerPath == "img/test-announces.jpg") {
                window.localStorage.setItem("default-picture", "yes");
                fileDownloadPromise.resolve();
            } else {
                //Save article with the NOT default picture
                var fileName = creteUniqueFileName(fileServerPath);

                if (createServerHttpPathForDownlPDF_boolean) {
                    //Remove from the url-query "https" pdf-file to download the real pdf-file
                    fileServerPath = "http" + fileServerPath.substr(5);
                } else {
                    window.localStorage.setItem("default-picture", "no");
                }

                var platform_path;

                if (device.platform != "Android") {
                    platform_path = cordova.file.documentsDirectory.replace("file://", "");
                } else {
                    platform_path = cordova.file.externalDataDirectory.replace("file://", "");
                }

                //local path to saving a file on a device
                var file_device_path = platform_path + fileName;

                if (createServerHttpPathForDownlPDF_boolean) {
                    file_device_path = file_device_path + ".pdf";
                }

                window.resolveLocalFileSystemURL(file_device_path, errorCallback, successCallback);

                function successCallback() {

                    var fileTransfer = new FileTransfer();
                    fileTransfer.download(fileServerPath, file_device_path,
                        function (entry) {

                            if (!createServerHttpPathForDownlPDF_boolean) {
                                window.localStorage.setItem("article-img-local-path", file_device_path);
                            } else {
                                window.localStorage.setItem("article-pdf-local-path", file_device_path);
                            }

                            //for garbage collector in the case of article files saving error
                            currentFilesPathsForGarbageCollector.paths.push(file_device_path);

                            fileDownloadPromise.resolve();
                        },
                        function (err) {
                            fileDownloadPromise.reject();
                        });
                }

                function errorCallback() {
                    fileDownloadPromise.reject();
                }
            } //END downloading current article picture
        } //downloadAticleFile()

        function save_article_complit() {

            var articleInfo = {};
            articleInfo.tag = window.localStorage.getItem("current-article-tag");
            articleInfo.id = window.localStorage.getItem("current-article-id");
            articleInfo.title = window.localStorage.getItem("current-article-title");
            articleInfo.author = window.localStorage.getItem("current-article-author");
            if (window.localStorage.getItem("default-picture") == "yes") {
                articleInfo.picture = "img/test-announces.jpg";
            } else {
                articleInfo.picture = window.localStorage.getItem("article-img-local-path");
            }

            articleInfo.pdf = window.localStorage.getItem("article-pdf-local-path");
            articleInfo.pdf_active_view_link = window.localStorage.getItem("current-article-pdf-active-view-link");

            var all_saved_articles = window.localStorage.getItem("saved_articles");
            all_saved_articles = JSON.parse(all_saved_articles);

            all_saved_articles.articles.push(articleInfo);

            localStorage.setItem('saved_articles', JSON.stringify(all_saved_articles));

            //Clear local storage
            window.localStorage.setItem("article-img-local-path", "");
            window.localStorage.setItem("article-pdf-local-path", "");

            $('#crud-overlay, #app-prelouder img, #app-prelouder').css({display: 'none'});
            $('body').css({overflow: 'visible'});

        }

    }; //END save_current_article()

    //Click NO DELETE #delete-article-overlay
    $('body').on('touchstart', '#delete-article-overlay #delete_no', function () {

        $('body').css({overflow: 'visible'});
        $('body').css({position: 'static'});
        $('#delete-article-overlay').css({display: 'none'});
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(650);
    });
    //END Click NO DELETE #delete-article-overlay

    function delete_current_article() {

        $('#delete-article-overlay').css({display: 'none'});
        //$('#crud-text').text('Now article is removing, please wait...');
        //$('#crud-text').css({display: 'none'});
        $('#crud-overlay, #crud-overlay #app-prelouder img, #crud-overlay #app-prelouder').css({display: 'block'});
        $("#touchstart-fix-overlay").fadeIn(100);
        $("#touchstart-fix-overlay").fadeOut(1000);

        var current_article_id = window.localStorage.getItem("current-article-id");

        //get "saved_articles"
        var all_saved_articles = window.localStorage.getItem("saved_articles");
        all_saved_articles = JSON.parse(all_saved_articles);

        jQuery.each(all_saved_articles.articles, function (i, val) {
            //find the current article by id,
            //write auxiliary variables to delete Article
            if (val.id == current_article_id) {
                window.localStorage.setItem("article_iter_for_delete", i);
                window.localStorage.setItem("pdf_delete_path", val.pdf);
                window.localStorage.setItem("img_delete_path", val.picture);
                all_saved_articles = null;
                return false
            }
        }); //end jQuery.each

        //Create two promises
        var articlePictureIsDeleted = new $.Deferred();
        var articlePdfIsDeleted = new $.Deferred();

        //Delete current article files
        //func deleteFile() from auxiliary_functions.js
        deleteFile(window.localStorage.getItem("img_delete_path"), articlePictureIsDeleted);

        $.when(articlePictureIsDeleted).then(function () {
            // Current article picture deleted
            deleteFile(window.localStorage.getItem("pdf_delete_path"), articlePdfIsDeleted);
        }, function () {
            //ignoring the error deleting files necessary for IOS devices from version 9.0,
            //because there's delete function with the successful removal of the file still returns an error
            deleteFile(window.localStorage.getItem("pdf_delete_path"), articlePdfIsDeleted);
        });

        $.when(articlePdfIsDeleted).then(function () {
            // Current article files deleted
            delete_article_complit();
        }, function () {
            //ignoring the error deleting files necessary for IOS devices from version 9.0,
            //because there's delete function with the successful removal of the file still returns an error
            delete_article_complit();
        });

        function delete_article_complit() {

            var all_saved_articles = window.localStorage.getItem("saved_articles");
            all_saved_articles = JSON.parse(all_saved_articles);
            all_saved_articles.articles.splice(window.localStorage.getItem("article_iter_for_delete"), 1);

            localStorage.setItem('saved_articles', JSON.stringify(all_saved_articles));
            $('#crud-overlay, #app-prelouder img, #app-prelouder').css({display: 'none'});
            $('body').css({overflow: 'visible'});
            //alert("Article removed");

            if (page_title == "saved_articles") {
                window.location.reload();
            }

            all_saved_articles = null;

        }

    };//END delete_current_article()


};//END view_and_crud_operations()