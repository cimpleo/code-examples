//Initialize page
var saved_articles = {
  // Page Constructor
  initialize: function () {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function () {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function () {
    saved_articles.receivedEvent('deviceready');
  },
  // Update DOM on a Received Event
  receivedEvent: function () {

    var connection = navigator.connection.type;

    if (connection == "none") {
      alert("You are offline now");
      //pdf viewer flag
      window.localStorage.setItem("after-pdf-view", "no");

      window.location.href = "start_page.html";
    } else {

      init_saved_articles_page();

      function init_saved_articles_page() {

        var all_saved_issues = window.localStorage.getItem("saved_issues");
        all_saved_issues = JSON.parse(all_saved_issues);

        if (all_saved_issues.issues.length < 1) {
          $('#saved_issues_wrap').html('<p>No issues saved.</p>');
        } else {
          jQuery.each(all_saved_issues.issues, function (i, val) {
            $('#saved_issues_wrap')
              .append(''
                + '<div class="more-item" id="issue" data-issue-url="' + val.url + '">'
                    + '<div class="photo">'
                        + '<img src="' + val.picture + '">'
                    + '</div>'
                    + '<div class="text-block">'
                        + '<div class="text-title">'
                            + val.issue_local_name
                        + '</div>'
                        + '<div class="text-content">'
                            + val.date
                        + '</div>'
                    + '</div>'
                + '</div>'
              );
          }); //end jQuery.each

          $('#saved_issues_wrap .more-item .photo').css({
            backgroundImage: function () {
              var imageSrc = $('img', $(this)).attr('src');
              return 'url("' + imageSrc + '")';
            }
          });
        }




      //obtain from localstorage all saved articles
      $(function get_saved_articles() {

        var all_saved_articles = window.localStorage.getItem("saved_articles");
        all_saved_articles = JSON.parse(all_saved_articles);

        if (all_saved_articles.articles.length < 1) {
          $('#saved_articles_wrap').html('<p>No articles saved.</p>');
        } else {

          //auxiliary variable to load articles by scroll
          var articles_quota = 12;

          jQuery.each(all_saved_articles.articles, function (i, val) {

            if (i == articles_quota) {
              return false
            }


            var article_tag = val.tag;
            var article_id = val.id;
            var article_title = val.title;
            var article_author = val.author;
            var picture = val.picture;
            var article_pdf_url = val.pdf;
            var pdf_active_view_link = val.pdf_active_view_link;

            $('#saved_articles_wrap')
              .append(''
                + '<div class="more-item article-item" data-article-tag="' + article_tag + '" data-article-pdf="' + article_pdf_url + '" data-pdf-active-view-link="' + pdf_active_view_link + '" data-article-picture="' + picture + '" data-article-id="' + article_id + '">'
                    + '<div class="photo">'
                        + '<img src="' + picture + '">'
                    + '</div>'
                    + '<div class="text-block">'
                        + '<div class="text-title">'
                            + article_tag
                        + '</div>'
                        + '<div class="text-content">'
                            + article_title
                        + '</div>'
                        + '<div class="text-author">'
                            + article_author
                        + '</div>'
                    + '</div>'
                + '</div>'
              );
          }); //end jQuery.each

          $('#saved_articles_wrap .more-item .photo').css({
            backgroundImage: function () {
              var imageSrc = $('img', $(this)).attr('src');
              return 'url("' + imageSrc + '")';
            }
          });

          //Flag for getting issue articles
          var takeArticles = true;

          //auxiliary variable to load articles by scroll
          window.localStorage.setItem("articles_quota_number", articles_quota);

          $(window).scroll(function () {
            //Scroll calculation. If scroll to the bottom of the page, then
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 700 && !takeArticles) {

              //Create an array of storage elements loadable by scroll
              var elems = [];

              //auxiliary variable to load articles by scroll
              var articles_quota_number = window.localStorage.getItem("articles_quota_number");

              //Remove from the array of those articles that are already loaded on the page
              var load_articles = $(all_saved_articles.articles).slice(articles_quota_number);

              if (load_articles.length > 0) {

                jQuery.each(load_articles, function (i, val) {
                  if (i == articles_quota) {
                    return false
                  }


                  var article_tag = val.tag;
                  var article_id = val.id;
                  var article_title = val.title;
                  var article_author = val.author;
                  var picture = val.picture;
                  var article_pdf_url = val.pdf;
                  var pdf_active_view_link = val.pdf_active_view_link;

                  var div = $('<div/>')
                    .append(''
                      + '<div class="more-item article-item" data-article-tag="' + article_tag + '" data-article-pdf="' + article_pdf_url + '" data-pdf-active-view-link="' + pdf_active_view_link + '" data-article-picture="' + picture + '" data-article-id="' + article_id + '">'
                          + '<div class="photo">'
                            + '<img src="' + picture + '">'
                          + '</div>'
                          + '<div class="text-block">'
                              + '<div class="text-title">'
                                + article_tag
                              + '</div>'
                              + '<div class="text-content">'
                                + article_title
                              + '</div>'
                              + '<div class="text-author">'
                                + article_author
                              + '</div>'
                          + '</div>'
                      + '</div>'
                    );

                  var elem = div.children();
                  elems.push(elem);
                  $('#saved_articles_wrap').append(elems);

                });

                $('#saved_articles_wrap .more-item .photo').css({
                  backgroundImage: function () {
                    var imageSrc = $('img', $(this)).attr('src');
                    return 'url("' + imageSrc + '")';
                  }
                });

                //Flag for getting articles
                var takeArticles = false;

                //rewrite counter to load content
                var counter = window.localStorage.getItem("articles_quota_number");
                counter = Number(counter) + Number(articles_quota);
                window.localStorage.setItem("articles_quota_number", counter);
              } //end if (announcesArray.length > 0)
            } //end scroll calculation
          }); //end scroll
        } //end else
      }); //end get_saved_articles()

      //enable CRUD-functionality for saved articles
      view_and_crud_operations_for_articles_online();


      } // End init_saved_articles_page

    } // End connection

  } // End receivedEvent

}; // End saved_articles(object)
