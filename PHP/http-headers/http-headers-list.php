<?php
function http_headers_list () {
	?>
	<link type="text/css" href="<?php print WP_PLUGIN_URL; ?>/http-headers/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>HTTP Headers</h2>
	<?php
	global $wpdb;
	$rows = $wpdb->get_results("SELECT id,header,value,enabled from wp_http_headers");
	print "<table class='wp-list-table widefat fixed'>";
	print "<tr><th>Enabled</th><th>Header</th><th>Value</th><th>&nbsp;</th></tr>";
	foreach ($rows as $row ){
		$enabled = empty($row->enabled)?'':'checked';
		print "<tr>";
		print "<td><input type='checkbox' name='enabled' $enabled disabled/></td>";	
		print "<td>$row->header</td>";
		print "<td>$row->value</td>";
		print "<td><a href='".admin_url('admin.php?page=http_headers_update&id='.$row->id)."'>Edit</a></td>";
		print "</tr>";}
	print "</table>";
	?>
	<a href="<?php print admin_url('admin.php?page=http_headers_create'); ?>">Add New</a>
	</div>
	<?php
}