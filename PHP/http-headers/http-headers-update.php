<?php
function http_headers_update () {
	global $wpdb;
	$id = $_GET["id"];
	$header = $_POST["header"];
	$value = $_POST["value"];
	$enabled = isset($_POST["enabled"])?1:0;
	//update
	if(isset($_POST['update'])){	
		$wpdb->update(
			'wp_http_headers', //table
			array('header' => $header,'value' => $value, 'enabled'=>$enabled), //data
			array('id' => $id), //where
			array('%s'), //data format
			array('%s') //where format
		);	
	}
	//delete
	else if(isset($_POST['delete'])){	
		$wpdb->query($wpdb->prepare("DELETE FROM wp_http_headers WHERE id = %s",$id));
	}
	else{//selecting value to update
		$headers = $wpdb->get_results($wpdb->prepare("SELECT header,value,enabled from wp_http_headers where id=%s",$id));
		foreach ($headers as $header_value){
			$header=$header_value->header;
			$value=$header_value->value;
			$enabled = empty($header_value->enabled)?'':'checked';
		}
	}
	?>
	<link type="text/css" href="<?php print WP_PLUGIN_URL; ?>/http-headers/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>Edit header</h2>

	<?php if($_POST['delete']){?>
	<div class="updated"><p>Header deleted</p></div>
	<a href="<?php print admin_url('admin.php?page=http_headers_list')?>">&laquo; Back to headers list</a>

	<?php } else if($_POST['update']) {?>
	<div class="updated"><p>Header updated</p></div>
	<a href="<?php print admin_url('admin.php?page=http_headers_list')?>">&laquo; Back to headers list</a>

	<?php } else {?>
	<form method="post" action="<?php print $_SERVER['REQUEST_URI']; ?>">
	<table class='wp-list-table widefat fixed'>
	<tr><th>HTTP Header</th><td><input type="text" name="header" value="<?php print $header;?>"/></td></tr>
	<tr><th>Value</th><td><input type="text" name="value" value="<?php print $value;?>"/></td></tr>
	<tr><th>Enabled</th><td><input type="checkbox" name="enabled" value="<?php print $enabled;?>" <?php print $enabled?'checked':''; ?>/></td></tr>
	</table>
	<input type='submit' name="update" value='Save' class='button'>
	<input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('You sure want to delete this domain?')">
	</form>
	<?php }?>

	</div>
	<?php
}