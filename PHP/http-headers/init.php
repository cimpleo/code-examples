<?php
/*
Plugin name: HTTP Headers Detector
Description: Plugin for tracking headers. $_SESSION['layout_change'] would be true if at least one header is equal to given value.
Author URI: http://www.cimpleo.com
*/

//menu items
add_action('admin_menu','http_headers_modify_menu');

// Hook init
add_action('init', 'http_headers_check');

add_action('init', 'http_headers_start_session', 1);
add_action('wp_logout', 'http_headers_end_session');
add_action('wp_login', 'http_headers_end_session');

function http_headers_getallheaders() 
{ 
   $headers = ''; 
   foreach ($_SERVER as $name => $value) 
   { 
       if (substr($name, 0, 5) == 'HTTP_') 
       { 
           $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
       } 
   } 
   return $headers; 
} 

function http_headers_start_session() {
    if(!session_id()) {
        session_start();
    }
}

function http_headers_end_session() {
    session_destroy ();
}

function http_headers_check() {
	$_SESSION['layout_change'] = false;
	global $wpdb;

	$user_headers = http_headers_getallheaders();
	var_dump($user_headers);
	$results = $wpdb->get_results("SELECT header,value FROM wp_http_headers WHERE enabled = 1");
	foreach ($results as $key => $result) {
		if($user_headers[$result->header] == $result->value) {
			$_SESSION['layout_change'] = true;
			return;
		}
	}
}

function http_headers_modify_menu() {
	
	//this is the main item for the menu
	add_menu_page('HTTP Headers', //page title
	'HTTP Headers', //menu title
	'manage_options', //capabilities
	'http_headers_list', //menu slug
	'http_headers_list'); //function
	
	//this is a submenu
	add_submenu_page('http_headers_list', //parent slug
	'Add more HTTP headers', //page title
	'Add more HTTP headers', //menu title
	'manage_options', //capability
	'http_headers_create', //menu slug
	'http_headers_create'); //function
	
	//this submenu is HIDDEN, however, we need to add it anyways
	add_submenu_page(null, //parent slug
	'Update HTTP Header', //page title
	'Update', //menu title
	'manage_options', //capability
	'http_headers_update', //menu slug
	'http_headers_update'); //function
}
require_once(ABSPATH . 'wp-content/plugins/http-headers/http-headers-list.php');
require_once(ABSPATH . 'wp-content/plugins/http-headers/http-headers-create.php');
require_once(ABSPATH . 'wp-content/plugins/http-headers/http-headers-update.php');

function http_headers_install() {
	global $wpdb;

	$table_title = "wp_http_headers";
	if(!$wpdb->get_var("SHOW TABLES LIKE '$table_title'")) {
	  $sql = "CREATE TABLE " . $table_title . " (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  header VARCHAR(55) NOT NULL,
		  value VARCHAR(55) NOT NULL,
		  enabled boolean DEFAULT '1',
		  UNIQUE KEY id (id)
		);";	  
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}
}

register_activation_hook(__FILE__, 'http_headers_install');