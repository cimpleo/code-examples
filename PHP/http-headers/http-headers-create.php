<?php
function http_headers_create () {
	$id = $_GET["id"];
	$header = $_POST["header"];
	$value = $_POST["value"];
	$enabled = isset($_POST["enabled"])?1:0;
	//insert
	if(isset($_POST['insert'])){
		global $wpdb;
		$wpdb->insert(
			'wp_http_headers', //table
			array('header' => $header,'value' => $value, 'enabled'=>$enabled), //data
			array('%s','%s') //data format			
		);
		$message.="Header inserted";
	}
	?>
	<link type="text/css" href="<?php print WP_PLUGIN_URL; ?>/http-headers/style-admin.css" rel="stylesheet" />
	<div class="wrap">
	<h2>Add New HTTP Header</h2>
	<?php if (isset($message)): ?><div class="updated"><p><?php print $message;?></p></div><?php endif;?>
	<form method="post" action="<?php print $_SERVER['REQUEST_URI']; ?>">
	<table class='wp-list-table widefat fixed'>
	<tr><th>HTTP Header</th><td><input type="text" name="header" value="<?php print $header;?>"/></td></tr>
	<tr><th>Value</th><td><input type="text" name="value" value="<?php print $value;?>"/></td></tr>
	<tr><th>Enabled</th><td><input type="checkbox" name="enabled" value="<?php print $enabled;?>" <?php print $enabled?'checked':''; ?>/></td></tr>
	</table>
	<input type='submit' name="insert" value='Save' class='button'>
	</form>
	</div>
	<?php
}