<?php

namespace App\Models;

use App\Models\UserMeta;
use App\Models\UserTypes;
use App\Models\CdmaWireless;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\AppServices\UserDashboard;


class User extends Authenticatable
{
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'type', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * 
	 * @var [type]
	 */
	protected $defaultMetaField = [
		'user_firstname', 'user_lastname','user_avatar'
	];

	public function UserType()
	{
		return $this->hasOne('App\Models\UserTypes', 'id', 'type');
	}

	public function UserMeta()
	{
		return $this->hasMany('App\Models\UserMeta', 'user_id');
	}

	public function UserPayments() {
		return $this->hasMany('App\Models\UserPayments', 'user_id');
	}

	public function CdmaWireless(){
		return $this->hasMany('App\Models\CdmaWireless', 'user_id');
	}


	public function GetDefaultMetaField() {
		$result = array();
		foreach ( $this->defaultMetaField as $key ) {
			$result[] = array(
				'user_id' 		=> $this->id,
				'meta_name' 	=> $key,
				'meta_value'	=> ''
			);
		}
		return $result;
	}


	public function SetMeta( $field, $value ) {
		$this->UserMeta()->create([ 'meta_name' => $field, 'meta_value' => $value ]);
	}

	public function GetMeta( $field ) {
		if ( $this->UserMeta()->where('meta_name', $field)->exists() ) {
			$result = $this->UserMeta()->where('meta_name', $field)->first();
			return $result->meta_value;
		}
		return false;
	}

	public function GetLastPayment() {
		$result = $this->UserPayments()->where('user_id', $this->id)->orderby('id','desc')->first();
		return $result->id;
	}


	public function DelMeta( $field ) {
		$this->UserMeta()->delete([ 'meta_name' => $field ]);
	}

	public function UpdateMeta($name, $value) {
		if ($this->UserMeta()->where(['meta_name' => $name])->exists()) {
			$this->UserMeta()->where(['meta_name' => $name])->update(['meta_value' => $value]);
			return true;
		}else {
			$this->SetMeta($name, $value);
			return true;
		}
		return false;

	}

	public function GetType($field = false) {
		if ( $type = $this->UserType()->first() ) {
			if ($field) {
				return $type->{$field};
			}
			return $type;
		}

		return false;
	}

	public function InitUserDashboard($type = null, $template = null) {
		if (!empty($type) && !empty($template)) {
			return new UserDashboard($this, $type, $template);
		}else {
			return new UserDashboard($this, $this->GetType('name'), $this->GetType('slug'));
		}
	}
}
