<?php 


/**
* 
*/
class CjExport {

	/**
	 * [$filepath description]
	 * @var [type]
	 */
	protected $filepath;
	protected $type;

	/**
	 * [$readyArray description]
	 * @var array
	 */
	protected $readyArray = array();

	/**
	 * [__construct description]
	 * @param [type] $type     [description]
	 * @param [type] $filename [description]
	 * @param [type] $offset   [description]
	 */
	function __construct( $type, $filename, $offset, $is_cron = false ) {
		$this->type = $type;
	// Check file by exist
		$this->_create_filepath( $filename );

	// Fill file, set up options for XML
		if ( $offset == 0  ) {
			switch ( $this->type ) {
				case 'xml':
					$this->fill_options_xml_file();
					break;
				case 'csv':
					$this->fill_options_csv_file();
					break;				
				default:
					break;
			}
		}
	// Generate EXPORT loop
		$this->generate_loop( $offset, $is_cron );

	// Write readyArray to created file
		if ( !empty( $this->readyArray ) )
			$this->writeInFile( $this->type );
	}

	/**
	 * [writeInFile description]
	 * @return [type] [description]
	 */
	protected function writeInFile( $type ) {
		if ( empty( $this->filepath ) )
			return false;

		switch ( $type ) {
			case 'csv':
				$status = $this->write_in_csv();
				break;
			case 'xml':
				$status = $this->write_in_xml();
				break;
			default:
				$status = false;
				break;
		}

		if ( $status ) {
			$this->status = 'completed';
			return true;
		}else {
			$this->status = 'errors';
		}

		return false;
	}

	protected function write_in_csv() {
		foreach ( $this->readyArray as $item ) {
			$csvItem = $this->filterItem_for_csv( $item );
			if ( $csvItem ) {
				file_put_contents( $this->filepath, implode(',', $csvItem), FILE_APPEND );
			}
		}
		return true;
	}

	protected function filterItem_for_csv($item) {
		if ( empty($item) )
			return false;

		foreach ( $item as $key => $value ) {
			switch ( $key ) {
				case 'name':
					$name = str_replace('"', '', $item['name']);
					$item['name'] = '"'. $name .'"';
					break;
				case 'description':
					$desc = $item['description'];
					if ( $desc != 'Description is missing' ) {
						$desc = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $desc);
						$desc = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $desc);
					}
					$item['description'] = '"'. $desc .'"';
					break;
				case 'sku':
					$item['sku'] = '"'. $item['sku'] .'"';
					break;
				case 'buyurl':
					$item['buyurl'] = htmlspecialchars($item['buyurl']);
					break;
				case 'imageurl':
					$item['imageurl'] = htmlspecialchars($item['imageurl']);
					break;
				default:
					break;
			}
		}

		return $item;

	}

	/**
	 * [write_in_xml description]
	 * @return [type] [description]
	 */
	protected function write_in_xml() {
		$xmlCatalog = simplexml_load_file( $this->filepath );
		if ( $xmlCatalog ) {
			foreach ( $this->readyArray as $item ) {
			// Set Node
				if ( $item ) {
					$xmlProduct = $xmlCatalog->addChild('product');
			// Set atribute for xml file	
					foreach ($item as $key => $value) {
						$xmlProduct->addChild( $key, htmlspecialchars( $value ) );
					}
				}
			}
			if ( $xmlString = $xmlCatalog->asXML() ) {
				$dom = new DOMDocument();
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				$dom->loadXML($xmlString);
				$formattedXML = $dom->saveXML();

				$fp = fopen($this->filepath,'w+');
				if ( $fp != false) {
					fwrite($fp, $formattedXML);
					fclose($fp);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * [generate_loop description]
	 * @param  [type] $offset [description]
	 * @return [type]         [description]
	 */
	protected function generate_loop( $offset, $is_cron ) {
		if ($is_cron ) {
			$query_loop = new WP_Query( array(
				'posts_per_page'	=> -1,
				'post_type'			=> 'product',
				'fields'			=> 'ids',
				'date_query' => array(
					'column'	=> 'post_modified',
					'after'		=> '1 day ago'
				)
			) );

		}else {
			$query_loop = new WP_Query( array(
				'posts_per_page'	=> 100,
				'post_type'			=> 'product',
				'offset'			=> $offset,
				'fields'			=> 'ids',
				'meta_query'		=> array(
					'event_date' => array(
						'key'	=> '_date',
						'value' => date('Y/m/d'),
						'compare' => '>=',
						'type' 	=> 'DATE'
					)
				)
			) );
		}

		if ( $query_loop->have_posts() ) {
			$currency = get_woocommerce_currency();
			$wc_factory = new WC_Product_Factory();

			foreach ( $query_loop->posts as $post_id ) {
				$ItemRecord = new CjItemRecord( $post_id, $currency, $wc_factory );
				$this->readyArray[] = $ItemRecord->get_productObject();
				if ( $variations = $ItemRecord->get_variationsItems() ) {
					foreach ( $variations as $item ) {
						$this->readyArray[] = $ItemRecord->get_variationsObject( $item );
					}
				}
			}
		}

	}

	/**
	 * [fill_options_csv_file description]
	 * @return [type] [description]
	 */
	protected function fill_options_csv_file() {
		if ( !empty( $this->filepath ) ) {
		// CSV parameters
			$parameters_array = array(
				'NAME', 'KEYWORDS', 'DESCRIPTION', 'SKU', 'BUYURL',
				'PRICE', 'CURRENCY', 'INSTOCK', 'AVAILABLE','IMAGEURL'
			);
			$parameters = implode( '|', $parameters_array );

		// Set settings
			$settings = get_option('CJAffiliate_plugin_export');
			$array_options = array( 'cid', 'subid', 'aid', 'processtype' );
			if ( $settings ) {
				foreach ($settings as $key => $value) {
					if ( in_array($key, $array_options ) && !empty( $value ) ) {
						$string = '&'. $key. '='. $value;
						file_put_contents( $this->filepath, $string, FILE_APPEND );
					}
				}
			// Set  parameters in file
				if ( $parameters ) {
					file_put_contents( $this->filepath , '&PARAMETERS=' . $parameters, FILE_APPEND );
				}
				return true;
			}		
		}
		return false;
	}

	/**
	 * [fill_options_xml_file description]
	 * @return [type] [description]
	 */
	protected function fill_options_xml_file() {
		if ( !empty( $this->filepath ) ) {
			include('sampleXML.php');
			$xmlCatalog = new SimpleXMLElement(  $xmlstr );
	// Set settings 
			$settings = get_option('CJAffiliate_plugin_export');
			$array_options = array( 'cid', 'subid', 'aid', 'processtype' );
			if ( $settings ) {
				$xmlHeader = $xmlCatalog->addChild('header');
				foreach ($settings as $key => $value) {
					if ( in_array($key, $array_options ) && !empty( $value ) ) {
						$xmlHeader->addChild( $key, $value );
					}
				}
				$xmlCatalog->asXML($this->filepath);
				return true;
			}		
		}
		return false;
	}

	/**
	 * [_create_filepath description]
	 * @param  [type] $filename [description]
	 * @return [type]           [description]
	 */
	protected function _create_filepath( $filename ) {
		$filename = $filename.'.'.$this->type;
		$filepath = CJAFFILIATE_UPLOADS.$filename;

		if ( !file_exists( $filename ) ) {
			touch( $filepath );
		}

		$this->filepath = $filepath;
	}

	/**
	 * [getStatus description]
	 * @return [type] [description]
	 */
	public function getStatus( $currentStep, $lastStep ) {
		if ( $currentStep === $lastStep && $this->status == 'completed' ) {
			return 'true';
		}
		return 'false';
	}

	/**
	 * [getFilepath description]
	 * @return [type] [description]
	 */
	public function getFilepath() {
		return $this->filepath;
	}
