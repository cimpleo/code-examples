<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		//
		\App\Exceptions\UserNotify::class
	];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $exception
	 * @return void
	 */
	public function report(Exception $exception)
	{
		if ($exception instanceof SoapFault) {
			dd($exception);
		}
		parent::report($exception);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $exception
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $exception)
	{

	// If user doesn't auth
		if ( $exception instanceof \Illuminate\Auth\AuthenticationException ) {
			return redirect()->route('login');
		}

	// Validator error
		if ( $request->is('register') ) {
			if($exception instanceof \Illuminate\Validation\ValidationException) {
				return redirect()->back()->withErrors( $exception->validator );
			}
		}

		// 418 Another provider
		if ($exception->getCode() === 418) {
			return response()->view('errors.418', [
				'user'		=> auth()->user(),
				'message' 	=> $exception->getMessage()
			]);
		}

		// API unavailable 503
		if ($exception->getCode() === 503  ) {
			$text = $exception->getMessage();
			$notifyMessages['danger'][] =  str_replace("'", '"', $text);
			return response()->view('errors.503', ['text'=>$text]);
		}

		// Http exceptions
		if ($this->isHttpException($exception)) {

			// try to find right error page for this exception
			// I have prepared 4 most common errors: 403,404, 500 and 503
			// $exception = FlattenException::create($e);
			$statusCode = $exception->getStatusCode($exception);
			if (in_array($statusCode, array(403, 404, 500 ))) {
				return response()->view('errors.' . $statusCode, [], $statusCode);
			}
		}
		
		return parent::render($request, $exception);
	}
}