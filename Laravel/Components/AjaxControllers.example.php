<?php

namespace App\Http\Controllers;



use App\Http\Requests\AjaxRequest;
use App\Http\Controllers\Controller;
use App\Models\{Model_name};
use App\Models\{Model_name};

class AjaxController extends Controller {

	use Ajax\{Name_of_trait};
	use Ajax\{Name_of_trait};

	public function index(AjaxRequest $request)	{
		try {
			$result = $this::{$request->input('action')}($request);
			return response()->json(['status' => true, 'data' => $result]);
		} catch (\Exception $e) {
			if ($e->getCode() === 412) {
				return response()->json(['status' => false, 'validate' => false, 'error' => $e->getMessage()]);
			}
			return response()->json(['status' => false, 'error' => $e->getMessage()]);
		}
	}
}