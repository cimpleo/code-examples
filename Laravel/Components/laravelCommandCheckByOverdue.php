<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;



use App\Models\Network;
use App\Models\Wireless\Wireless;

use App\Services\Chargebee\Chargebee;

use App\Http\Controllers\APIs\V1\Networks\{network_name};
use App\Http\Controllers\APIs\V1\Networks\{network_name};


class TelispireCheckOverdueVerizonWireless extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'network:checkoverdutverizonwireless';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command check Verizon User Wireless by payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {  
        \Log::channel('apilog_command')->info([
            'command' => $this->signature,
            'action'  => 'RUN'
        ]);
        
        $all_cdma_wireless = Wireless::where('type', '=', 'CDMA')->get();
        if ($all_cdma_wireless) {
            // Get Overide Payments from all Wirelesss.
            $overdue_payments = $this->getOverDueSubcribtionsList($all_cdma_wireless);
            if (!empty($overdue_payments)) {
                // Foreach by each Network.
                foreach ($overdue_payments as $network_name => $wireless_items ) {
                    // Get current API.
                    $API = $this->getApiClass($network_name);
                    if ($PixApiClass) {
                        foreach ($wireless_items as $wireless_id => $wireless_subcribtion) {
                            // Check date. (check by payment is missed for 3 days?)
                            if ($this->isOverDueDate($wireless_subcribtion['due_since'])) {
                                $this->changeWirelessStatus($API, $wireless_id);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * [changeWirelessStatus description]
     * @param  [type] $wireless_id [description]
     * @return [type]              [description]
     */
    private function changeWirelessStatus($API, $wireless_id) {
        try {   
            $result = $API->wireless_to_hotline(['wireless_id' => $wireless_id]);
            \Log::channel('apilog_command')->info( 'Wireless: [ '. $wireless_id .' ] has been changed status. To Hotline.');
        } catch (\Exception $e){
            \Log::channel('apilog_command')->alert('ALERT! We cannot change wireless Status. ' . json_encode($e->GetMessage()) );
        }
    }


    /**
     * [checkOverDueDate description]
     * @param  timestamp
     * @return boolen            [description]
     */
    private function isOverDueDate($due_since) {
        $current_date = time();
        $different = $current_date - $due_since;
        $different_oval = round($different / 86400);

        return ($different_oval >= 3) ? true : false;
    }

    /**
     * [getOverDueSubcribtionsList description]
     * @param  object $all_wireless [description]
     * @return [wirlesss_id] => SubcribtionValues
     */
    private function getOverDueSubcribtionsList(object $all_wireless):array {
        $Chargebee = new Chargebee;
        $user_payments_wireless = [];
        foreach ($all_wireless as $wireless) {
            if (!empty($wireless->Payment) && !empty($wireless->Payment->transaction_id)) {
                try {
                    $subcribtion_query = $Chargebee->getSubscription($wireless->Payment->transaction_id);
                    $subcribtion = $subcribtion_query->subscription()->getValues();
                    if ($subcribtion['due_invoices_count'] > 0 && isset($subcribtion['due_since'])) {
                        $user_payments_wireless[$wireless->getNetwork('name')][$wireless->id] = $subcribtion;
                    }
                } catch ( \Exception $e) {
                    \Log::channel('apilog_command')->info([
                        'command' => 'network:checkoverdutverizonwireless',
                        'wireless_id' => $wireless->id,
                        'exception' => $e->getMessage()
                    ]);
                }
            }
        }
        return $user_payments_wireless;
    }



    /**
     * [getPixApiClass description]
     * @param  string $network_name [description]
     * @return [type]               [description]
     */
    private function getApiClass(string $network_name) {
        switch ($network_name) {
            case 'name':
                $API = new {network_name};
                break;
            case 'name':
                $API = new {network_name};
                break;
            default:
                $API = false;
                break;
        }
        return $API;
    }


}
