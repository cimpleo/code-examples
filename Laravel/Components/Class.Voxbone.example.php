<?php 

namespace App\AppServices\Voxbone;

use App\AppServices\Voxbone\Cart;

use App\APIs\ClassProviders\voxbone\vb_didGroup;

final class Voxbone {
	
	private $server;


	function __construct( &$UserDashboard) {
		$this->server = new Requests;
		$UserDashboard->Cart = new Cart(
			$UserDashboard->id,
			$UserDashboard->GetMetaValue('voxbone_cart_id')
		);
		if ( $CartIdentifier = $UserDashboard->Cart->GetCartIdentifier() ) {
			$UserDashboard->UpdateMetaValue('voxbone_cart_id', $CartIdentifier );
		}

	}

	public function GetCountryList(){
		$result = $this->server->GetCountryList_query();

		if ( $result['status'] == true && $result['data']->resultCount != 0 ) {
			$countriesList = $result['data']->countries;
			foreach ( $countriesList as $key ) {
				$newList[] = array( 
					'id' => $key->countryCodeA3,
					'state' => $key->countryCodeA3,
					'text'	=> $key->countryName
				);
			}
		}
		if ( isset( $newList ) )
			return $newList;
	}

	public function GetDidGroupByCountry($param) {
		try {
			return $this->server->get_didGroup_by_country($param);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function GetSingleDidGroup($array) {
		try {
			$id = $array[0];
			$country = $array[1];
			return $this->server->get_single_did_group($id, $country);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function CreateCartQuery($user) {
		try {
			return $this->server->create_cart_query($user->id);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function GetListQuery($cart_id) {
		try {
			return $this->server->list_cart_query($cart_id);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function ListCartQuery($cart_id) {
		try {
			return $this->server->list_cart_query($cart_id);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function GetCardByIdQuery($user) {
		try {
			return $this->server->get_cart_by_id_query($user);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function ListSpecificCart($cart_id) {
		try {
			return $this->server->list_specific_cart($cart_id);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function CheckoutCartQuery($cart_id) {
		try {
			return $this->server->checkout_cart_query($cart_id);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function CheckoutCart($array) {
		try {
			$user = $array[0];
			$cart_id = $array[1];
			return $this->server->checkout_cart($user,$cart_id);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function GetOrderReference($result) {
		try {
			return $this->server->get_order_reference($result);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function GetDidNumberByReference($reference) {
		try {
			return $this->server->get_didnumber_by_reference($reference);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function AccountBalanceQuery() {
		try {
			return $this->server->account_balance_query();
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}

	public function ListOrder($reference) {
		try {
			return $this->server->list_order($reference);
		} catch ( providerException $e) {
			return $this->errors = $e;
		}
	}
}