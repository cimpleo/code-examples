<?php

namespace App\AppServices\{};

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Config;

class Requests {
	
	private $env_status;
	private $login;
	private $password;

	function __construct() {
		$env = config::get('api.{}.env');
		$account = config::get('api.{}.account');
		$pas = config::get('api.{}.pas');
		if (empty($env) || empty($account) || empty($pas)) {
			throw new providerException("Doesn't set required data");
		}
		$this->env_status = $env;
		$this->login = $account;
		$this->password = $pas;
		if ( $this->env_status == 'live' ){
			$this->inventory_url = '';
			$this->ordering_url  = '';
		}
		else {
			$this->inventory_url = '';
			$this->ordering_url  = '';
		}

	}

	public function GetCountryList_query() {
		$response = Curl::to( $this->inventory_url . 'country' )
			->withOption('USERPWD', $this->login.':'. $this->password )
			->withContentType('application/json')
			->withHeader('Accept: application/json')
			->withData( array(
	        	'pageNumber'=>0,
	        	'pageSize' => 5000
			))->get();
		$checking_response = $this->check_response_for_errors( $response );
		return $this->is_error( $checking_response );
	}



	private function check_response_for_errors($response) {
		if ($this->isJSON($response)) {
			$responseJSON = json_decode( $response );
			if ( isset( $responseJSON->errors ) ) {
				$errors = $responseJSON->errors;
				return array('status' => false, 'errors' => $errors);
			}
		}
		else {
			$error_array = array(
				'apiErrorMessage' => 'We have problems, please try again later.',
				'apiErrorCode' => 500
			);
			return array(
				'status' => false,
				'data' => (object) $error_array
			);
		}
		return array('status' => true, 'data' => $responseJSON);
	}

	private function is_error( $response ) {
		if ( $response['status'] === false ) {
			//throw new providerException( $response['data']->apiErrorMessage, $response['data']->apiErrorCode );
			return (array)$response;
		}
		return (array)$response;
	}

	private function isJSON( $string ) {
		json_decode( $string );
		return (json_last_error() == JSON_ERROR_NONE);
	}

}