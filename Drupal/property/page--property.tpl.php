<?php $images = drupal_get_path('theme', 'coldwellbanker'); ?>
<header class="clearfix">
    <div class="container">
        <div class="logo" id="logo">
            <a href="<?php print $front_page ?>">
                <?php if ($logo): ?>
                    <img src="<?php print $logo ?>"  id="logo" />
                <?php endif; ?>            
            </a>
        </div>
        <div class="navAbove">
            <div class="feedback">
                <div class="mortgageLink" id="mortgageLink">
                    <a target="_blank" href="https://www.coldwellbankermortgage.com/home/landscape?jpid=SSLoanStart&amp;cid=81408">Coldwell Banker Mortgage 888.308.6558</a>
                </div>
            </div>
            <div id="socialWrapper" class="socialWrapper">

                <div class="socialIcon pinterest">
                    <a target="_blank" href="http://www.pinterest.com/coldwellbanker">&nbsp;</a>
                </div>
                <div class="socialIcon wordpress">
                    <a target="_blank" href="http://blog.coldwellbanker.com/">&nbsp;</a>
                </div>
                <div class="socialIcon youtube">
                    <a target="_blank" href="http://www.youtube.com/coldwellbanker">&nbsp;</a>
                </div>
                <div class="socialIcon twitter">
                    <a target="_blank" href="http://twitter.com/coldwellbanker">&nbsp;</a>
                </div>
                <div class="socialIcon facebook">
                    <a target="_blank" href="http://www.facebook.com/coldwellbanker">&nbsp;</a>
                </div>
            </div>
        </div>

        <?php
        $menu = menu_tree('main-menu');
        echo render($menu);
        ?>
    </div>
</header>
<?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
            <?php print $messages; ?>
        </div></div> <!-- /.section, /#messages -->
<?php endif; ?>
<div class="page">
    <div class="page-property-detail">
        <?php print render($page['header']); ?>  
        <?php if ($title): ?><div class="property-title"><h1>
                    <?php print $title; ?></h1></div><?php endif; ?>
        <div id="tabs">
            <ul>
                <li><div class="left"></div><a href="#tabs-1"><?php print t('PROPERTY DETAILS'); ?></a><div class="right"></div></li>
                <li><div class="left"></div><a href="#tabs-2"><?php print t('MAP'); ?></a><div class="right"></div></li>  
                <li><div class="left"></div><a href="#tabs-3"><?php print t('AGENT'); ?></a><div class="right"></div></li>
                <li><div class="left"></div><a href="#tabs-4"><?php print t('OFFICE'); ?></a><div class="right"></div></li> 
            </ul>
            <div id="tabs-1"><div class="propertyDetailContent">
                    <div class="details-container">
                        <span class="intern"><?php              
                if ($variables['fields_property']) {
                    print $variables['fields_property']['INTERN'];
                }
                ?></span>
                        <div class="left-column">
                            <div class="second-string">
                                <span class="buero">
                                    <?php
                                    if ($variables['fields_property']) {
                                        print $variables['fields_property']['Webserver___Buero'];
                                    }
                                    ?>
                                </span>
                                <a class="website-url" href="<?php print $variables['fields_property']['websiteUrl'] ?>" target="_blank"><?php print t('View Local Website'); ?></a>
                            </div>
                            <span class="price">&euro;<?php print $variables['fields_property']['Miete'] ?></span>
                            <div class="info"><span class="count-rooms">
                                    <span class="dsn"> DSN </span>
                                    <?php
                                    if ($variables['fields_property']) {
                                        print $variables['fields_property']['dsn'];
                                    }
                                    ?>
                            </div>
                            <div class="view-map">View Map</div>
                        </div>

                        <div class="right-column">
                            <div class="land"><label>Lacation: </label><?php
                                    if ($variables['fields_property']) {
                                        print $variables['fields_property']['Land_PLZ_Ort'];
                                    }
                                    ?></div>
                            <div class="objektart"><label>Prop Type:</label><?php
                                if ($variables['fields_property']) {
                                    print $variables['fields_property']['Objektart'];
                                }
                                    ?></div>
                        </div>
                    </div>
                    <div class="image-and-agent">
                        <div class="photo-container">
                            <ul id="pikame">
                                <li><img src="http://i.coldwellbanker.com/138i82/61w3zgghqajrm422dpfq9n62h2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/wgwc4v512gkk40esv4qajjhjd2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/wgwc4v512gkk40esv4qajjhjd2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/wgwc4v512gkk40esv4qajjhjd2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/wgwc4v512gkk40esv4qajjhjd2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/wgwc4v512gkk40esv4qajjhjd2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/wgwc4v512gkk40esv4qajjhjd2i82"/><span></span></li>
                                <li><img src="http://i.coldwellbanker.com/138i82/61w3zgghqajrm422dpfq9n62h2i82"/><span></span></li>
                            </ul>
                            <div class="detailTitleDivider"> </div>
                        </div>
                        <div class="contact-column">
                            <h2>Contact <span class="name"><?php
                                if ($variables['agent']) {
                                    print $variables['agent']['ip_usr_firstname'] . ' '
                                            . $variables['agent']['ip_usr_lastname'];
                                }
                                    ?>
                                </span></h2>
                            <div class="detailTitleDivider"> </div>
                            <div>
                                <div class="photo"></div>
                                <div class="contact-info">                                    
                                    <div class="office"><label><?php print t('Office:'); ?> 
                                        </label>
                                        <?php
                                        if ($variables['fields_property']) {
                                            print $variables['fields_property']['Webserver___Buero'];
                                        }
                                        ?></div>
                                    <div class="adress"><label><?php print t('Adress:'); ?> </label> <?php
                                        if ($variables['office']) {
                                            print $variables['office']['address_street']
                                                    .', '. $variables['office']['address_postalCode']
                                                    .', '. $variables['office']['address_city'];
                                        }
                                        ?>
                                    </div>
                                    <div class="phone"><label><?php print t('Phone:'); ?> </label>
                                        <?php
                                        if ($variables['office']) {
                                            print $variables['office']['phone'];
                                        }
                                        ?></div>
                                    <div class="mobile"><label><?php print t('Mobile:'); ?> </label>
                                        <?php
                                        if ($variables['agent']) {
                                            print $variables['agent']['ip_usr_mobilePhone'];
                                        }
                                        ?>
                                    </div>
                                    <div class="email"><label><?php print t('Email:'); ?> </label>
                                        <?php
                                        if ($variables['agent']) {
                                            print $variables['agent']['ip_usr_email'];
                                        }
                                        ?>
                                    </div>
                                </div>

                                <?php
                                $form = module_invoke('webform', 'block_view', 'client-block-27');
                                 print $form['content'];
                                ?>                          
                            </div>
                        </div>    
                    </div>
                    <div class="detail-sub-container">  
                        <div class="share"><a href="#">Share</a></div>
                        <div class="view-map">Map</div>                        
                    </div>
                    <div class="other-content">
                        <div class="left-sidebar">
                            <div class="description-container">
                                <h2 class="detail-title">Description</h2>
                                <div class="detailTitleDivider"> </div>
                                <div class="text">
                                    <div id="about-label">
                                        <?php print t('Description') ?>
                                    </div>
                                    <p><?php
                                        if ($variables['fields_property']) {
                                            print $variables['fields_property']['TEXTOBJEKT'];
                                        }
                                        ?></p>
                                    <div id="about-label">
                                        <?php print t('Equipment') ?>
                                    </div>
                                    <p><?php
                                        if ($variables['fields_property']) {
                                            print $variables['fields_property']['TEXTAUSSTATTUNG'];
                                        }
                                        ?></p>
                                    <div id="about-label">
                                        <?php print t('Location') ?>
                                    </div>
                                    <p><?php
                                        if ($variables['fields_property']) {
                                            print $variables['fields_property']['TEXTLAGE'];
                                        }
                                        ?></p>
                                    <div id="about-label">
                                        <?php print t('Other') ?>
                                    </div>
                                    <p><?php
                                        if ($variables['fields_property']) {
                                            print $variables['fields_property']['TEXTAUSSTATTUNG'];
                                        }
                                        ?></p>
                                </div>
                            </div>
                            <div class="border"></div>
                            <div class="features-container">
                                <h2 class="detail-title">Features</h2>
                                <div class="detailTitleDivider"> </div>
                                <?php
                                $features = array();
                                if ($variables['features']) {
                                    $features = $variables['features'];
                                }
                                foreach ($features as $key => $value) {
                                    $label = str_replace("_", " ", $key);
                                    $label = str_replace("Webserver", "", $label);
                                    $label = str_replace("   ", "", $label);
                                    $label = strtolower($label);
                                    $label = ucfirst($label);
                                    print '<strong>' . $label . '</strong>';
                                    print '<div class="text">' . $value . '</div>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="right-sidebar">
                            <div class="map">
                                <div id="small-map">        

                                </div>
                                <div class="view-map">Comprehensive Map View</div>
                                <img width="11" height="11"  src="/<?php print $images ?>/images/orange-arrow.png">
                            </div>
                        </div>
                    </div>
                </div></div>
            <div id="tabs-2"><div class="propertyDetailContent">
                    <div id="map"></div>
                </div></div>
            <div id="tabs-3">
                <div class="propertyDetailContent">
                    <div class="details-agent">
                        <div class="agent-photo">

                        </div>
                        <div class="info-box">
                            <div class="name"><?php
                                if ($variables['agent']) {
                                    print $variables['agent']['ip_usr_firstname'] . ' '
                                            . $variables['agent']['ip_usr_lastname'];
                                }
                                ?>
                            </div>   
                            <span>
                                <?php if ($variables['agent']) {
                                    $all_properties='/agent/'.$variables['agent']['ip_usr_id'].'?agent='.$variables['agent']['ip_usr_email'];
                                    print '<a class="all-list" href="'.$all_properties.'">'.t('View All Agent Listings').'</a>';
                                    
                                }?>                             
                            </span>
                            <div class="adress"><label>Adress </label> <?php
                                if ($variables['office']) {
                                    print $variables['office']['address_street'] . '<br>' . $variables['office']['address_city'] . ', ' .
                                            $variables['office']['address_postalCode'];
                                }
                                ?>
                            </div>
                            <div class="phone"><label>Phone: </label>
                                <?php
                                if ($variables['office']) {
                                    print $variables['office']['phone'];
                                }
                                ?></div>
                            <div class="mobile"><label>Mobile: </label>
                                <?php
                                if ($variables['agent']) {
                                    print $variables['agent']['ip_usr_mobilePhone'];
                                }
                                ?>
                            </div>
                            <?php
                                $agent_id="";
                                if ($variables['agent']) {
                                    $agent_id=$variables['agent']['ip_usr_id'];
                                }
                                ?>                           
                        </div>
                         <div class="contact"><a href="<?php print  $agent_id; ?>"><?php print t('Contact'); ?></a></div>
                                                   <div id="agent-id-<?php print  $agent_id ?>" class="dialog-form">
<?php 
     $form = module_invoke('webform', 'block_view', 'client-block-27');
     print '<div id="info"></div>'.$form['content'];
?>
</div>
                         <div class="property-info">
                            <div class="photo">
                            </div>
                            <label><?php print t('Property Details:'); ?></label>
                            <div class="sale-status"><label><?php print t('Sale Status'); ?></label><?php print $variables['fields_property']['Objektart'] ?></div>
                            <div class="price"><label><?php print t('Price'); ?></label>&euro;<?php print $variables['fields_property']['Miete'] ?></div>
                            <div class="location"><label><?php print t('Location'); ?></label><?php print $variables['fields_property']['Land_PLZ_Ort'] ?></div>
                        </div>
                    </div>
                    <div class="description-agent">
                        <div class="detail-title">
                            <?php
                            if ($variables['agent']) {
                                print t('About') . ' ' . $variables['agent']['ip_usr_firstname'] . ' '
                                        . $variables['agent']['ip_usr_lastname'];
                            }
                            ?>
                        </div>
                        <p><?php
//                                if ($variables['agent']) {
//                                    print $variables['agent']['ip_usr_description'];
//                                }
                            ?>
                            Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum
                        </p>
                    </div>
                    <div class="language-agent">
                        <div class="detail-title">
                            <?php print t('languages')//. geoip_region_name_by_code('de')  ?>
                        </div>
                        <ul>
                            <li><?php
                            if ($variables['agent']) {
                                print country_by_iso_code($variables['agent']['ip_usr_profileLanguage']);
                            }
                            ?></li> 
                        </ul>
                    </div>    
                </div>

            </div>
            <div id="tabs-4">
                <div class="propertyDetailContent">
                    <div class="details-office">
                        <div class="office-image">

                        </div>
                        <div class="info-box">
                            <div class="name"> <?php
                                if ($variables['fields_property']) {
                                    print $variables['fields_property']['Webserver___Buero'];
                                }
                            ?>
                            </div>                              
                            <div class="adress"><label>Adress </label> <?php
                                if (isset($variables['office']['address_street'])) {
                                    print $variables['office']['address_street'] . '<br>' . $variables['office']['address_city'] . ', ' .
                                            $variables['office']['address_postalCode'];
                                }
                            ?>
                            </div>
                            <div class="phone"><label>Phone: </label>
                                <?php
                                if ($variables['office']) {
                                    print $variables['office']['phone'];
                                }
                                ?></div>
                            <div class="fax">
                             <?php if (!empty($variables['office']['fax'])): ?>
                                <label><?php print t('Fax:'); ?></label>
                                <?php
                                print $variables['office']['fax'];
                                ?>
                              <?php endif; ?>  
                                </div>
                        </div>
                        <div class="property-info">
                            <div class="photo">
                            </div>
                            <label><?php print t('Property Details:'); ?></label>
                            <div class="sale-status"><label><?php print t('Sale Status'); ?></label><?php print $variables['fields_property']['Objektart'] ?></div>
                            <div class="price"><label><?php print t('Price'); ?></label>&euro;<?php print $variables['fields_property']['Miete'] ?></div>
                            <div class="location"><label><?php print t('Location'); ?></label><?php print $variables['fields_property']['Land_PLZ_Ort'] ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <footer>     
        <div id="footerContent" class="footerContent">
            <div id="navPanel" class="footerRow">
                <?php
                $menu = menu_tree('menu-footer-navigation');
                echo render($menu);
                ?><div class="didYouKnow">
                    <div class="description">
                        <h3>DID YOU <span class="know">KNOW?</span></h3>
                        <span class="advancedOptions">
                            <strong>Advanced Options and Keyword Search </strong> let you search beyond beds, baths and price.
                        </span>
                    </div>
                </div>
                <div class="loginBox">

                </div>
            </div>
            <div id="footerBottomPanel" class="footerRow">
                <?php
                $menu = menu_tree('menu-footer-bottom-links');
                echo render($menu);
                ?>
                <?php print render($page['footer_second']); ?>            
            </div>
        </div>
    </footer>