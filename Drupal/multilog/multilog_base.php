<?php

abstract class multilog_base {
  const MULTILOG_ARGTOSTRING_MAX_NESTING_LEVEL = 10;
  /**
   * @var array
   */
  protected $filters = array();

  /**
   * @param array $event
   *
   */
  abstract public function writeEvent(array $event);

  /**
   * @param array $filters
   *
   */
  public function setFilters($filters = array()) {
    if (is_array($filters)) {
      $this->filters = $filters;
    }
  }

  /**
   * @param $type
   * @param int $severity
   * @return bool
   */

  protected function filterExists($type, $severity = 5) {
    $filterExists = FALSE;

    foreach ($this->filters as $filter) {
      if ($type == $filter['key'] && $severity <= $filter['level']) {
        $filterExists = TRUE;
      }
    }

    return $filterExists;
  }

  protected function getMessage(array $event) {
    $message = '';
    $format = $this->getFormat($event['type']);
    if ($format) {
      $params = $this->parseMsg($event);
      $message = t(
        $format, array(
          '!stamp' => $params['date'],
          '!severity' => $params['severity'],
          '!logger' => $params['type'],
          '!message' => $params['message'],
          '!user' => 'USER: ' . $params['user'],
          '!request' => 'REQUEST: ' . $params['request'],
          '!getpost' => 'GET&POST: ' . $params['get'] . "\n" . $params['post'],
          '!trace' => "STACKTRACE:\n" . $params['stacktrace'],
          '!headers' => "HEADERS: " . $params['headers'],
          '!args' => 'VARIABLES: ' . $params['params'],
          '!cookies' => 'COOKIES: ' . $params['cookies'],
        )
      );

    }

    return $message;
  }

  /**
   * @param string $type
   * @return string
   */
  private function getFormat($type) {
    $format = '';

    foreach ($this->filters as $filter) {
      if ($type == $filter['key']) {
        $format = isset($filter['format']) ? $filter['format'] : '';
      }
    }

    return $format;
  }

  /**
   * @param array $event
   * @return array
   */
  private function parseMsg(array $event) {
    $result = array();

    $sevList = multilog_get_serenities_list();

    $result['date'] = date('Y-m-d H:i:s');
    $result['severity'] = $sevList[$event['severity']];
    $result['type'] = $event['type'];
    if (isset($event['message'])) {
      $msg = preg_replace('/%/', '!', $event['message']);
      $vars = array();
      if (is_array($event['variables'])) {
        foreach ($event['variables'] as $key => $item) {
          if (substr($key, 0, 1) == '%') {
            $key = '!' . substr($key, 1);
            $vars[$key] = $item;
          }
        }
      }
      $result['message'] = strtr($msg, $vars);
    }
    else {
      $result['message'] = '';
    }
    $result['user'] = (property_exists($event['user'], 'name')
        ? $event['user']->name : "") . ' (' . $event['user']->uid . ")";
    $result['get'] = $this->argToString($_GET);
    $result['post'] = $this->argToString($_POST);
    $result['params'] = $this->argToString($event['variables']);
    $result['stacktrace'] = $this->parseTrace(debug_backtrace());

    if (isset($_SERVER['REMOTE_ADDR'])) {
      $result['request'] = $_SERVER['REQUEST_METHOD'] . ' '
        . $event['request_uri'] . ' IP: ' . $event['ip']
        . ' REFERRER: ' . $event['referer'];
      $result['cookies'] = $this->argToString($_COOKIE);
      $result['user agent'] = $_SERVER['HTTP_USER_AGENT'];
      $result['headers'] = $this->argToString(headers_list());
    }
    else {
      $result['request'] = "Command line: " . (isset($_SERVER["_"])
          ? $_SERVER["_"] : '') . (isset($_SERVER["argv"]) ? implode(
          " ", $_SERVER["argv"]
        ) : '');
      $result['cookies'] = "";
      $result['user agent'] = "";
      $result['headers'] = "";
    }
    $result['headers']
      = "{$result['headers']}\n{$result['user agent']}\n{$result['cookies']}";

    return $result;
  }

  /**
   * @param $arg
   * @param int $limit
   * @param int $level
   * @return bool|float|int|string
   */
  private function argToString($arg, $limit = 1024, $level = 0) {
    if ($level >= self::MULTILOG_ARGTOSTRING_MAX_NESTING_LEVEL) {
      $result = '';
    }
    elseif (is_bool($arg)) {
      $result = $arg ? "true" : "false";
    }
    elseif (is_string($arg)) {
      $result = '"' . $arg . '"';
    }
    elseif (is_null($arg)) {
      $result = 'NULL';
    }
    elseif (is_scalar($arg)) {
      $result = $arg;
    }
    elseif (is_array($arg)) {
      if (!count($arg)) {
        $res = array('Array ()');
      }
      else {
        $res = array();
        $cnt = 128;
        foreach ($arg as $key => $item) {
          if ($cnt > 0) {
            $cnt--;
            $res[] = $key . ' = ' . $this->argToString(
                $item, $limit, $level + 1
              );
          }
          else {
            $res[] = '...';
          }
        }
      }

      $result = 'Array (' . implode(",", $res) . ')';
    }
    else {
      if (is_object($arg)) {
        $result = 'Object:' . get_class($arg);
      }
      else {
        $result = '';
      }
    }

    $result = $this->truncate_str($result, $limit);

    return $result;
  }

  /**
   * @param string $str
   * @param int $limit
   * @return string
   */
  private function truncate_str($str, $limit) {
    if (strlen($str) > $limit) {
      $str = substr($str, 0, $limit) . '...';
    }
    return $str;
  }

  /**
   * @param array $trace
   * @return string
   */
  private function parseTrace(array $trace) {
    $res = array();
    $i = 1;
    foreach ($trace as $level) {
      if ($i > 5) { // first 5 points of trace are omitted due no sense
        $res[] = '    ' . ($i - 5) . ". " . $this->parseTraceLevel(
            $level
          );
      }
      $i++;
    }
    $result = implode("\n", $res);
    return $result;
  }

  /**
   * @param array $traceLevel
   * @return string
   */
  private function parseTraceLevel(array $traceLevel) {
    $multilog_get_expand_trace_args = TRUE;
    $class = isset($traceLevel['class']) ? $traceLevel['class'] : '';
    $type = isset($traceLevel['type']) ? $traceLevel['type'] : '';
    $file = isset($traceLevel['file']) ? $traceLevel['file'] : '';
    $function = isset($traceLevel['function']) ? $traceLevel['function'] : '';
    $line = isset($traceLevel['line']) ? $traceLevel['line'] : '';
    if (!empty($traceLevel['args'])) {
      $args = array();
      foreach ($traceLevel['args'] as $arg) {
        if ((is_array($arg) && $multilog_get_expand_trace_args)
          || !is_array($arg)
        ) {
          $args[] = $this->argToString($arg, 2048);
        }
        else {
          $args[] = 'Array(' . count($arg) . ')';
        }
      }
      $arguments = implode(', ', $args);
    }
    else {
      $arguments = '';
    }

    $str = $class . $type . $function . '( ' . $arguments . " )  " .
      ($multilog_get_expand_trace_args ? /*"\n" . str_repeat(' ', 8)*/
        '' : "") . $file . ":" . $line;

    return $str;
  }
}
