<?php
function filelog_page_view() {
  $path = variable_get(
    'filelog_path', filelog_get_default_log_path()
  );

  $rows = array();
  if (is_dir($path)) {
    $files = scandir($path);
    foreach ($files as $filename) {
      if (in_array($filename, array('.', '..'))) {
        continue;
      }
      $tmp_path = "{$path}/{$filename}";
      if (is_dir($tmp_path)) {
        $rows[] = array(
          array('data' => "<b>$filename</b>", 'colspan' => 4)
        );

        $sub_files = scandir($tmp_path);
        foreach ($sub_files as $sub_file_name) {
          if (in_array($sub_file_name, array('.', '..'))) {
            continue;
          }
          $size = filesize("{$tmp_path}/{$sub_file_name}");
          $file_time = filemtime("{$tmp_path}/{$sub_file_name}");
          $url_enc = urlencode("{$filename}/{$sub_file_name}");
          $rows[] = array(
            $sub_file_name,
            multilog_simplify_number($size),
            format_date($file_time),
            l(
              t('View'), "admin/config/development/multilog/filelog/{$url_enc}",
              array('attributes' => array('target' => '_blank'))
            ),
          );
        }
        $rows[] = array(
          array('data' => "", 'colspan' => 4)
        );
      }
    }
  }

  return array(
    'log_list' => array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(
        t('	Log file name'),
        t('Size'),
        t('Modified'),
        t('Actions')
      )
    )
  );
}

function multilog_simplify_number($number) {
  if ($number < 1024) {
    $result = "$number bytes";
  }
  elseif ($number < 1024 * 1024) {
    $result = round($number / 1024, 1) . " Kbytes";
  }
  elseif ($number < 1024 * 1024 * 1024) {
    $result = round($number / (1024 * 1024), 1) . " Mbytes";
  }
  else {
    $result = round($number / (1024 * 1024 * 1024), 1) . " Gbytes";
  }
  return $result;
}

function filelog_file_view($path) {
  $decode_path = urldecode($path);
  $dir_path =   $path = variable_get(
    'filelog_path', filelog_get_default_log_path()
  );
  $log_path = "{$dir_path}/{$decode_path}";
  $output = '<pre>';
  if(is_readable($log_path)){
    $output .=  @file_get_contents($log_path);
  }
  $output .= '</pre>';
  return $output;
}
