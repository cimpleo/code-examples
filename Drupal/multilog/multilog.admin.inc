<?php
function multilog_admin_form($form, &$form_state) {
  $logger_list = module_invoke_all('logger_info');

  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['multilog_basic'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Basic settings'),
    '#group' => 'vertical_tabs',
    '#tree'  => TRUE
  );

  $loggers_option_list = array();

  foreach ($logger_list as $logger_key => $logger_info) {
    $loggers_option_list[$logger_key] = $logger_info['title'];
    $form[$logger_key] = array(
      '#type'  => 'fieldset',
      '#title' => $logger_info['title'],
      '#group' => 'vertical_tabs',
      '#tree' => TRUE
    );
    if (function_exists($logger_info['settings_callback'])) {
      $form[$logger_key]['settings'] = $logger_info['settings_callback']();
    }
  }

  $form['multilog_basic']['methods'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select loggers'),
    '#options'       => $loggers_option_list,
    '#default_value' => variable_get('multilog_methods', array())
  );

  $filters = variable_get('multilog_filters', multilog_get_default_filter());
  $form['multilog_basic']['filters_title'] = array(
    '#type'   => 'markup',
    '#markup' => '<div><b>' . t('Filters') . '</b></div>'
  );
  $form['multilog_basic']['filters'] = array(
    '#tree'  => TRUE,
    '#theme' => 'multilog_filters_table'
  );
  foreach ($filters as $filter) {
    $form['multilog_basic']['filters'][] = multilog_filter_forms($filter);
  }
  $form['multilog_basic']['filters']['new'] = multilog_filter_forms();

  $form['save'] = array(
    '#type'  => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

function multilog_filter_forms($filter = array()) {
  $form = array();

  if (!empty($filter)) {
    $form['delete'] = array(
      '#type'  => 'checkbox',
      '#title' => t('Delete')
    );
  }

  $form["key"] = array(
    '#title'         => t('Type'),
    '#type'          => 'textfield',
    '#default_value' => isset($filter['key']) ? $filter['key'] : '',
    '#size'          => 25,
    '#required'      => isset($filter['key']) ? TRUE : FALSE
  );

  $form['level'] = array(
    '#type'          => 'select',
    '#title'         => t('Level'),
    '#options'       => multilog_get_serenities_list(),
    '#default_value' => isset($filter['level']) ? $filter['level'] : 6
  );

  $form['format'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Format'),
    '#default_value' => isset($filter['format']) ? $filter['format'] : '',
    '#description'   => t(
      'Allowed values: %date %severity %type %message %trace %user %request %header %get&post %args %cookies'
    )
  );

  return $form;
}

function multilog_admin_form_submit($form, &$form_state) {
  variable_set(
    'multilog_methods',
    array_filter($form_state['values']['multilog_basic']['methods'])
  );

  $filters = array();
  foreach (
    $form_state['values']['multilog_basic']['filters'] as $key => $filter
  ) {
    if (is_int($key) && $filter['delete'] == 0) {
      unset($filter['delete']);
      $filters[] = $filter;
    }
  }
  if (!empty($form_state['values']['multilog_basic']['filters']['new']['key'])) {
    $filters[] = $form_state['values']['multilog_basic']['filters']['new'];
  }
  variable_set('multilog_filters', $filters);

  $logger_list = module_invoke_all('logger_info');
  foreach ($logger_list as $logger_key => $logger_info) {
    if (isset($form_state['values'][$logger_key]['settings'])
      && function_exists(
        $logger_info['settings_callback']
      )
    ) {
      $logger_info['settings_callback'](
        'save', $form_state['values'][$logger_key]['settings']
      );
    }
  }
}