<?php
/**
 * @file
 * ideaboost_init.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ideaboost_init_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_board-of-advisors:who-we-are/board-of-advisors
  $menu_links['main-menu_board-of-advisors:who-we-are/board-of-advisors'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'who-we-are/board-of-advisors',
    'router_path' => 'who-we-are/board-of-advisors',
    'link_title' => 'Board of Advisors',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_board-of-advisors:who-we-are/board-of-advisors',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_who-we-are:who-we-are',
  );
  // Exported menu link: main-menu_cfc-media-lab-team:who-we-are/cfc-media-lab-staff
  $menu_links['main-menu_cfc-media-lab-team:who-we-are/cfc-media-lab-staff'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'who-we-are/cfc-media-lab-staff',
    'router_path' => 'who-we-are/cfc-media-lab-staff',
    'link_title' => 'CFC Media Lab Team',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_cfc-media-lab-team:who-we-are/cfc-media-lab-staff',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_who-we-are:who-we-are',
  );
  // Exported menu link: main-menu_eligibility:node/74
  $menu_links['main-menu_eligibility:node/74'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/74',
    'router_path' => 'node/%',
    'link_title' => 'Eligibility',
    'options' => array(
      'attributes' => array(
        'title' => 'Eligibility',
      ),
      'identifier' => 'main-menu_eligibility:node/74',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_program:<front>',
  );
  // Exported menu link: main-menu_fall-2012-cohort:taxonomy/term/8
  $menu_links['main-menu_fall-2012-cohort:taxonomy/term/8'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/8',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Fall 2012 Cohort',
    'options' => array(
      'attributes' => array(
        'title' => 'Fall 2012 Cohort',
      ),
      'identifier' => 'main-menu_fall-2012-cohort:taxonomy/term/8',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'main-menu_portfolio:portfolio',
  );
  // Exported menu link: main-menu_fall-2013-cohort:taxonomy/term/6
  $menu_links['main-menu_fall-2013-cohort:taxonomy/term/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/6',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Fall 2013 Cohort',
    'options' => array(
      'attributes' => array(
        'title' => 'Fall 2013 Cohort',
      ),
      'identifier' => 'main-menu_fall-2013-cohort:taxonomy/term/6',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_portfolio:portfolio',
  );
  // Exported menu link: main-menu_fall-2014-cohort:taxonomy/term/4
  $menu_links['main-menu_fall-2014-cohort:taxonomy/term/4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/4',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Fall 2014 Cohort',
    'options' => array(
      'attributes' => array(
        'title' => 'Fall 2014 Cohort',
      ),
      'identifier' => 'main-menu_fall-2014-cohort:taxonomy/term/4',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_portfolio:portfolio',
  );
  // Exported menu link: main-menu_financing:financing
  $menu_links['main-menu_financing:financing'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'financing',
    'router_path' => 'financing',
    'link_title' => 'Financing',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_financing:financing',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_resources:tools',
  );
  // Exported menu link: main-menu_mentor-network:who-we-are/mentor-network
  $menu_links['main-menu_mentor-network:who-we-are/mentor-network'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'who-we-are/mentor-network',
    'router_path' => 'who-we-are/mentor-network',
    'link_title' => 'Mentor Network',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_mentor-network:who-we-are/mentor-network',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_who-we-are:who-we-are',
  );
  // Exported menu link: main-menu_overview:node/18
  $menu_links['main-menu_overview:node/18'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/18',
    'router_path' => 'node/%',
    'link_title' => 'Overview',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_overview:node/18',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_program:<front>',
  );
  // Exported menu link: main-menu_partners:partners
  $menu_links['main-menu_partners:partners'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'partners',
    'router_path' => 'partners',
    'link_title' => 'Partners',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_partners:partners',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_portfolio:portfolio
  $menu_links['main-menu_portfolio:portfolio'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'portfolio',
    'router_path' => 'portfolio',
    'link_title' => 'Portfolio',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_portfolio:portfolio',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_program:<front>
  $menu_links['main-menu_program:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Program',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_program:<front>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_program:node/19
  $menu_links['main-menu_program:node/19'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/19',
    'router_path' => 'node/%',
    'link_title' => 'Program',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_program:node/19',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_program:<front>',
  );
  // Exported menu link: main-menu_resources:tools
  $menu_links['main-menu_resources:tools'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tools',
    'router_path' => 'tools',
    'link_title' => 'Resources',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_resources:tools',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_spring-2013-cohort:taxonomy/term/7
  $menu_links['main-menu_spring-2013-cohort:taxonomy/term/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'taxonomy/term/7',
    'router_path' => 'taxonomy/term/%',
    'link_title' => 'Spring 2013 Cohort',
    'options' => array(
      'attributes' => array(
        'title' => 'Spring 2013 Cohort',
      ),
      'identifier' => 'main-menu_spring-2013-cohort:taxonomy/term/7',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'main-menu_portfolio:portfolio',
  );
  // Exported menu link: main-menu_tools:tools
  $menu_links['main-menu_tools:tools'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'tools',
    'router_path' => 'tools',
    'link_title' => 'Tools',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_tools:tools',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_resources:tools',
  );
  // Exported menu link: menu-footer-menu_about:about
  $menu_links['menu-footer-menu_about:about'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'about',
    'router_path' => 'about',
    'link_title' => 'About',
    'options' => array(
      'attributes' => array(
        'title' => 'About',
      ),
      'identifier' => 'menu-footer-menu_about:about',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_blog:http://stabletalk.cfcmedialab.com/category/cfc-media-lab/ideaboost/
  $menu_links['menu-footer-menu_blog:http://stabletalk.cfcmedialab.com/category/cfc-media-lab/ideaboost/'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'http://stabletalk.cfcmedialab.com/category/cfc-media-lab/ideaboost/',
    'router_path' => '',
    'link_title' => 'Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_blog:http://stabletalk.cfcmedialab.com/category/cfc-media-lab/ideaboost/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_ontact:node/75
  $menu_links['menu-footer-menu_ontact:node/75'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/75',
    'router_path' => 'node/%',
    'link_title' => 'Сontact',
    'options' => array(
      'attributes' => array(
        'title' => 'Сontact',
      ),
      'identifier' => 'menu-footer-menu_ontact:node/75',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_press:press
  $menu_links['menu-footer-menu_press:press'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'press',
    'router_path' => 'press',
    'link_title' => 'Press',
    'options' => array(
      'attributes' => array(
        'title' => 'Press',
      ),
      'identifier' => 'menu-footer-menu_press:press',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_privacy-policy:node/15
  $menu_links['menu-footer-menu_privacy-policy:node/15'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/15',
    'router_path' => 'node/%',
    'link_title' => 'Privacy Policy',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_privacy-policy:node/15',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-footer-menu_terms-of-use:node/17
  $menu_links['menu-footer-menu_terms-of-use:node/17'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/17',
    'router_path' => 'node/%',
    'link_title' => 'Terms of Use',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_terms-of-use:node/17',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('Blog');
  t('Board of Advisors');
  t('CFC Media Lab Team');
  t('Eligibility');
  t('Fall 2012 Cohort');
  t('Fall 2013 Cohort');
  t('Fall 2014 Cohort');
  t('Financing');
  t('Mentor Network');
  t('Overview');
  t('Partners');
  t('Portfolio');
  t('Press');
  t('Privacy Policy');
  t('Program');
  t('Resources');
  t('Spring 2013 Cohort');
  t('Terms of Use');
  t('Tools');
  t('Сontact');

  return $menu_links;
}
