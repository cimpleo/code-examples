<?php
/**
 * @file
 * ideaboost_init.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function ideaboost_init_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Fall 2014 Cohort',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '2ad5636a-dcc4-4c2e-8f0a-bf46536a4335',
    'vocabulary_machine_name' => 'cohort',
    'field_date_start' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
    'field_date_end' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Network Connect',
    'description' => '',
    'format' => 'full_html',
    'weight' => 1,
    'uuid' => '5a4eb6cd-11fb-457f-930f-ad856f32a9ea',
    'vocabulary_machine_name' => 'cohort',
    'field_date_start' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
    'field_date_end' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Fall 2013 Cohort',
    'description' => '',
    'format' => 'full_html',
    'weight' => 2,
    'uuid' => '80cc0863-c749-48f0-a451-5c8f1cf7b3fe',
    'vocabulary_machine_name' => 'cohort',
    'field_date_start' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
    'field_date_end' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Community Partner',
    'description' => 'Community Partner',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => '9ab3df58-88dd-415b-ab66-8c32b717ef0c',
    'vocabulary_machine_name' => 'partner_type',
  );
  $terms[] = array(
    'name' => 'Fall 2012 Cohort',
    'description' => '',
    'format' => 'full_html',
    'weight' => 4,
    'uuid' => 'a88d2147-9ea7-4b13-86a0-9d9ad92649d6',
    'vocabulary_machine_name' => 'cohort',
    'field_date_start' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
    'field_date_end' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Spring 2013 Cohort',
    'description' => '',
    'format' => 'full_html',
    'weight' => 3,
    'uuid' => 'b9367985-cc93-4297-8caf-a092ec659577',
    'vocabulary_machine_name' => 'cohort',
    'field_date_start' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
    'field_date_end' => array(
      'und' => array(
        0 => array(
          'value' => '2015-09-08 00:00:00',
          'timezone' => 'Asia/Omsk',
          'timezone_db' => 'Asia/Omsk',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Founding Partner',
    'description' => 'Founding Partner',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'cc93cb5b-ad75-4a8c-b005-2bab1c0d5783',
    'vocabulary_machine_name' => 'partner_type',
  );
  $terms[] = array(
    'name' => 'Support',
    'description' => '',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'e587487a-73d1-4f0a-804a-7666eb08aec6',
    'vocabulary_machine_name' => 'partner_type',
  );
  $terms[] = array(
    'name' => 'Program Partner',
    'description' => 'Program Partner',
    'format' => 'full_html',
    'weight' => 0,
    'uuid' => 'f8147d5f-6ce2-4319-bf2a-8b08e5a8a8b4',
    'vocabulary_machine_name' => 'partner_type',
  );
  return $terms;
}
