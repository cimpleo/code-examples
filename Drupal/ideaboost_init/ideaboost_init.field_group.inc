<?php
/**
 * @file
 * ideaboost_init.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ideaboost_init_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pane_info|fieldable_panels_pane|fieldable_panels_pane|full';
  $field_group->group_name = 'group_pane_info';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'fieldable_panels_pane';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pane Info',
    'weight' => '0',
    'children' => array(
      0 => 'field_content',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Pane Info',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-pane-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_pane_info|fieldable_panels_pane|fieldable_panels_pane|full'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pane_info|fieldable_panels_pane|fieldable_panels_pane|preview';
  $field_group->group_name = 'group_pane_info';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'fieldable_panels_pane';
  $field_group->mode = 'preview';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Pane Info',
    'weight' => '1',
    'children' => array(
      0 => 'field_content',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Pane Info',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-pane-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_pane_info|fieldable_panels_pane|fieldable_panels_pane|preview'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_partner_founding|node|partner|partners_founding';
  $field_group->group_name = 'group_partner_founding';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partner';
  $field_group->mode = 'partners_founding';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Partner Founding',
    'weight' => '9',
    'children' => array(
      0 => 'body',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Partner Founding',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-partner-founding field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_partner_founding|node|partner|partners_founding'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slide_info|node|slider_slide|slide';
  $field_group->group_name = 'group_slide_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slider_slide';
  $field_group->mode = 'slide';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slide Info',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Slide Info',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-slide-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_slide_info|node|slider_slide|slide'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_wrapper|node|partner|partners_founding';
  $field_group->group_name = 'group_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partner';
  $field_group->mode = 'partners_founding';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '0',
    'children' => array(
      0 => 'field_image',
      1 => 'field_url_link',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_wrapper|node|partner|partners_founding'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_wrapper|node|partner|partners_program_community';
  $field_group->group_name = 'group_wrapper';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'partner';
  $field_group->mode = 'partners_program_community';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Wrapper',
    'weight' => '0',
    'children' => array(
      0 => 'field_image',
      1 => 'field_url_link',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Wrapper',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-wrapper field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_wrapper|node|partner|partners_program_community'] = $field_group;

  return $export;
}
