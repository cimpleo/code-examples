<?php
/**
 * @file
 * ideaboost_init.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ideaboost_init_taxonomy_default_vocabularies() {
  return array(
    'cohort' => array(
      'name' => 'Cohort',
      'machine_name' => 'cohort',
      'description' => 'Cohort',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'partner_type' => array(
      'name' => 'Partner type',
      'machine_name' => 'partner_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
