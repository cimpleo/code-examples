<?php
/**
 * @file
 * compliance_training.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function compliance_training_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function compliance_training_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function compliance_training_node_info() {
  $items = array(
    'training' => array(
      'name' => t('Training'),
      'base' => 'node_content',
      'description' => t('Add a new training page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'training_adviser' => array(
      'name' => t('Training Adviser'),
      'base' => 'node_content',
      'description' => t('An adviser for trainings.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
