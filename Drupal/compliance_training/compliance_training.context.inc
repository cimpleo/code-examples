<?php
/**
 * @file
 * compliance_training.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function compliance_training_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'training_list';
  $context->description = 'This context is only for the main Trainings list page.';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'training' => 'training',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'compliance_training-compliance_training_term_group' => array(
          'module' => 'compliance_training',
          'delta' => 'compliance_training_term_group',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('FTLF');
  t('This context is only for the main Trainings list page.');
  $export['training_list'] = $context;

  return $export;
}
