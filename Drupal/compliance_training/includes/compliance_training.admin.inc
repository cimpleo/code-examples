<?php
/**
 * @file
 * compliance_training.admin.inc
 */

function compliance_training_form_compliance_structure_config_alter (&$form, &$form_state) {
  $form['trainings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Trainings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'global',
  );

  $form['trainings']['upcoming'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upcoming Block'),
    '#description' => t('These settings all pertain to the Upcoming Training Events block on the homepage.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['trainings']['upcoming']['compliance_training_upcoming_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('This will be the title of the block.'),
    '#default_value' => variable_get('compliance_training_upcoming_title',''),
  );

  $form['trainings']['upcoming']['compliance_training_upcoming_more'] = array(
    '#type' => 'textfield',
    '#title' => t('More Text'),
    '#description' => t('This will be the text displayed in the "More" link.'),
    '#default_value' => variable_get('compliance_training_upcoming_more',''),
  );

  $form['trainings']['promobox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Promo Box'),
    '#description' => t('These settings all pertain to the Promo box that appears on the main Trainings list page.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['trainings']['promobox']['compliance_training_promo_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('This will be the title of the box.'),
    '#default_value' => variable_get('compliance_training_promo_title',''),
  );

  $promo_text = variable_get('compliance_training_promo_text', '');
  $form['trainings']['promobox']['compliance_training_promo_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Promo Text'),
    '#description' => t('This text is the main text in the promo box.'),
    '#default_value' => empty($promo_text) ? '': $promo_text['value'],
  );

  $pl =  variable_get('compliance_training_promo_lk','');
  $form['trainings']['promobox']['customcclink_compliance_training_promo_lk_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Promo Link Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => (empty($pl) || empty($pl['title'])) ? '': $pl['title'],
  );

  $form['trainings']['promobox']['customcclink_compliance_training_promo_lk_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Promo Link Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => (empty($pl) || empty($pl['link'])) ? '': $pl['link'],
  );
}
