<?php
/**
 * @file
 * compliance_training.block.inc
 */

/**
 * Implements hook_block_info
 */
function compliance_training_block_info() {
  $blocks = array();

  $blocks['compliance_training_term_group'] = array(
    'info' => t('Training: Term group blocks'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  $blocks['compliance_training_upcoming'] = array(
    'info' => t('Training: Upcoming Trainings'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function compliance_training_block_view($delta = '', $context = '') {
  $block = NULL;

  // View blocks.
  $callback = "_compliance_training_block_view_{$delta}";
  if (function_exists($callback)) {
    $block = $callback($delta, $context);
  }

  return $block;
}

/**
 * Block view function for Term groups block.
 */
function _compliance_training_block_view_compliance_training_term_group(&$delta,&$context) {
  // Load our vocab and tree.
  $vocab = taxonomy_vocabulary_machine_name_load('training_type');
  $tree = taxonomy_get_tree($vocab->vid);

  // Bail out if something's missing.
  if (empty($vocab) || empty($tree)) {
    return FALSE;
  }

  // Build the base efq.
  $base = new EntityFieldQuery();
  $base->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'training')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->propertyCondition('promote', NODE_PROMOTED)
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 2)
    ->addTag('compliance_training_term_group');

  $items = array();
  // Now loop through the tree and get results from each term.
  foreach ($tree as $term) {
    // First add the field condition, or change it.
    if (!empty($base->fieldConditions)) {
      foreach ($base->fieldConditions as $index => $condition) {
        if ($condition['field']['field_name'] == 'field_training_type') {
         $base->fieldConditions[$index]['value'] = $term->tid;
        }
      }
    } else {
      $base->fieldCondition('field_training_type', 'tid', $term->tid);
    }
    $results = $base->execute();

    // Create links to results.
    if (!empty($results['node'])) {
      $links = array();
      foreach ($results['node'] as $nid => $efqnode) {
        $node = node_load($nid);
        $links[] = l($node->title,'node/'.$nid);
      }

      // Add box into the group.
      $items[] = array(
        'class' => 'term-box',
        'title' => $term->name,
        'text' => $term->description,
        'view_all' => l('View All','training/'.$term->tid,array('attributes' => array('class' => array('view-all')))),
        'links' => $links,
      );
    }
  }

  // Now add the promo subscriber box at the end.
  $promo_text = variable_get('compliance_training_promo_text', '');
  $pl =  variable_get('compliance_training_promo_lk','');
  $promo_array = array(
    'class' => 'term-box promo',
    'title' => variable_get('compliance_training_promo_title',''),
    'text' =>  empty($promo_text) ? '': $promo_text['value'],
  );
  if (!empty($pl['link'])) {
    $link_title = empty($pl) ? '': $pl['title'];
    $promo_array['links'] = l($link_title,$pl['link'],array('attributes' => array('class' => array('learn-more'))));
  }
  $items[] = $promo_array;

  // Now, lets create the markup for the boxes. This technically could be translated into a theme function.
  // But I don't think we need to do it right now --Paulm 3/6/15
  $markup = '';
  foreach ($items as $box) {// Check for row.
    $temp = '';
    $temp .= '<div class="'.$box['class'].'">';
    if (!empty($box['view_all'])) {
      $temp .= $box['view_all'];
    }

    $temp .= '<h2>'.$box['title'].'</h2>';
    $temp .= '<div class="box-text">'.$box['text'].'</div>';
    $temp .= '<div class="links">';

    if (!empty($box['links'])) {
      if (is_array($box['links'])) {
        $temp .= theme('item_list', array('items' => $box['links']));
      }
      else {
        $temp .= $box['links'];
      }
    }

    $temp .= '</div>';
    $temp .= '</div>';

    // Add box markup to temp.
    $markup .= $temp;
  }

  $block = array();
  $block['content'] = array(
    '#markup' => $markup,
  );

  return $block;
}

/**
 * View callback for upcoming trainings block found on homepage.
 */
function _compliance_training_block_view_compliance_training_upcoming(&$delta,&$context) {
  $block = array();

  // Build the Query.
  // Build the base efq.
  $efq = new EntityFieldQuery();
  $efq->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'training')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldOrderBy('field_weight', 'value', 'ASC')
    ->propertyOrderBy('created', 'DESC')
    ->range(0, 4)
    ->addTag('compliance_training_upcoming');

  $results = $efq->execute();

  $items = array();
  if (!empty($results['node'])) {
    // Build markup.
    $nids = array_keys($results['node']);
    foreach ($nids as $nid) {
      $node = node_load($nid);
      $node_url = l($node->title,'node/'.$nid);
      $loctime_array = field_view_field('node',$node,'field_location_date',array('label' => 'hidden'));
      $loctime = empty($loctime_array) ? '' : render($loctime_array);
      $items[] = $node_url.$loctime;
    }
  } else {
    $items[] = 'There are currently no events scheduled.';
  }
  $markup = theme('item_list',array('items' => $items));
  $more_text = variable_get('compliance_training_upcoming_more','More Training Events');
  $markup .= l($more_text,'training',array('attributes' => array('class' => array('more'))));

  $upcoming_title = variable_get('compliance_training_upcoming_title','Upcoming Training Events');
  $block['subject'] = $upcoming_title;
  $block['content'] = array(
    '#markup' => $markup,
  );

  return $block;
}
