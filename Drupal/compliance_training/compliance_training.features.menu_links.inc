<?php
/**
 * @file
 * compliance_training.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function compliance_training_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_trainings:training
  $menu_links['main-menu_trainings:training'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'training',
    'router_path' => 'training',
    'link_title' => 'Trainings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_trainings:training',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Trainings');


  return $menu_links;
}
