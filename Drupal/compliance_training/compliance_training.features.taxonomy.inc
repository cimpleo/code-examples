<?php
/**
 * @file
 * compliance_training.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function compliance_training_taxonomy_default_vocabularies() {
  return array(
    'training_type' => array(
      'name' => 'Training Type',
      'machine_name' => 'training_type',
      'description' => 'Types of training\'s that are available.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
