(function ($) {

  Drupal.behaviors.customViewModule = {
    attach: function (context, settings) {

        var latLng = new google.maps.LatLng(Drupal.settings.customViewModule.lat, Drupal.settings.customViewModule.lng);
        var map;
        function initialize() {
            var mapOptions = {
                zoom: 17,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };        
            
            map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
            
            var image = '/' + Drupal.settings.customViewModule.path + '/templates/images/mapicon-single.png';
            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                icon: image
            });
        }
        
        google.maps.event.addDomListener(window, 'load', initialize);
        
    }
  };

})(jQuery);