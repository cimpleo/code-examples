(function ($) {

  Drupal.behaviors.objectCarousel = {
    attach: function (context, settings) {
        
         $('.carousel').bxSlider({
            slideWidth: 425,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideMargin: 80,
            prevText: '<',
            nextText: '>',
        });
    }    
  };

})(jQuery);