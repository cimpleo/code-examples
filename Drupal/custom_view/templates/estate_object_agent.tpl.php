<?php
$agent = $variables['agent'];
?>
<div class="agent-main-info">
    
    <div class="agent-photo">
        
    </div>
    
    <div class="main-info-wrapper">
    
        <h2 class="agent-name"><?php print_r($agent->ip_usr_vorname.' '.$agent->ip_usr_nachname); ?></h2>
        <div class="agent-function"><?php print t($agent->ip_usr_funktion); ?></div>
        <div class="providers-office"><?php print t($agent->buero); ?></div>
        <div class="providers-street"><?php print t($agent->address_street); ?></div>
        <div class="providers-postalcode"><?php print t($agent->address_postalCode); ?></div>
        <div class="providers-city"><?php print t($agent->address_city); ?></div>
        <div class="providers-phone"><label><?php print t('Phone:'); ?></label><?php print t($agent->phone).' | '; ?></div>
        <div class="providers-mobilephone"><label><?php print t('Mob. Phone:'); ?></label><?php print t($agent->ip_usr_mobilePhone); ?></div>
        <div id="agent-dialog"></div>       
    </div>
    
</div>

<div class="agent-links">
    <a class="agent-estate-objects" href="/property-results/list?email=<?php print($agent->Webserver___EMail_P);?>"><?php print t('See').' '.$agent->estate_count.' '.t(' available real estate').'<span> ></span>'; ?></a>
    <a class="agent-estate-objects" href="<?php print($agent->websiteUrl);?>"><?php print t('See my site').'<span> ></span>'; ?></a>
</div>

<div class="contact-link">
    <a href="#"><?php print t('Contacts'); ?></a>
</div>

<div class="agent-details">

    <div class="estate-info">
        <div class="internal">
            <?php print t($agent->INTERN); ?>
        </div>
        
        <div class="internal">
            <?php print t($agent->Land_PLZ_Ort); ?>
        </div>
        
        <div class="cost">    
            <label><?php print t('&#8364;'); ?></label><?php print t($agent->expression); ?>
        </div>
        
        
        <div class="room">
            <?php print t($agent->Zimmer); unset($agent->Zimmer); ?><label><?php print t('Rooms'); ?></label>
        </div>

        <div class="live-area">
            <?php print t($agent->Wohnflaeche); unset($agent->Wohnflaeche); ?><label><?php print t('Sq Feet'); ?></label>
        </div>
    </div>    
    
    <div class="agent-description">
        
        <div class="description">
            <?php if($agent->ip_usr_description) print t($agent->ip_usr_description); ?>
        </div>
        
        <div class="knowledge">
            <?php if($agent->ip_usr_fachwissen) :?><label><?php print t('Specialist knowledge'); ?></label><?php print t($agent->ip_usr_fachwissen); ?><?php endif; ?>
        </div>
        
        <div class="credentials">
            <?php if($agent->ip_usr_referenzen) :?><label><?php print t('Credentials'); ?></label><?php print t($agent->ip_usr_referenzen); ?><?php endif; ?>
        </div>
        
        <a href="#" class="top-link"><?php print t('Top page^'); ?></a>
        
    </div>
    
</div>