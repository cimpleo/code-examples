<?php
/**
 * @file
 * compliance_uber.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function compliance_uber_default_rules_configuration() {
  $items = array();
  $items['uc_checkout_admin_notification'] = entity_import('rules_config', '{ "uc_checkout_admin_notification" : {
      "LABEL" : "E-mail admin checkout notification",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules", "uc_order" ],
      "ON" : [ "uc_checkout_complete" ],
      "DO" : [
        { "uc_order_email_invoice" : {
            "order" : [ "order" ],
            "from" : "\\u0022www.HealthCenterCompliance.com\\u0022 \\u003Cinfo@healthcentercompliance.com\\u003E",
            "addresses" : "info@healthcentercompliance.com\\r\\nmurphy@bluebellmarketing.com",
            "subject" : "New Order at [store:name]",
            "template" : "admin",
            "view" : "admin-mail"
          }
        }
      ]
    }
  }');
  $items['uc_checkout_customer_notification'] = entity_import('rules_config', '{ "uc_checkout_customer_notification" : {
      "LABEL" : "E-mail customer checkout notification",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules", "uc_order" ],
      "ON" : [ "uc_checkout_complete" ],
      "DO" : [
        { "uc_order_email_invoice" : {
            "order" : [ "order" ],
            "from" : "\\u0022www.HealthCenterCompliance.com\\u0022 \\u003Cinfo@healthcentercompliance.com\\u003E",
            "addresses" : "[order:email]",
            "subject" : "Your Order at [store:name]",
            "template" : "customer",
            "view" : "checkout-mail"
          }
        }
      ]
    }
  }');
  return $items;
}
