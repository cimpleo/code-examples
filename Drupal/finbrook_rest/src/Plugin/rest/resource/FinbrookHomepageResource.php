<?php
/**
 * @file
 * Create the homepage REST resource.
 */

namespace Drupal\finbrook_rest\Plugin\rest\resource;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a resource to get bundles by entity.
 *
 * @RestResource(
 *   id = "finbrook_rest_homepage",
 *   label = @Translation("Finbrook Homepage"),
 *   uri_paths = {
 *     "canonical" = "/api/homepage"
 *   }
 * )
 */
class FinbrookHomepageResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * A instance of the alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user,
    AliasManagerInterface $alias_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->entityManager = $entity_manager;
    $this->currentUser = $current_user;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns homepage data.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   A HTTP Exception.
   */
  public function get() {
    $test = array();
    $sliders = array();

    $node = \Drupal\node\Entity\Node::load(1);

    /* @var $url \Drupal\Core\Url */
    $url = Url::fromUri($node->get('field_notification_link_url')->uri);
    if ($url->isExternal()) {
      $uri = $url->getUri();
    }
    else {
      try {
        $uri = $url->getInternalPath();
      }
      catch (\UnexpectedValueException $e) {
        $uri = '';
      }
    }
    foreach ($node->get('field_sliders') as $item) {
      if ($item->entity->get('field_visible')->value == '1') {
        array_push($sliders, [
          'id' => (int)$item->entity->get('id')->value,
          'body' => trim(preg_replace('/\s+/', ' ', $item->entity->get('field_body')->value)),
          'image' => $item->entity->get('field_image')->entity->url(),
          'opacity' => (float)$item->entity->get('field_background_color')[0]->opacity,
          'color' => $item->entity->get('field_background_color')[0]->color,
        ]);
      }
    }
    $newimg = null;
    $img = $node->get('field_promotion_image')->entity;
    if (!empty($img))
    {
      $newimg = ImageStyle::load('promotion')->buildUrl($img->uri->value);
    }
    $test['title'] = $node->get('title')->value;
    $test['notificationLinkTitle'] = $node->get('field_notification_link_title')->value;
    $test['notificationLinkURL'] = $this->aliasManager->getAliasByPath("/$uri");
    $test['notificationMessage'] = trim(preg_replace('/\s+/', ' ', $node->get('field_notification_message')->value));
    $test['showNotificationLink'] = (boolval($node->get('field_show_link_in_notification')->value) ? true : false);
    $test['showNotification'] = (boolval($node->get('field_show_notification_box')->value) ? true : false);
    $test['promotionImage'] = $newimg;
    $test['promotionMessage'] = trim(preg_replace('/\s+/', ' ', $node->get('field_promotion_message')->value));
    $test['sliders'] = $sliders;
    // Return response
    $response = new ResourceResponse($test);

    return $response;
  }
  
}