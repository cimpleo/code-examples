<?php
/**
 * @file
 * Create the homepage REST resource.
 */

namespace Drupal\finbrook_rest\Plugin\rest\resource;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Cache\CacheableResponseInterface;

/**
 * Provides a resource to get bundles by entity.
 *
 * @RestResource(
 *   id = "finbrook_rest_homepage_articles",
 *   label = @Translation("Finbrook Homepage Articles"),
 *   uri_paths = {
 *     "canonical" = "/api/homepage/articles"
 *   }
 * )
 */
class FinbrookHomepageArticlesResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * A instance of the alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user,
    AliasManagerInterface $alias_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->entityManager = $entity_manager;
    $this->currentUser = $current_user;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns articles data for front page.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   A HTTP Exception.
   */
  public function get() {
    $test = [];
    $allnids = [];
    $test['MainSection'] = [];
    $test['NewsFooter'] = [];
    $request = \Drupal::request();
    $path = $request->getUri();

    $connection = \Drupal\Core\Database\Database::getConnection();
    $published_state = $connection
      ->select('content_moderation_state_field_data', 'state')
      ->fields('state', ['content_entity_id'])
      ->condition('state.moderation_state', 'published')
      ->execute()
      ->fetchAllKeyed(0,0);
    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'article')
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->condition('nid', $published_state, 'IN')
      ->range(0, 3)
      ->execute();
    $allnids = array_merge($allnids, $nids);

    $articles = \Drupal\node\Entity\Node::loadMultiple($nids);
    foreach ($articles as $item) {
      $nid = $item->nid->value;
      /* @var $url \Drupal\Core\Url */
      $uri = Url::fromRoute('entity.node.canonical', ['node' => $nid])->getInternalPath();
      $newimg = null;
      $img = $item->field_teaser_image->entity;
      if (!empty($img)) {
        $newimg = ImageStyle::load('teaser')->buildUrl($img->field_image->entity->uri->value);
      }
      array_push($test['MainSection'], [
        'id' => (int)$nid,
        'title' => $item->title->value,
        'created' => (new \DateTime())->setTimestamp($item->get('created')->value)->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:sP'),
        'image' => $newimg,
        'category' => $item->get('field_category')->entity->name->value,
        'categoryClass' => $item->get('field_category')->entity->field_class->value,
        'path' => $this->aliasManager->getAliasByPath("/$uri")
      ]);
    }
    $tids = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', "article_categories")
      ->sort('weight', 'ASC')
      ->execute();

    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);

    foreach($terms as $term) {
      $termname = $term->toLink()->getText();
      $nids = \Drupal::entityQuery('node')
        ->condition('type', 'article')
        ->condition('status', 1)
        ->condition('field_category', $term->tid->value)
        ->condition('nid', $published_state, 'IN')
        ->sort('created', 'DESC')
        ->condition('nid', $allnids, 'NOT IN')
        ->range(0, 3)
        ->execute();
      $allnids = array_merge($allnids, $nids);

      $articles = \Drupal\node\Entity\Node::loadMultiple($nids);

      $test['Section1'][$termname] = [];

      foreach ($articles as $item) {
        $nid = $item->nid->value;
        /* @var $url \Drupal\Core\Url */
        $uri = Url::fromRoute('entity.node.canonical', ['node' => $nid])->getInternalPath();
        $newimg = null;
        $img = $item->field_teaser_image->entity;
        if (!empty($img)) {
          $newimg = ImageStyle::load('teaser')->buildUrl($img->field_image->entity->uri->value);
        }
        array_push($test['Section1'][$termname], [
          'id' => (int)$nid,
          'title' => $item->title->value,
          'created' => (new \DateTime())->setTimestamp($item->created->value)->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:sP'),
          'category' => $item->field_category->entity->name->value,
          'categoryClass' => $item->field_category->entity->field_class->value,
          'image' => $newimg,
          'path' => $this->aliasManager->getAliasByPath("/$uri")
        ]);
      }
    }

    $nids = \Drupal::entityQuery('node')
      ->condition('type', 'article')
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->condition('nid', $published_state, 'IN')
      ->condition('nid', $allnids, 'NOT IN')
      ->range(0, 4)
      ->execute();
    $articles = \Drupal\node\Entity\Node::loadMultiple($nids);
    foreach ($articles as $item) {
      $nid = $item->nid->value;
      /* @var $url \Drupal\Core\Url */
      $uri = Url::fromRoute('entity.node.canonical', ['node' => $nid])->getInternalPath();
      $newimg = null;
      $img = $item->field_teaser_image->entity;
      if (!empty($img))
      {
        $newimg = ImageStyle::load('teaser')->buildUrl($img->field_image->entity->uri->value);
      }
      array_push($test['NewsFooter'], [
        'id' => (int)$nid,
        'title' => $item->title->value,
        'summary' => $item->body->summary,
        'created' => (new \DateTime())->setTimestamp($item->created->value)->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:sP'),
        'image' => $newimg,
        'category' => $item->field_category->entity->name->value,
        'categoryClass' => $item->field_category->entity->field_class->value,
        'path' => $this->aliasManager->getAliasByPath("/$uri")
      ]);
    }

    $response = new ResourceResponse($test);
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency(new FinbrookHomepageArticlesCacheableDependency($path));
    }
    return $response;
  }

}
