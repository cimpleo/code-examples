<?php
/**
 * Created by PhpStorm.
 * User: breidert
 * Date: 19.06.17
 * Time: 14:26
 */

namespace Drupal\finbrook_rest\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableDependencyInterface;

class FinbrookHomepageArticlesCacheableDependency implements CacheableDependencyInterface {

  /**
   * Maximum depth parameter
   *
   * @var string
   */
  protected $slug = '';

  /**
   * FinbrookHomepageArticlesCacheableDependency constructor.
   *
   * @param $slug
   */
  public function __construct($slug) {
    $this->slug = $slug;
  }


  /**
   * {@inheritdoc}
   *
   * @return array
   */
  public function getCacheContexts() {
    $contexts = [];
    // URL parameters as contexts
    if($this->slug != '') {
      $contexts[] = 'url.query_args';
    }
    return $contexts;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   */
  public function getCacheTags() {
    $tags = [];
    return $tags;
  }

  /**
   * {@inheritdoc}
   *
   * @return int
   */
  public function getCacheMaxAge() {
    // Default to 1 hour
    return 3600;
  }

}
