<?php
/**
 * @file
 * Create the homepage REST resource.
 */

namespace Drupal\finbrook_rest\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a resource to get bundles by entity.
 *
 * @RestResource(
 *   id = "finbrook_rest_article",
 *   label = @Translation("Finbrook Article"),
 *   uri_paths = {
 *     "canonical" = "/api/articles"
 *   }
 * )
 */
class FinbrookArticleResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * A instance of the alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The relative path to article.
   *
   * @var string
   */
  protected $path = '';

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user,
    AliasManagerInterface $alias_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->entityManager = $entity_manager;
    $this->currentUser = $current_user;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   A HTTP Exception.
   */
  public function get() {
    // Get the current request.
    $request = \Drupal::request();
    $path = $request->get('path');
    if (!empty($path)) {
      $this->path = $path;
    }
    $article = [];
    $uri = $this->aliasManager->getPathByAlias($this->path);
    if (preg_match('/node\/(\d+)/', $uri, $matches)) {
      $node = \Drupal\node\Entity\Node::load($matches[1]);
      $uid = $node->getOwnerId();
      $user = \Drupal\user\Entity\User::load((int)$uid);
      $username = $user->getDisplayName();
      $article['id'] = (int)$node->get('nid')->value;
      $article['title'] = $node->get('title')->value;
      $article['created'] = (new \DateTime())->setTimestamp($node->get('created')->value)->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:sP');
      $article['updated'] = (new \DateTime())->setTimestamp($node->get('changed')->value)->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:sP');
      $article['author'] = $username;
      $article['body'] = check_markup($node->get('body')->value, $node->get('body')->format);
    }
    else {
      throw new NotFoundHttpException('Articles not found');
    }

    $response = new ResourceResponse($article);

    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency(new FinbrookArticleCacheableDependency($this->path));
    }

    return $response;
  }

}
