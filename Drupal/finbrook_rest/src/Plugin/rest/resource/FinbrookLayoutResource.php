<?php
/**
 * @file
 * Create the menu item REST resource.
 */

namespace Drupal\finbrook_rest\Plugin\rest\resource;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get bundles by entity.
 *
 * @RestResource(
 *   id = "finbrook_rest_layout",
 *   label = @Translation("Finbrook Layout"),
 *   uri_paths = {
 *     "canonical" = "/api/layout"
 *   }
 * )
 */
class FinbrookLayoutResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * A instance of entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * A instance of the alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * A list of menu items.
   *
   * @var array
   */
  protected $menuItems = [];

  /**
   * A list of footer menu items.
   *
   * @var array
   */
  protected $footerMenuItems = [];

  /**
   * The maximum depth we want to return the tree.
   *
   * @var int
   */
  protected $maxDepth = 0;

  /**
   * The minimum depth we want to return the tree from.
   *
   * @var int
   */
  protected $minDepth = 1;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityManagerInterface $entity_manager,
    AccountProxyInterface $current_user,
    AliasManagerInterface $alias_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->entityManager = $entity_manager;
    $this->currentUser = $current_user;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity.manager'),
      $container->get('current_user'),
      $container->get('path.alias_manager')
    );
  }

  /**
   * Responds to GET requests.
   *
   * Returns a list of menu items for specified menu name.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The response containing a list of bundle names.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   A HTTP Exception.
   */
  public function get() {
      // Setup variables.
      $this->setup();
      $test = array();

      // Create the parameters.
      $parameters = new MenuTreeParameters();
      $parameters->onlyEnabledLinks();

      if (!empty($this->maxDepth)) {
        $parameters->setMaxDepth($this->maxDepth);
      }

      if (!empty($this->minDepth)) {
        $parameters->setMinDepth($this->minDepth);
      }

      // Load the tree based on this set of parameters.
      $menu_tree = \Drupal::menuTree();

      // Transform the tree using the manipulators you want.
      $manipulators = [
        // Only show links that are accessible for the current user.
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        // Use the default sorting of menu links.
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];

      $tree = $menu_tree->load('top-navigation', $parameters);
      $tree = $menu_tree->transform($tree, $manipulators);
      $menu = $menu_tree->build($tree);
      $this->getMenuItems($menu['#items'], $this->menuItems);
      $test['topMenu'] = array_values($this->menuItems);

      $tree = $menu_tree->load('bottom-navigation', $parameters);
      $tree = $menu_tree->transform($tree, $manipulators);
      $menu = $menu_tree->build($tree);
      $this->getMenuItems($menu['#items'], $this->footerMenuItems);
      $test['bottomMenu'] = array_values($this->footerMenuItems);
    
      // Return response
      $response = new ResourceResponse($test);

      // Configure caching for minDepth and maxDepth parameters
      if ($response instanceof CacheableResponseInterface) {
        $response->addCacheableDependency(new RestMenuItemsCachableDepenency($this->minDepth, $this->maxDepth));
      }

      return $response;
  }

  /**
   * Generate the menu tree we can use in JSON.
   *
   * @param array $tree
   *   The menu tree.
   * @param array $items
   *   The already created items.
   */
  protected function getMenuItems(array $tree, array &$items = []) {
    foreach ($tree as $item_value) {
      /* @var $org_link \Drupal\Core\Menu\MenuLinkDefault */
      $org_link = $item_value['original_link'];

      // Set name to uuid or base id.
      $item_name = $org_link->getDerivativeId();
      if (empty($item_name)) {
        $item_name = $org_link->getBaseId();
      }

      /* @var $url \Drupal\Core\Url */
      $url = $item_value['url'];

      $external = FALSE;
      if ($url->isExternal()) {
        $uri = $url->getUri();
        $external = TRUE;
        $absolute = $uri;
      }
      else {
        try {
          $uri = $url->getInternalPath();
          $absolute = Url::fromUri('internal:/' . $uri, ['absolute' => TRUE])
                         ->toString(TRUE)
                         ->getGeneratedUrl();
        }
        catch (\UnexpectedValueException $e) {
          $uri = '';
        }
      }

      $alias = $this->aliasManager->getAliasByPath("/$uri");

      $items[$item_name] = [
        'key'      => $item_name,
        'title'    => $org_link->getTitle(),
        'uri'      => $uri,
        'alias'    => ltrim($alias, '/'),
        'external' => $external,
        'absolute' => $absolute,
        'weight'   => $org_link->getWeight(),
        'expanded' => $org_link->isExpanded(),
        'enabled'  => $org_link->isEnabled(),
      ];

      if (!empty($item_value['below'])) {
        $items[$item_name]['below'] = [];
        $this->getMenuItems($item_value['below'], $items[$item_name]['below']);
      }
    }
  }

  /**
   * This function is used to generate some variables we need to use.
   *
   * These variables are available in the url.
   */
  private function setup() {
    // Get the current request.
    $request = \Drupal::request();

    // Get and set the max depth if available.
    $max = $request->get('max_depth');
    if (!empty($max)) {
      $this->maxDepth = $max;
    }

    // Get and set the min depth if available.
    $min = $request->get('min_depth');
    if (!empty($min)) {
      $this->minDepth = $min;
    }

  }

}
