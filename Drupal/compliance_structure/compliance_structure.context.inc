<?php
/**
 * @file
 * compliance_structure.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function compliance_structure_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'breadcrumb';
  $context->description = 'Allow for breadcrumbs to be added to pages of the site.';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'plan' => 'plan',
        'risk_area' => 'risk_area',
        'service' => 'service',
        'training' => 'training',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'products' => 'products',
        'plans' => 'plans',
        'training' => 'training',
        'user' => 'user',
        'user/*' => 'user/*',
        'cart' => 'cart',
        'checkout' => 'checkout',
        'checkout/*' => 'checkout/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'compliance_structure-compliance_structure_breadcrumb' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_breadcrumb',
          'region' => 'preface_first',
          'weight' => '-54',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Allow for breadcrumbs to be added to pages of the site.');
  t('FTLF');
  $export['breadcrumb'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage';
  $context->description = 'Homepage only context.';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-45' => array(
          'module' => 'block',
          'delta' => '45',
          'region' => 'content',
          'weight' => '-10',
        ),
        'compliance_training-compliance_training_upcoming' => array(
          'module' => 'compliance_training',
          'delta' => 'compliance_training_upcoming',
          'region' => 'content',
          'weight' => '-9',
        ),
        'compliance_structure-compliance_structure_home_main' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_home_main',
          'region' => 'preface_first',
          'weight' => '-10',
        ),
        'compliance_structure-compliance_structure_home_advice' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_home_advice',
          'region' => 'preface_first',
          'weight' => '-9',
        ),
        'compliance_structure-compliance_structure_home_stay' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_home_stay',
          'region' => 'preface_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('FTLF');
  t('Homepage only context.');
  $export['homepage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'set_page_title';
  $context->description = 'Add page title to select pages.';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user' => 'user',
        'user/*' => 'user/*',
        'users' => 'users',
        'users/*' => 'users/*',
        'cart' => 'cart',
        'checkout/*/complete' => 'checkout/*/complete',
        '~user/*/subscriber' => '~user/*/subscriber',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'compliance_structure-compliance_structure_title' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_title',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Add page title to select pages.');
  t('FTLF');
  $export['set_page_title'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = 'Global context for FTLF';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'branding',
          'weight' => '-10',
        ),
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'compliance_structure-compliance_structure_ph_text' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_ph_text',
          'region' => 'header_first',
          'weight' => '-10',
        ),
        'compliance_structure-compliance_structure_emailsignup' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_emailsignup',
          'region' => 'footer_first',
          'weight' => '-56',
        ),
        'compliance_structure-compliance_structure_footerhq' => array(
          'module' => 'compliance_structure',
          'delta' => 'compliance_structure_footerhq',
          'region' => 'footer_first',
          'weight' => '-55',
        ),
        'block-4' => array(
          'module' => 'block',
          'delta' => '4',
          'region' => 'footer_first',
          'weight' => '-54',
        ),
        'compliance_resource-compliance_connection_modal' => array(
          'module' => 'compliance_resource',
          'delta' => 'compliance_connection_modal',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('FTLF');
  t('Global context for FTLF');
  $export['sitewide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'subscriber';
  $context->description = 'Context only active for subscribers';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~admin/store/orders/*/invoice' => '~admin/store/orders/*/invoice',
        '~primary' => '~primary',
        'subscriber' => 'subscriber',
        'subscriber/*' => 'subscriber/*',
        'orders' => 'orders',
        'ordered-subscriptions' => 'ordered-subscriptions',
        'user' => 'user',
        'user/*' => 'user/*',
      ),
    ),
    'user' => array(
      'values' => array(
        'authenticated user' => 'authenticated user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-subscriber-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-subscriber-menu',
          'region' => 'menu',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context only active for subscribers');
  t('FTLF');
  $export['subscriber'] = $context;

  return $export;
}
