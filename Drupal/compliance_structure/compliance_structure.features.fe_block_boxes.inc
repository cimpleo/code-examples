<?php
/**
 * @file
 * compliance_structure.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function compliance_structure_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Copyright Information';
  $fe_block_boxes->format = 'ds_code';
  $fe_block_boxes->machine_name = 'footer_copyright_info';
  $fe_block_boxes->body = '<a href="http://www.feldesmantucker.com/" target="_blank"><img src="/sites/default/images/ftlf-320.png" alt="Feldesman Tucker Leifer Fidell LLP"></a><div class="footerlinksone"><a href="/sitemap">Sitemap</a><a href="/terms-use">Terms of Use</a><a href="/privacy-policy">Privacy Policy</a><a href="/contact">Contact Us</a><a href="/user/login">Subscriber Login</a></div><div class="footerlinkstwo"><p><strong>© 2014 Feldesman Tucker Leifer Fidell LLP&nbsp;</strong></p></div><div class="clearfloat"></div>';

  $export['footer_copyright_info'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Phone number';
  $fe_block_boxes->format = 'ds_code';
  $fe_block_boxes->machine_name = 'header_anonymous';
  $fe_block_boxes->body = '<div class="buttonblock"><div class="orangebutton"><a href="/search/node">Search</a></div></div>
<div class="buttonblock cart"><div class="cart"><a href="/cart"></a></div></div>
<div class="buttonblock"><div class="bluebutton"><a href="/user/login">Sign-In</a></div></div><div class="clearfloat"></div><div class="callus"><p><b>Questions?</b> Contact Us Today!<br><span class="bigblue">1-800-266-1938</span><span class="bigline">&nbsp;|&nbsp;</span><a href="/contact">Email</a></p></div>';

  $export['header_anonymous'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Training Opportunities - Home Page';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'training_opportunities_home';
  $fe_block_boxes->body = '<div class="hometitletwo">Training Opportunities &amp; Resources</div><div class="homefour"><img src="/sites/default/images/training.jpg" alt="Training Opportunities and Resources"><p>Help your health center understand its compliance responsibilities and implement a compliance program to manage compliance risks. Our in-person and web-based training sessions include top compliance risk areas such as grants management, fraud and abuse, the FTCA, HIPAA, and developing an effective compliance program. Our trainings are developed and presented by attorneys from Feldesman Tucker Leifer Fidell LLP.</p></div>';

  $export['training_opportunities_home'] = $fe_block_boxes;

  return $export;
}
