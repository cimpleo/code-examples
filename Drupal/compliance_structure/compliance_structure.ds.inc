<?php
/**
 * @file
 * compliance_structure.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function compliance_structure_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|about_us|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'about_us';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'about_us_breadcrumb' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
    'service_links_displays_group' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'sld_group_image',
    ),
  );
  $export['node|about_us|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|compliance_connection|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'compliance_connection';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'compliance_connection_breadcrumb' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
    'post_date' => array(
      'weight' => '4',
      'label' => 'inline',
      'format' => 'ds_post_date_short',
    ),
  );
  $export['node|compliance_connection|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|compliance_news|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'compliance_news';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'compliance_news_breadcrumb' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'inline',
      'format' => 'ds_post_date_short',
    ),
  );
  $export['node|compliance_news|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|ftca_e_briefing|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'ftca_e_briefing';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'ftca_e_briefing_breadcrumb' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
    'post_date' => array(
      'weight' => '2',
      'label' => 'inline',
      'format' => 'ds_post_date_short',
    ),
  );
  $export['node|ftca_e_briefing|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|page|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'user_products' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
  );
  $export['node|page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|webform|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'webform';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'compliance_phone_breadcrumb' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
  );
  $export['node|webform|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|webform|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'webform';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'compliance_phone_breadcrumb' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
      ),
    ),
  );
  $export['node|webform|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function compliance_structure_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'about_us_breadcrumb';
  $ds_field->label = 'About Us breadcrumb';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a href="/about-us" alt="About Us">About Us</a> &rsaquo; [node:title]</p>',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['about_us_breadcrumb'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'compliance_breadcrumb';
  $ds_field->label = 'Compliance breadcrumb';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a href="/compliance-resources" alt="Compliance Resources">Compliance Resources</a> &rsaquo; [node:title]</p>',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['compliance_breadcrumb'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'compliance_connection_breadcrumb';
  $ds_field->label = 'Compliance Connection breadcrumb';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a href="/subscriber/compliance-connection">Compliance Connection</a> › [node:title]</p>',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['compliance_connection_breadcrumb'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'compliance_news_breadcrumb';
  $ds_field->label = 'Compliance News breadcrumb';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a href="/compliance-resources/news" alt="Compliance News">Compliance News</a> &rsaquo; [node:title]</p>',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['compliance_news_breadcrumb'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'compliance_phone_breadcrumb';
  $ds_field->label = 'Compliance Phone breadcrumb';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a href="/subscriber">Subscriber Resources</a> › Compliance Phone Consultation</p>',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['compliance_phone_breadcrumb'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'ftca_e_briefing_breadcrumb';
  $ds_field->label = 'FTCA E-Briefing breadcrumb';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<p><a href="/subscriber/ftca/e-briefing" alt="FTCA E-Briefing">FTCA E-Briefing</a> &rsaquo; [node:title]</p>',
      'format' => 'full_html',
    ),
    'use_token' => 1,
  );
  $export['ftca_e_briefing_breadcrumb'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function compliance_structure_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|about_us|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'about_us';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'about_us_breadcrumb',
        1 => 'title',
        2 => 'body',
        3 => 'field_available_downloads',
        4 => 'service_links_displays_group',
      ),
    ),
    'fields' => array(
      'about_us_breadcrumb' => 'ds_content',
      'title' => 'ds_content',
      'body' => 'ds_content',
      'field_available_downloads' => 'ds_content',
      'service_links_displays_group' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|about_us|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|compliance_connection|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'compliance_connection';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'compliance_connection_breadcrumb',
        1 => 'title',
      ),
      'left' => array(
        2 => 'field_written_by',
        3 => 'field_topics',
      ),
      'right' => array(
        4 => 'post_date',
      ),
      'footer' => array(
        5 => 'body',
        6 => 'field_available_downloads',
      ),
    ),
    'fields' => array(
      'compliance_connection_breadcrumb' => 'header',
      'title' => 'header',
      'field_written_by' => 'left',
      'field_topics' => 'left',
      'post_date' => 'right',
      'body' => 'footer',
      'field_available_downloads' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|compliance_connection|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|compliance_news|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'compliance_news';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'compliance_news_breadcrumb',
        1 => 'title',
      ),
      'left' => array(
        2 => 'post_date',
      ),
      'right' => array(
        3 => 'field_topics',
      ),
      'footer' => array(
        4 => 'body',
        5 => 'field_available_downloads',
      ),
    ),
    'fields' => array(
      'compliance_news_breadcrumb' => 'header',
      'title' => 'header',
      'post_date' => 'left',
      'field_topics' => 'right',
      'body' => 'footer',
      'field_available_downloads' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|compliance_news|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|ftca_e_briefing|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'ftca_e_briefing';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'ftca_e_briefing_breadcrumb',
        1 => 'title',
      ),
      'left' => array(
        2 => 'post_date',
      ),
      'right' => array(
        3 => 'field_topics',
      ),
      'footer' => array(
        4 => 'body',
        5 => 'field_available_downloads',
      ),
    ),
    'fields' => array(
      'ftca_e_briefing_breadcrumb' => 'header',
      'title' => 'header',
      'post_date' => 'left',
      'field_topics' => 'right',
      'body' => 'footer',
      'field_available_downloads' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|ftca_e_briefing|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'body',
        2 => 'field_available_downloads',
        3 => 'user_products',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'body' => 'ds_content',
      'field_available_downloads' => 'ds_content',
      'user_products' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|webform|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'webform';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'compliance_phone_breadcrumb',
        1 => 'title',
        2 => 'body',
        3 => 'webform',
      ),
    ),
    'fields' => array(
      'compliance_phone_breadcrumb' => 'ds_content',
      'title' => 'ds_content',
      'body' => 'ds_content',
      'webform' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|webform|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|webform|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'webform';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'compliance_phone_breadcrumb',
        1 => 'title',
        2 => 'body',
        3 => 'webform',
      ),
    ),
    'fields' => array(
      'compliance_phone_breadcrumb' => 'ds_content',
      'title' => 'ds_content',
      'body' => 'ds_content',
      'webform' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|webform|full'] = $ds_layout;

  return $export;
}
