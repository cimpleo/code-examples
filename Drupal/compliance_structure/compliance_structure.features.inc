<?php
/**
 * @file
 * compliance_structure.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function compliance_structure_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function compliance_structure_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function compliance_structure_node_info() {
  $items = array(
    'about_us' => array(
      'name' => t('About Us'),
      'base' => 'node_content',
      'description' => t('Add another About Us page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'compliance_connection' => array(
      'name' => t('Compliance Connection'),
      'base' => 'node_content',
      'description' => t('Add a new article to the Compliance Connection.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'compliance_news' => array(
      'name' => t('Compliance News'),
      'base' => 'node_content',
      'description' => t('Add a new Compliance News article.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'ftca_e_briefing' => array(
      'name' => t('FTCA E-Briefing'),
      'base' => 'node_content',
      'description' => t('Add a new FTCA E-Briefing article.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
