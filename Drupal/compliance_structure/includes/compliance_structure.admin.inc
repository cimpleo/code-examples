<?php
/**
 * @file
 * compliance_structure.admin.inc
 */

/**
 * Primary configuration area for any site wide settings.
 *
 * @param array $form.
 *   The form fields the displayed.
 * @param array $form_state.
 *   The save form_state passed to the form.
 *
 * @return form.
 */
function compliance_structure_config($form, &$form_state) {
  $form['global'] = array(
    '#type' => 'vertical_tabs',
  );
  
  // Pre Header fieldset.
  $form['pre_header'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pre Header'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'global',
  );
  $form['pre_header']['compliance_structure_pre_header_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Text in Pre Header'),
    '#description' => t('This text appears in the line above the header.'),
    '#default_value' => variable_get('compliance_structure_pre_header_text',''),
  );
  
  // Homepage fieldset.
  $form['homepage'] = array(
    '#type' => 'fieldset',
    '#title' => t('Homepage'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'global',
  );
  $form['homepage']['main_content_area'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main Content Area'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['homepage']['main_content_area']['compliance_structure_homepage_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Homepage Title'),
    '#description' => t('This text is the big title in the center homepage area.'),
    '#default_value' => variable_get('compliance_structure_homepage_title',''),
  );
  $homepage_text = variable_get('compliance_structure_homepage_text', '');
  $form['homepage']['main_content_area']['compliance_structure_homepage_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Homepage Text'),
    '#description' => t('This text is the main text in the center homepage area.'),
    '#default_value' => empty($homepage_text) ? '': $homepage_text['value'],
  );
  $form['homepage']['main_content_area']['compliance_structure_homepage_bg'] = array(
    '#title' => t('Background Image'),
    '#type' => 'managed_file',
    '#description' => t('This image is the image used for the background in the main homepage content.'),
    '#default_value' => variable_get('compliance_structure_homepage_bg', ''),
    '#upload_location' => 'public://default_images/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg svg'),
    ),
  );
  $learnmore =  variable_get('compliance_structure_hp_lm','');
  $form['homepage']['main_content_area']['learnmore'] = array(
    '#type' => 'fieldset',
    '#title' => t('Learn More Link'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['homepage']['main_content_area']['learnmore']['customcclink_compliance_structure_hp_lm_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($learnmore) ? '': $learnmore['title'],
  ); 
  $form['homepage']['main_content_area']['learnmore']['customcclink_compliance_structure_hp_lm_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($learnmore) ? '': $learnmore['link'],
  );  
  $form['homepage']['get_advice_area'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get Advice Area'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $getadvice_text = variable_get('compliance_structure_getadvice_text', '');
  $form['homepage']['get_advice_area']['compliance_structure_getadvice_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Get Advice Text'),
    '#description' => t('This text is the Get Advice area on the homepage.'),
    '#default_value' => empty($getadvice_text) ? '': $getadvice_text['value'],
  );
  $form['homepage']['get_advice_area']['ga_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Featured Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $ga_1 =  variable_get('compliance_structure_ga_1','');
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_1_title'] = array(
    '#type' => 'textfield',
    '#title' => t('(1) Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($ga_1) ? '': $ga_1['title'],
  ); 
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_1_link'] = array(
    '#type' => 'textfield',
    '#title' => t('(1) Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($ga_1) ? '': $ga_1['link'],
  );  
  $ga_2 =  variable_get('compliance_structure_ga_2','');
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_2_title'] = array(
    '#type' => 'textfield',
    '#title' => t('(2) Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($ga_2) ? '': $ga_2['title'],
  ); 
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_2_link'] = array(
    '#type' => 'textfield',
    '#title' => t('(2) Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($ga_2) ? '': $ga_2['link'],
  );   
  $ga_3 =  variable_get('compliance_structure_ga_3','');
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_3_title'] = array(
    '#type' => 'textfield',
    '#title' => t('(3) Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($ga_3) ? '': $ga_3['title'],
  ); 
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_3_link'] = array(
    '#type' => 'textfield',
    '#title' => t('(3) Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($ga_3) ? '': $ga_3['link'],
  );    
  $ga_4 =  variable_get('compliance_structure_ga_4','');
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_4_title'] = array(
    '#type' => 'textfield',
    '#title' => t('(4) Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($ga_4) ? '': $ga_4['title'],
  ); 
  $form['homepage']['get_advice_area']['ga_links']['customcclink_compliance_structure_ga_4_link'] = array(
    '#type' => 'textfield',
    '#title' => t('(4) Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($ga_4) ? '': $ga_4['link'],
  ); 
  $form['homepage']['stay_compliant_area'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stay Compliant Area'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $staycompliant_text = variable_get('compliance_structure_staycompliant_text', '');
  $form['homepage']['stay_compliant_area']['compliance_structure_staycompliant_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Stay Compliant Text'),
    '#description' => t('This text is the Stay Compliant area on the homepage.'),
    '#default_value' => empty($staycompliant_text) ? '': $staycompliant_text['value'],
  );
  $form['homepage']['stay_compliant_area']['sc_links'] = array(
    '#type' => 'fieldset',
    '#title' => t('Featured Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $sc_1 =  variable_get('compliance_structure_sc_1','');
  $form['homepage']['stay_compliant_area']['sc_links']['customcclink_compliance_structure_sc_1_title'] = array(
    '#type' => 'textfield',
    '#title' => t('(1) Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($sc_1) ? '': $sc_1['title'],
  ); 
  $form['homepage']['stay_compliant_area']['sc_links']['customcclink_compliance_structure_sc_1_link'] = array(
    '#type' => 'textfield',
    '#title' => t('(1) Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($sc_1) ? '': $sc_1['link'],
  );  
  $sc_2 =  variable_get('compliance_structure_sc_2','');
  $form['homepage']['stay_compliant_area']['sc_links']['customcclink_compliance_structure_sc_2_title'] = array(
    '#type' => 'textfield',
    '#title' => t('(2) Title'),
    '#description' => t('This text is what will appear on the button.'),
    '#default_value' => empty($sc_2) ? '': $sc_2['title'],
  ); 
  $form['homepage']['stay_compliant_area']['sc_links']['customcclink_compliance_structure_sc_2_link'] = array(
    '#type' => 'textfield',
    '#title' => t('(2) Link'),
    '#description' => t('This link is the page that the button links to. Internal path only, do not include the first forward slash.'),
    '#default_value' => empty($sc_2) ? '': $sc_2['link'],
  );  
  
  // Global field set.
  $form['globalconfig'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'global',
  );
  $form['globalconfig']['compliance_structure_email_signup_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Newsletter Signup Title'),
    '#description' => t('This text is the title used for the Newsletter signup block.'),
    '#default_value' => variable_get('compliance_structure_email_signup_title',''),
  );
  $emailsignup_text = variable_get('compliance_structure_email_signup_text', '');
  $form['globalconfig']['compliance_structure_email_signup_text'] = array(
    '#type' => 'text_format',
    '#title' => t('Newsletter Signup Text'),
    '#description' => t('This text is the text that appears in the Newsletter sign up block above the footer.'),
    '#default_value' => empty($emailsignup_text) ? '': $emailsignup_text['value'],
  );
  $form['globalconfig']['compliance_structure_hq_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Have Questions Phone Number'),
    '#description' => t('This is the phone number in the Have Questions? block in the footer.'),
    '#default_value' => variable_get('compliance_structure_hq_number',''),
  );
  
  $form['#submit'][] = 'compliance_structure_managed_file_saver';
  $form['#submit'][] = 'compliance_structure_custom_link_admin';
  
  return system_settings_form($form);
}

/**
 * We need a custom submit handler that will save all of our managed_files.
 */
function compliance_structure_managed_file_saver(&$form,&$form_state) {
  global $user;
  // We're going to go through the input's and see if there are files.
  foreach ($form_state['input'] as $key => $input) {
    if(is_array($input) && key_exists('fid',$input)) {
      $fid = $form_state['input'][$key]['fid'];
      if ($fid != 0) {
        $file = file_load($fid);
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        file_usage_add($file, 'user', 'user', $user->uid);
      }
    }
  }
}

/**
 * We need a custom submit handler for the link field option.
 */
function compliance_structure_custom_link_admin(&$form,&$form_state) {
  $links = array();
  // Loop through the form values and find the custom link's.
  foreach ($form_state['values'] as $input => $value) {
    if (strpos($input,'customcclink') !== false) {
       $key = str_replace('customcclink_','',$input);
       // Check for title or link.
       if (strpos($key,'_link') !== false) {
         $key = str_replace('_link','',$key);
         if (drupal_lookup_path('source', $value) || drupal_lookup_path('alias', $value) || !empty(menu_get_item($value))) {
           $links[$key]['link'] = $value;
         } else {
           if (!empty($value)) {
            drupal_set_message('The path: "'.$value. '" is not valid.','warning');
           }
         }
       } else {
         $key = str_replace('_title','',$key);
         $links[$key]['title'] = $value; 
       }
    }
  }
  
  // Loop through links and create variables.
  foreach ($links as $variable => $arr) {
    variable_set($variable,$arr);
  }
}

