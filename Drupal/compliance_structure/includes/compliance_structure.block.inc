<?php
/**
 * @file
 * compliance_structure.block.inc
 */

/**
 * Implements hook_block_info
 */
function compliance_structure_block_info() {
  $blocks = array();

  $blocks['compliance_structure_ph_text'] = array(
    'info' => t('Pre Header Text'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  $blocks['compliance_structure_home_main'] = array(
    'info' => t('Homepage: Main Content'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  $blocks['compliance_structure_home_advice'] = array(
    'info' => t('Homepage: Get Advice'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  $blocks['compliance_structure_home_stay'] = array(
    'info' => t('Homepage: Stay Compliant'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  $blocks['compliance_structure_title'] = array(
    'info' => t('Structure Title'),
    'cache' => DRUPAL_CACHE_PER_PAGE
  );
  $blocks['compliance_structure_breadcrumb'] = array(
    'info' => t('Structure Breadcrumb'),
    'cache' => DRUPAL_CACHE_PER_PAGE
  );
  $blocks['compliance_structure_emailsignup'] = array(
    'info' => t('Newsletter Sign Up'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );
  $blocks['compliance_structure_footerhq'] = array(
    'info' => t('Footer: Have Questions'),
    'cache' => DRUPAL_CACHE_GLOBAL
  );

  return $blocks;
}

/**
 * Implements hook_block_info_alter
 */
function compliance_structure_block_info_alter(&$blocks, $theme, $code_blocks) {
  // We need to alter static blocks. So let's do it here.
  if ($theme != 'hcc') {
    return FALSE;
  }
  // "Phone Number" Block
  if (array_key_exists(1,$blocks['block'])) {
    $blocks['block'][1]['region'] = -1;
  }
  // "FTLF logo" Block
  if (array_key_exists(3,$blocks['block'])) {
    $blocks['block'][3]['region'] = -1;
  }
  // "Copyright Information" Block
  if (array_key_exists(4,$blocks['block'])) {
    $blocks['block'][4]['region'] = -1;
  }
  // "Sales Message" Block
  if (array_key_exists(43,$blocks['block'])) {
    $blocks['block'][43]['region'] = -1;
  }
  // "Research, Learn, Ask" Block
  if (array_key_exists(44,$blocks['block'])) {
    $blocks['block'][44]['region'] = -1;
  }
  // "Footer Links" Block
  if (array_key_exists(46,$blocks['block'])) {
    $blocks['block'][46]['region'] = -1;
  }
}

/**
 * Implements hook_block_view().
 */
function compliance_structure_block_view($delta = '', $context = '') {
  $block = NULL;

  // View blocks.
  $callback = "_compliance_structure_block_view_{$delta}";
  if (function_exists($callback)) {
    $block = $callback($delta, $context);
  }

  return $block;
}

/**
 * Block view function for Pre Header text block.
 */
function _compliance_structure_block_view_compliance_structure_ph_text(&$delta,&$context) {
  $block = array();

  $markup = variable_get('compliance_structure_pre_header_text','');

  $block['content'] = array(
    '#markup' => $markup,
  );

  return $block;
}

/**
 * Block view function for main homepage content block.
 */
function _compliance_structure_block_view_compliance_structure_home_main(&$delta,&$context) {
  $block = array();

  $markup = '<h1>'.variable_get('compliance_structure_homepage_title','').'</h1>';
  $homepage_text = variable_get('compliance_structure_homepage_text', '');
  $markup .= empty($homepage_text) ? '': $homepage_text['value'];
  $lm =  variable_get('compliance_structure_hp_lm','');
  if(!empty($lm['link'])) {
    $lm_text = empty($lm['title']) ? 'Learn More': $lm['title'];
    $markup .= '<a href="'.$lm['link'].'" class="hp-main">'.$lm_text.'</a>';
  }

  $block['content'] = array(
    '#markup' => $markup,
  );

  return $block;
}

/**
 * Block view function for Get Advice homepage block.
 */
function _compliance_structure_block_view_compliance_structure_home_advice(&$delta,&$context) {
  $block = array();

  $getadvice_text = variable_get('compliance_structure_getadvice_text', '');
  $markup = empty($getadvice_text) ? '': $getadvice_text['value'];

  $links = array();
  $items = array();
  $links[] = variable_get('compliance_structure_ga_1','');
  $links[] = variable_get('compliance_structure_ga_2','');
  $links[] = variable_get('compliance_structure_ga_3','');
  $links[] = variable_get('compliance_structure_ga_4','');

  foreach ($links as $link) {
    if (!empty($link['link'])) {
      $items[] = l($link['title'],$link['link']);
    }
  }

  if (!empty($items)) {
    $markup .= theme('item_list',array('items' => $items));
  }

  $block['subject'] = 'Get Advice';
  $block['content'] = array(
    '#markup' => $markup,
  );

  return $block;
}

/**
 * Block view function for Stay Compliant homepage block.
 */
function _compliance_structure_block_view_compliance_structure_home_stay(&$delta,&$context) {
  $block = array();

  $staycompliant_text = variable_get('compliance_structure_staycompliant_text', '');
  $markup = empty($staycompliant_text) ? '': $staycompliant_text['value'];

  $links = array();
  $items = array();
  $links[] = variable_get('compliance_structure_sc_1','');
  $links[] = variable_get('compliance_structure_sc_2','');

  foreach ($links as $link) {
    if (!empty($link['link'])) {
      $items[] = l($link['title'],$link['link']);
    }
  }

  if (!empty($items)) {
    $markup .= theme('item_list',array('items' => $items));
  }

  $block['subject'] = 'Stay Compliant';
  $block['content'] = array(
    '#markup' => $markup,
  );

  return $block;
}

/**
 * Implements template_preprocess_block.
 */
function compliance_structure_preprocess_block(&$vars) {
  switch($vars['block']->delta) {
    case 'compliance_structure_home_main':
      $vars['classes_array'][] = 'grid-12';
      // We need to set the background image to be the uploaded file.
      $prev_style = '';
      if(!empty($vars['attributes_array']['style'])) {
        $prev_style = $vars['attributes_array']['style'];
      }
      $hp_bg = variable_get('compliance_structure_homepage_bg','');
      $file = file_load($hp_bg);
      if (empty($hp_bg) || empty($file)) {
        break;
      }
      $image_url = file_create_url($file->uri);
      $vars['attributes_array']['style'] = $prev_style.'background-image:url("'.$image_url.'");';
      break;
    case 'compliance_structure_home_advice':
      $vars['classes_array'][] = 'grid-8';
      break;
    case 'compliance_structure_home_stay':
      $vars['classes_array'][] = 'grid-4';
      break;
    default:
      break;
  }
}

/**
 * Allow for breadcrumbs to be added to pages of the site.
 *
 * @param string $delta.
 *  THe machine_name of the block.
 * @param array $object.
 *  The array of context values.
 *
 * @return array.
 */
function _compliance_structure_block_view_compliance_structure_title(&$delta, &$context) {
  $block['content'] = array(
    '#type' => 'markup',
    '#markup' => '<h1>' . drupal_get_title() . '</h1>',
  );

  drupal_alter('compliance_structure_title', $block, $delta, $context);

  return $block;
}

/**
 * Allow for breadcrumbs to be added to pages of the site.
 *
 * @param string $delta.
 *  THe machine_name of the block.
 * @param array $object.
 *  The array of context values.
 *
 * @return array.
 */
function _compliance_structure_block_view_compliance_structure_breadcrumb(&$delta, &$context) {
  $breadcrumb = array(
    'home' => l('Home', '<front>'),
  );

  // Allow for other modules to alter the breadrumb.
  drupal_alter('compliance_structure_breadcrumb', $breadcrumb, $delta, $context);

  $block['content'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . implode(' › ', $breadcrumb) . '</p>',
  );

  return $block;
}

/**
 * Block of text for the email signup above the footer..
 */
function _compliance_structure_block_view_compliance_structure_emailsignup(&$delta, &$context) {
  $subject = variable_get('compliance_structure_email_signup_title','Sign up for our free e-Newsletter!');
  $emailsignup_text = variable_get('compliance_structure_email_signup_text', '');
  $markup = empty($emailsignup_text) ? '': $emailsignup_text['value'];

  $block['subject'] = $subject;
  $block['content'] = array(
    '#type' => 'markup',
    '#markup' => $markup,
  );

  return $block;
}

/**
 * Block in the footer titled "Have Questions?"
 */
function _compliance_structure_block_view_compliance_structure_footerhq(&$delta, &$context) {
  $markup = '<p>'.variable_get('compliance_structure_hq_number', '').'</p>';

  $block['subject'] = 'Have Questions?';
  $block['content'] = array(
    '#type' => 'markup',
    '#markup' => $markup,
  );

  return $block;
}
