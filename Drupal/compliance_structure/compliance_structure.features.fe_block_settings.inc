<?php
/**
 * @file
 * compliance_structure.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function compliance_structure_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-footer_copyright_info'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'footer_copyright_info',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => -42,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-header_anonymous'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'header_anonymous',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['block-training_opportunities_home'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'training_opportunities_home',
    'module' => 'block',
    'node_types' => array(),
    'pages' => 'primary',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => -3,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['menu-menu-admin-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-admin-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => 'admin/store/orders/*/invoice
primary',
    'roles' => array(
      'administrator' => 3,
      'editor' => 4,
    ),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => -35,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Admin menu',
    'visibility' => 0,
  );

  $export['menu-menu-subscriber-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-subscriber-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => 'admin/store/orders/*/invoice
primary',
    'roles' => array(
      'administrator' => 3,
      'assessment fgm sub' => 17,
      'audits subscriber' => 13,
      'authenticated user' => 2,
      'bronze subscriber' => 15,
      'compliance phone consult' => 12,
      'compliance subscriber' => 7,
      'editor' => 4,
      'ftca audits sub' => 16,
      'ftca phone consult' => 11,
      'ftca subscriber' => 8,
      'nachc toolkit subscriber' => 9,
      'pca toolkit subscriber' => 10,
      'premium subscriber' => 6,
      'silver subscriber' => 14,
    ),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => -46,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => 'Subscriber Menu',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '<front>
admin/store/orders
admin/store/orders/*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -1,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => -40,
      ),
      'seven' => array(
        'region' => 'dashboard_inactive',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -10,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'hcc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'hcc',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
