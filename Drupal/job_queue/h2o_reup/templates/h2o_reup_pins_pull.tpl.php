<?php
  $row = array();
  foreach ($form['pin_list']['#value'] as $pin) {
    $row[] = array(
      $pin[0],
      $pin[1],
      l('Delete', 'admin/settings/tms/h2o_pins_pull/delete/' . $pin[0], array('query' => drupal_get_destination()))
  );
  }
  $row[] = array(
    drupal_render($form['new_pin']),
    drupal_render($form['new_plan']),
    ''
  );

$output = theme('table', array(
  'header' => array('Pin Number', 'Plan Name', ''),
  'rows' => $row,
));

$output .= drupal_render_children($form);

echo $output;