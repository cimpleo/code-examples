<?php

class job_queue_ui_handler_field_data extends views_handler_field {
  private $_dump_path = array();

  public function render($values) {
    $value = unserialize($this->get_value($values));
    $code = $this->_dump_var($value);
    $return = '<pre>' . $code . '</pre>';
    return $return;
  }


  private function _dump_var(&$var, $level = 0, $name = NULL, $prefix = '[', $suffix = '] => ', $format_name = TRUE) {
    $return = $this->_format_indent($level);
    $path_name = '';
    if (!is_null($name)) {
      $return .= $prefix . ($format_name ? $this->_format_var($name) : $name) . $suffix;
    }
    if (is_array($var)) {
      if (array_key_exists('@@RECURSION@@', $var)) {
        $name = $this->_dump_path[$var['@@RECURSION@@']]['name'];
        if (is_null($name)) {
          $name = '*ROOT*';
        }
        return $return . $name . ' Array *RECURSION*';
      }
      $var['@@RECURSION@@'] = $level;
      $path_name = $name;
    }
    elseif (is_object($var)) {
      for ($i = 0; $i < $level; $i++) {
        if ($this->_dump_path[$i]['var'] === $var) {
          return $return . $this->_dump_path[$i]['name'] . ' Object *RECURSION*';
        }
      }
      $path_name = get_class($var);
    }
    $this->_dump_path[$level] = array(
      'name' => $path_name,
      'var' => &$var,
    );
    $return .= $this->_format_var($var, $level);
    if (is_array($var)) {
      unset($var['@@RECURSION@@']);
    }
    return $return;
  }

  private function _format_var(&$var, $level = 0) {
    if (is_string($var)) {
      return $this->_format_string($var);
    }
    elseif (is_null($var)) {
      return $this->_format_null($var);
    }
    elseif (is_numeric($var)) {
      return $this->_format_numeric($var);
    }
    elseif (is_bool($var)) {
      return $this->_format_bool($var);
    }
    elseif (is_array($var)) {
      return $this->_format_array($var, $level);
    }
    elseif (is_object($var)) {
      return $this->_format_object($var, $level);
    }
    else {
      return (string) $var;
    }
  }

  private function _format_indent($level) {
    return str_repeat(' ', $level * 2);
  }

  private function _format_array(&$var, $level = 0) {
    $return = "array(\n";
    foreach ($var as $name => $val) {
      if ($name !== '@@RECURSION@@') {
        $return .= $this->_dump_var($var[$name], $level + 1, $name) . ",\n";
      }
    }
    $return .= $this->_format_indent($level) . ')';

    return $return;
  }

  private function _format_object(&$var, $level = 0) {
    $return = 'class ' . get_class($var) . " {\n";
    foreach ($var as $name => $val) {
      $return .= $this->_dump_var($var->$name, $level + 1, $name, 'public $', ' = ', FALSE) . ";\n";
    }
    $return .= $this->_format_indent($level) . '}';

    return $return;
  }

  private function _format_bool($var) {
    return $var ? 'TRUE' : 'FALSE';
  }

  private function _format_NULL() {
    return 'NULL';
  }

  private function _format_string($var) {
    if (strlen($var) > 1024) {
      $id = hash('sha1', uniqid(getmypid() . mt_rand(1, 999999999), TRUE));
      return
        '<a href="#" onclick="$(\'#binvar_' . $id . '\').toggle();return false;">BINARY (' . strlen($var) . ')</a>' .
        '<div id="binvar_' . $id . '" style="display: none;overflow-x:auto">' . htmlspecialchars($var) . '</div>';
    }
    return '"' . htmlspecialchars($var) . '"';
  }

  private function _format_numeric($var) {
    return $var;
  }
}
