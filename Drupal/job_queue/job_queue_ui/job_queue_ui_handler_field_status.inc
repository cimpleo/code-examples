<?php

class job_queue_ui_handler_field_status extends views_handler_field_numeric {
  public function render($values) {
    $value = $this->get_value($values);

    $attempts = property_exists($values, 'job_queue_attempts') ? $values->job_queue_attempts : 0;
    $str = array(
      JOB_QUEUE_STATUS_ACTIVE => t('Active (%attempts attempts)', array('%attempts' => $attempts)),
      JOB_QUEUE_STATUS_SUCCESS => t('Success (%attempts attempts)', array('%attempts' => $attempts)),
      JOB_QUEUE_STATUS_FAIL => t('Fail (%attempts attempts)', array('%attempts' => $attempts)),
      JOB_QUEUE_STATUS_FATAL => t('Fatal (%attempts attempts)', array('%attempts' => $attempts)),
      JOB_QUEUE_STATUS_DISABLED => t('Disabled'),
    );
    if ($value == 1 && $attempts == 0) {
      $value = t('Active');
    }
    else {
      $value = array_key_exists(intval($value), $str) ? $str[intval($value)] : $value;
    }
    return filter_xss($this->options['prefix']) . $value . filter_xss($this->options['suffix']);
  }
}