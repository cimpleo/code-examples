<?php
function job_queue_mail_settings($form, $form_state) {
  $config = variable_get('job_queue_mail_jobs', array(
    'use_smtp' => 1,
    'host' => (string) ini_get('SMTP'),
    'port' => (int) ini_get('smtp_port'),
    'username' => '',
    'password' => '',
    'security' => 'none',
    'base' => array(
      'email_from' => variable_get('site_mail', ini_get('sendmail_from')),
      'name_from' => variable_get('site_name', ''),
      'debug_email' => ''
    ),

  ));

  $form['job_queue_mail_jobs'] = array(
    '#tree' => TRUE,
  );

  $form['job_queue_mail_jobs']['use_smtp'] = array(
    '#title' => 'use smtp',
    '#type' => 'checkbox',
    '#default_value' => $config['use_smtp']
  );

  $form['job_queue_mail_jobs']['host'] = array(
    '#title' => 'host',
    '#type' => 'textfield',
    '#default_value' => $config['host']
  );

  $form['job_queue_mail_jobs']['port'] = array(
    '#title' => 'port',
    '#type' => 'textfield',
    '#default_value' => $config['port']
  );

  $form['job_queue_mail_jobs']['username'] = array(
    '#title' => 'username',
    '#type' => 'textfield',
    '#default_value' => $config['username']
  );

  $form['job_queue_mail_jobs']['password'] = array(
    '#title' => 'password',
    '#type' => 'textfield',
    '#default_value' => $config['password']
  );

  $form['job_queue_mail_jobs']['security'] = array(
    '#type' => 'radios',
    '#title' => 'Security',
    '#options' => array('none' => 'None', 'ssl' => 'SSL', 'tls' => 'TLS'),
    '#default_value' => isset($config['security']) ? $config['security'] : 'none'
  );

  $form['job_queue_mail_jobs']['base'] = array(
    '#title' => 'Base settings',
    '#type' => 'fieldset',
    '#tree' => TRUE
  );

  $form['job_queue_mail_jobs']['base']['email_from'] = array(
    '#type' => 'textfield',
    '#title' => 'email from',
    '#default_value' => $config['base']['email_from']
  );

  $form['job_queue_mail_jobs']['base']['name_from'] = array(
    '#type' => 'textfield',
    '#title' => 'name from',
    '#default_value' => $config['base']['name_from']
  );

  $form['job_queue_mail_jobs']['base']['debug_email'] = array(
    '#type' => 'textfield',
    '#title' => 'debug email',
    '#default_value' => $config['base']['debug_email']
  );

  return system_settings_form($form);
}