<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_getdatasetid extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['getdatasetid'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['getdatasetid'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter a value.'),
      '#default_value' => $this->options['getdatasetid'] ? $this->options['getdatasetid'] : '[field_data_set]',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.

    $tokens = explode(';', $this->options['getdatasetid']);
    $vals = $this->get_render_tokens('');
    $term_name = $vals[$tokens[0]];
    $tid = joc_dashboards_getTermId($term_name, 'joc_charts_data_set');
    return $tid;
  }
}
