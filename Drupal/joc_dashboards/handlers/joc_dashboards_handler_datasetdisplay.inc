<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_datasetdisplay extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['datasetdisplay'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['datasetdisplay'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter tokens'),
      '#default_value' => $this->options['datasetdisplay'] ? $this->options['datasetdisplay'] : '!1;!2;!3;[getdatasetid];1;[php_3];[field_data_set]',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.
    global $base_url;

    $tokens = explode(';', $this->options['datasetdisplay']);
    $vals = $this->get_render_tokens('');

    $tLane = $vals[$tokens[0]];
    $dType = rawurlencode($vals[$tokens[1]]);
    $cType = rawurlencode($vals[$tokens[2]]);
    $dsId = $vals[$tokens[3]];
    $bid = $tokens[4];
    $domid = $vals[$tokens[5]];

    $text = $vals[$tokens[6]];
    $path = $base_url.'/joc-dashboard-charts-ajax/nojs/'.$tLane.'/'.$dType.'/'.$cType.'/'.$dsId.'/'.$bid.'/'.$domid;
    $attributes = array(
      'attributes' => array('class' => array('use-ajax')),
      'html' => TRUE,
    );

    return l($text, $path, $attributes);
  }
}
