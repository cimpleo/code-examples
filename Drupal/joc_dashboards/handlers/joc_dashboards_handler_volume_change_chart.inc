<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_volume_change_chart extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['volume_change_chart'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['volume_change_chart'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter two values.'),
      '#default_value' => $this->options['volume_change_chart'] ? $this->options['volume_change_chart'] : '!1;!2;!3;!4;[created];[field_volume]',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.

    $tokens = explode(';', $this->options['volume_change_chart']);
    $vals = $this->get_render_tokens('');
    $dashboardName = $vals[$tokens[1]];
    $typeName = $vals[$tokens[2]];
    $term_name = $vals[$tokens[3]];

    $tradeLaneId = $vals[$tokens[0]];
    $dashboardId = joc_dashboards_getTermId($dashboardName, 'joc_charts_dashboard');
    $typeId = joc_dashboards_getTermId($typeName, 'joc_charts_type');
    $tid = is_numeric($term_name) ? $term_name : joc_dashboards_getTermId($term_name, 'joc_charts_data_set');
    $this_date = $vals[$tokens[4]];
    $this_volume = $vals[$tokens[5]];

    $yearago = strtotime($this_date . ' -1 year');

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'dashboard_charts')
      ->propertyCondition('status', 1)
      ->propertyCondition('created', $yearago, '=')
      ->propertyOrderBy('created', 'ASC')
      ->fieldCondition('field_import_type ', 'value', array('volume'), 'IN')
      ->fieldCondition('field_trade_lane', 'tid', $tradeLaneId)
      ->fieldCondition('field_dashboard', 'tid', $dashboardId)
      ->fieldCondition('field_type', 'tid', $typeId)
      ->fieldCondition('field_data_set', 'tid', $tid);
    $result = $query->execute();

    if (isset($result['node'])) {
      $dashboard_charts_nids = array_keys($result['node']);
      $dashboard_charts_items = entity_load('node', $dashboard_charts_nids);

      foreach ($dashboard_charts_items as $nid) {
        $prior_volume = $nid->field_volume['und'][0]['value'];
      }

      $needles = array('%', ',', '$');
      $this_volume = str_replace($needles, '', $this_volume);
      $prior_volume = str_replace($needles, '', $prior_volume);

      return getPercentageRaw($this_volume, $prior_volume);
    } else {
      $needles = array('%', ',', '$');
      $this_volume = str_replace($needles, '', $this_volume);
      $prior_volume = 0;

      return 100;
    }

    return 0;
  }
}
