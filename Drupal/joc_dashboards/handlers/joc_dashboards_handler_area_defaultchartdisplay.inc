<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_area_defaultchartdisplay extends views_handler_area {
  function option_definition() {
    $options = parent::option_definition();
    $options['defaultchartdisplay'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['defaultchartdisplay'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter tokens'),
      '#default_value' => $this->options['defaultchartdisplay'] ? $this->options['defaultchartdisplay'] : '!1;%2;%3;[%php_2];1',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($empty = FALSE) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.

    # Trade lane (REGION column)
    $tradeLane = $this->view->result[0]->_field_data['nid']['entity']->field_trade_lane['und'][0]['tid'];

    # Dashboard type (CHART_TYPE column)
    $dashboardTypeId = $this->view->result[0]->_field_data['nid']['entity']->field_dashboard['und'][0]['tid'];
    $dType = taxonomy_term_load($dashboardTypeId);

    # Type (TYPE column)
    $cTypeId = $this->view->result[0]->_field_data['nid']['entity']->field_type['und'][0]['tid'];
    $cType = taxonomy_term_load($cTypeId);

    # Data set (DATA_SET column)
    $dataSet = $this->view->result[0]->field_data_field_data_set_field_data_set_tid;

    $block_id = $this->view->current_display;

    return views_embed_view('template_charts', $block_id, $tradeLane, $dType->name, $cType->name, $dataSet);
  }
}
