<?php

/**
 * A handler to display dates as just the time if today, otherwise as time and date.
 *
 * @ingroup views_field_handlers
 */
class joc_dashboards_handler_getpercentage extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();
    $options['getpercentage'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['getpercentage'] = array(
      '#type' => 'textfield',
      '#title' => t('Tokens'),
      '#description' => t('Enter three values, the last one must be the decimal count.'),
      '#default_value' => $this->options['getpercentage'] ? $this->options['getpercentage'] : '[currentweek];[mostrecentweek]',
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    // do nothing -- to override the parent query.
  }

  function render($data) {
    // If the devel module is enabled, you may view all of the
    // data provided by fields previously added in your view.

    $tokens = explode(';', $this->options['getpercentage']);
    $vals = $this->get_render_tokens('');
    $needles = array('%', ',', '$');

    if(!empty($vals)) {
      $val1 = str_replace($needles, '', trim(strip_tags($vals[$tokens[0]])));
      $val2 = str_replace($needles, '', trim(strip_tags($vals[$tokens[1]])));
      $decimals = $tokens[2];

      return $decimals ? getPercentage($val1, $val2, $decimals) : getPercentage($val1, $val2);
    }

    return 0;
  }
}
