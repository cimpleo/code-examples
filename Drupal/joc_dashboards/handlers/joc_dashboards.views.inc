<?php
/**
 * Implements hook_views_data().
 */
function joc_dashboards_views_data() {
  $data['joc_dashboards']['table']['group'] = t('JOC');
  $data['joc_dashboards']['table']['join'] = array(
    // Exist in all views.
    '#global' => array(),
  );

  $data['joc_dashboards']['getpercentage'] = array(
    'title' => t('GetPercentage'),
    'help' => t('Display percentage difference.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_getpercentage',
    ),
  );

  $data['joc_dashboards']['getdatasetid'] = array(
    'title' => t('Get Data_Set id'),
    'help' => t('Get data_set id.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_getdatasetid',
    ),
  );

  $data['joc_dashboards']['defaultchartdisplay'] = array(
    'title' => t('Default Chart display'),
    'help' => t('Displays the corresponding chart when a Dashboards page loads for the first time.'),
    'area' => array(
      'handler' => 'joc_dashboards_handler_area_defaultchartdisplay',
    ),
  );

  $data['joc_dashboards']['viewdomid'] = array(
    'title' => t('View dom id'),
    'help' => t('Return the view\'s dom id.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_viewdomid',
    ),
  );

  $data['joc_dashboards']['datasetdisplay'] = array(
    'title' => t('Data Set display'),
    'help' => t('Display the Data Set.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_datasetdisplay',
    ),
  );

  $data['joc_dashboards']['currentweek'] = array(
    'title' => t('Rates Current Week'),
    'help' => t('Display current value for weekly rates.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_currentweek',
    ),
  );

  $data['joc_dashboards']['mostrecentweek'] = array(
    'title' => t('Rates Most Recent Week'),
    'help' => t('Display most recent value for weekly rates.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_mostrecentweek',
    ),
  );

  $data['joc_dashboards']['yearagoratesw'] = array(
    'title' => t('Rates 1 Year Ago'),
    'help' => t('Display Current value for weekly rates.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_yearagoratesw',
    ),
  );

  $data['joc_dashboards']['indexcurrentmonth'] = array(
    'title' => t('Index Current Month'),
    'help' => t('Display current value for monthly indices.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_indexcurrentmonth',
    ),
  );

  $data['joc_dashboards']['indexmostrecentmonth'] = array(
    'title' => t('Index Most Recent Month'),
    'help' => t('Display most recent value for monthly indices.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_indexmostrecentmonth',
    ),
  );

  $data['joc_dashboards']['indexyearagomonth'] = array(
    'title' => t('Index 1 Year Ago Month'),
    'help' => t('Display 1 year ago value for monthly indices.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_indexyearagomonth',
    ),
  );

  $data['joc_dashboards']['t8indexcurrent'] = array(
    'title' => t('Index Current Week'),
    'help' => t('Display current value for weekly indices.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t8indexcurrent',
    ),
  );

  $data['joc_dashboards']['t8indexmostrecent'] = array(
    'title' => t('Index Most Recent Week'),
    'help' => t('Display most recent value for weekly indices.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t8indexmostrecent',
    ),
  );

  $data['joc_dashboards']['t8indexyearago'] = array(
    'title' => t('Index 1 Year Ago Week'),
    'help' => t('Display 1 year ago value for weekly indices.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t8indexyearago',
    ),
  );

  $data['joc_dashboards']['percentagecurrent'] = array(
    'title' => t('Percentage Current'),
    'help' => t('Display current value for percentages.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_percentagecurrent',
    ),
  );

  $data['joc_dashboards']['percentageyearago'] = array(
    'title' => t('Percentage 1 Year Ago'),
    'help' => t('Display 1 year ago value for percentages.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_percentageyearago',
    ),
  );

  $data['joc_dashboards']['percentageytd'] = array(
    'title' => t('Percentage Average YTD'),
    'help' => t('Display Average YTD value for percentages.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_percentageytd',
    ),
  );

  $data['joc_dashboards']['pricingcurrentmonth'] = array(
    'title' => t('Pricing Current Month'),
    'help' => t('Display current value for monthly pricing.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_pricingcurrentmonth',
    ),
  );

  $data['joc_dashboards']['pricingmostcurrent'] = array(
    'title' => t('Pricing Most Current'),
    'help' => t('Display most current value for monthly pricing.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_pricingmostcurrent',
    ),
  );

  $data['joc_dashboards']['pricingyearago'] = array(
    'title' => t('Pricing Year Ago'),
    'help' => t('Display 1 year ago value for monthly pricing.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_pricingyearago',
    ),
  );

  $data['joc_dashboards']['volumecurrent'] = array(
    'title' => t('Volume Current'),
    'help' => t('Display current value for monthly volumes'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_volumecurrent',
    ),
  );

  $data['joc_dashboards']['volumemostrecent'] = array(
    'title' => t('Volume Most Recent'),
    'help' => t('Display most recent value for monthly volumes'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_volumemostrecent',
    ),
  );

  $data['joc_dashboards']['volumeytd'] = array(
    'title' => t('Volume YTD'),
    'help' => t('Display year to date value for monthly volumes.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_volumeytd',
    ),
  );

  $data['joc_dashboards']['volumepriorytd'] = array(
    'title' => t('Volume vs. Prior YTD'),
    'help' => t('Display vs. Prior YTD value for monthly volumes.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_volumepriorytd',
    ),
  );

  $data['joc_dashboards']['volume_change_chart'] = array(
    'title' => t('Volume % Change vs. Prior YTD'),
    'help' => t('Display % Change for monthly volumes.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_volume_change_chart',
    ),
  );

  $data['joc_dashboards']['rateqcurrent'] = array(
    'title' => t('Rates Q Current'),
    'help' => t('Display current value for quarterly rates.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_rateqcurrent',
    ),
  );

  $data['joc_dashboards']['rateqmostrecent'] = array(
    'title' => t('Rates Q Most Recent'),
    'help' => t('Display most recent value for quarterly rates.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_rateqmostrecent',
    ),
  );

  $data['joc_dashboards']['rateqyearago'] = array(
    'title' => t('Rates Q Year Ago'),
    'help' => t('Display 1 year ago value for quarterly rates.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_rateqyearago',
    ),
  );

  $data['joc_dashboards']['rentqcurrent'] = array(
    'title' => t('Rent Q Current'),
    'help' => t('Display current value for quarterly rent.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_rentqcurrent',
    ),
  );

  $data['joc_dashboards']['rentqyearago'] = array(
    'title' => t('Rent Q Year Ago'),
    'help' => t('Display 1 year ago value for quarterly rent.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_rentqyearago',
    ),
  );

  $data['joc_dashboards']['rentaverageytd'] = array(
    'title' => t('Rent Q Average YTD'),
    'help' => t('Display Average YTD value for quarterly rent.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_rentaverageytd',
    ),
  );

  $data['joc_dashboards']['t9volumecurrent'] = array(
    'title' => t('T9 Volume Current'),
    'help' => t('Display current value for Q volumes'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t9volumecurrent',
    ),
  );

  $data['joc_dashboards']['t9volumemostrecent'] = array(
    'title' => t('T9 Volume Most Recent'),
    'help' => t('Display most recent value for Q volumes'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t9volumemostrecent',
    ),
  );

  $data['joc_dashboards']['t9volumeyearago'] = array(
    'title' => t('T9 Volume Year Ago'),
    'help' => t('Display year to date value for Q volumes.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t9volumeyearago',
    ),
  );

  $data['joc_dashboards']['t10tradevaluecurrent'] = array(
    'title' => t('T10 Trade Value Current'),
    'help' => t('Display current value for montly trade values.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t10tradevaluecurrent',
    ),
  );

  $data['joc_dashboards']['t10tradevaluemostcurrent'] = array(
    'title' => t('T10 Trade Value Most Current'),
    'help' => t('Display most current value for montly trade values.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t10tradevaluemostcurrent',
    ),
  );

  $data['joc_dashboards']['t10tradevalueyearago'] = array(
    'title' => t('T10 Trade Value Year Ago'),
    'help' => t('Display 1 year ago value for montly trade values.'),
    'field' => array(
      'handler' => 'joc_dashboards_handler_t10tradevalueyearago',
    ),
  );

  return $data;
}
