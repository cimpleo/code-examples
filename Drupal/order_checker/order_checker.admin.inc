<?php

function order_checker_form_settings($form, $form_state) {
  $state_config = variable_get('order_checker_state_config', array(
    1 => '205,251,256,334,938',
    2 => '907',
    3 => '684',
    4 => '480,520,602,623,928',
    5 => '479,501,870',
    6 => '',
    7 => '',
    8 => '',
    9 => '',
    10 => '',
    11 => '',
    12 => '209,213,310,323,408,415,424,442,510,530,559,562,619,626,650,657,661,707,714,747,760,805,818,831,858,909,916,925,949,951',
    13 => '303,719,720,970',
    14 => '203,475,860',
    15 => '302',
    16 => '202,240',
    17 => '',
    18 => '239,305,321,352,386,407,561,727,754,772,786,813,850,863,904,941,954',
    19 => '229,404,470,478,678,706,762,770,912',
    20 => '671',
    21 => '808',
    22 => '208',
    23 => '217,224,309,312,331,618,630,708,773,779,815,847,872',
    24 => '219,260,317,574,765,812',
    25 => '319,515,563,641,712',
    26 => '316,620,785,913',
    27 => '270,502,606,859',
    28 => '225,318,337,504,985',
    29 => '207',
    30 => '',
    31 => '240,301,410,443,202,571',
    32 => '339,351,413,508,617,774,781,857,978',
    33 => '231,248,269,313,517,586,616,734,810,906,947,989',
    34 => '218,320,507,612,651,763,952',
    35 => '228,601,662,769',
    36 => '314,417,573,636,660,816',
    37 => '406',
    38 => '308,402',
    39 => '702,775',
    40 => '603',
    41 => '201,551,609,732,848,856,862,908,973,646,347,917,212,718',
    42 => '505,575',
    43 => '212,315,347,516,518,585,607,631,646,716,718,845,914,917,929',
    44 => '252,336,704,828,910,919,980',
    45 => '701',
    46 => '',
    47 => '216,234,330,419,440,513,567,614,740,937',
    48 => '405,539,580,918',
    49 => '458,503,541,971',
    50 => '',
    51 => '215,267,412,484,570,610,717,724,814,878',
    52 => '787,939',
    53 => '401',
    54 => '803,843,864',
    55 => '605',
    56 => '423,615,731,865,901,931',
    57 => '210,214,254,281,325,361,409,430,432,469,512,682,713,806,817,830,832,903,915,936,940,956,972,979',
    58 => '385,435,801',
    59 => '802',
    60 => '340',
    61 => '276,434,540,571,703,757,804,202,240',
    62 => '206,253,360,425,509,202,240',
    63 => '304,681',
    64 => '262,414,608,715,920',
    65 => '307',
  ));

  $form['state'] = array(
    '#tree' => TRUE
  );

  $zone_list = order_checker_get_zone_list();

  foreach ($zone_list as $zone_id => $zone_name) {
    $form['state'][$zone_id] = array(
      '#type' => 'textfield',
      '#title' => $zone_name,
      '#default_value' => isset($state_config[$zone_id]) ? $state_config[$zone_id] : ''
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

function order_checker_form_settings_submit(&$form, &$form_state) {
  variable_set('order_checker_state_config', $form_state['values']['state']);
  drupal_set_message('Data saved');
}