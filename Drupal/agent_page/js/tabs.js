(function ($) {
    Drupal.behaviors.tabAgent = {
        attach: function (context, settings) {
            function getUrlVars() {
	    var vars = {};
	    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	        vars[key] = value;
	    });
	    return vars;
	}
            var $tabs=$("#tabs").tabs();
            var agent = getUrlVars()["agent"];
            if (typeof agent !=="undefined"){
                 $tabs.tabs("option", "active", 2);   
            }
            $('.properties').click(function(){  
                 $tabs.tabs("option", "active", 2);                       
            }); 
              $('.links .office').click(function(){ 
                 
                $tabs.tabs("option", "active", 1);                       
            }); 
            
        }
    };
})(jQuery);