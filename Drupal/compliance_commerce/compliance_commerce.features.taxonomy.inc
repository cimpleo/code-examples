<?php
/**
 * @file
 * compliance_commerce.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function compliance_commerce_taxonomy_default_vocabularies() {
  return array(
    'service' => array(
      'name' => 'Service',
      'machine_name' => 'service',
      'description' => 'Taxonomy to accompany the new services and plans.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
