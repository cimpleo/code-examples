<?php
/**
 * @file
 * compliance_commerce.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function compliance_commerce_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'list';
  $ds_view_mode->label = 'List';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['list'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'product_related';
  $ds_view_mode->label = 'Product Related';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['product_related'] = $ds_view_mode;

  return $export;
}
