<?php

/**
 * Example license type.
 */
class ComplianceCommerceLicense extends CommerceLicenseBase  {

  /**
   * Implements CommerceLicenseInterface::accessDetails().
   */
  public function accessDetails() {
    return $this->type;
  }

}
