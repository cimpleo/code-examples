<?php
/**
 * @file
 * compliance_commerce.blocks.inc
 */

/**
 * Implements hook_block_info().
 */
function compliance_commerce_block_info() {
  // Detail header for Plan and Service Pages.
  $blocks['commerce_detail_header'] = array(
    'info' => t('Detail Header'),
    'cache' => DRUPAL_CACHE_PER_PAGE
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function compliance_commerce_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 'commerce_detail_header':
      $block['subject'] = t('Detail Header');
      $block['title'] = '<none>';
      $block['content'] = compliance_commerce_detail_header();
      break;
  }
  return $block;
}

/**
 * Implements hook_block_view_alter().
 */
function compliance_commerce_block_view_alter(&$data, $block) {
  // Remove the Uber shopping cart block.
  if ($block->delta == 25) {
    $data['content'] = NULL;
  }
}

/**
 * [compliance_commerce_detail_header description]
 *
 * @return [type] [description]
 */
function compliance_commerce_detail_header() {
  // Get the working entity.
  $entity = menu_get_object();
  $wrapper = entity_metadata_wrapper('node', $entity);

  // Add the add to cart for based off plans and services.
  switch ($entity->type) {
    case 'training':
      $output = compliance_commerce_detail_header_training($entity, $wrapper);
      return $output;
      break;
    case 'plan':
      $field = 'field_plan';
      $category = 'plan';
      break;
    default:
      $field = 'field_product';
      $category = 'field_service_category';
      break;

  }

    // Acquire the selling price.
  $price = empty($wrapper->{$field}->commerce_price) ? array()
    : $wrapper->{$field}->commerce_price->value();

  // Render and customize the add to car form.
  $addtocart = field_view_field('node', $entity, $field, array('label' => 'hidden'));

  // Alter the submit button.
  $addtocart[0]['submit']['#value'] = t('Buy Now');

  // Get the attached product.
  $product = empty($wrapper->{$field}) ? array() : $wrapper->{$field}->value();

  // Format the amount.
  $amount = commerce_currency_format($price['amount'], $price['currency_code'], $product);

  // Attach the category icon.
  $category = empty($wrapper->{$category}) ? array() : $wrapper->{$category}->value();
  $category_name = empty($category) ? NULL : $category->name;

  $renderedImage = '';
  $type = 'taxonomy_term'; // For products

  // If we're looking at a plan or if the product has no category.
  if (empty($category)) {
    $category = $entity;
    $type = 'node';
  }

  $field_image = field_get_items($type, $category, 'field_image');

  // Get the unix timestamp subscription limit.
  $limit = empty($wrapper->{$field}->commerce_license_duration) ? '1 Year'
    : $wrapper->{$field}->commerce_license_duration->value();

  $limit = commerce_license_duration_from_timestamp($limit);
  $length = commerce_license_duration_units();
  $duration = str_replace('s', '', $length[end($limit)]);

  $subscription_limit = reset($limit) . ' ' . $duration;

  if (!empty($field_image)) {
    $image = field_view_value(
      $type, $category, 'field_image', $field_image[0],
      array('type' => 'image',
            'settings' => array(
              'image_style' => 'icon', //place your image style here
              ),
          )
    );
  }

  $renderedImage = empty($image) ? '' : render($image);

  // We need to exclude that hyphen if we don't have a category_name.
  $title_markup = $renderedImage . '<h1>' . drupal_get_title() . '</h1>';

  $output = array(
    'left' => array(
      '#type' => 'container',
      'title' => array(
        '#type' => 'markup',
        '#markup' =>  $title_markup,
        '#weight' => 0,
      ),
      'body' => array(
        field_view_field('node', $entity, 'body', array('label' => 'hidden')),
        '#weight' => 2,
      ),
      '#attributes' => array('class' => array(
        'content-left', drupal_html_class((empty($category_name) ? drupal_get_title() : $category_name)),
      )),
    ),
    'right' => array(
      '#type' => 'container',
      'amount' => array(
        '#type' => 'markup',
        '#markup' => '<div class="amount">' . $amount . '</div>',
      ),
      'subscription' => array(
        '#type' => 'markup',
        '#markup' => '<div class="subscription">' . $subscription_limit . ' Subscription</div>',
      ),
      'product' => array(
        $addtocart,
        '#weight' => 4,
      ),
      '#attributes' => array('class' => array('cart-left')),
    ),
  );

  // Alter the output if the price is zero.
  if (empty($price['amount']) || $price['amount'] == '0.00') {
    unset($output['right']['amount'], $output['right']['subscription']);
    if (!empty($entity->field_link)) {
      $contact_us = field_view_field('node', $entity, 'field_link', array('label' => 'hidden'));
      $output['right']['product'] = $contact_us;
    }
  }

  return $output;
}

/**
 * Training content block generator.
 */
function compliance_commerce_detail_header_training($entity, $wrapper) {

  // Acquire the selling price.
  $price = empty($wrapper->field_price) ? array()
    : $wrapper->field_price;


  // Catch the Entity Metadata Wrapper exception if the value is null
  try {
    $amount = commerce_currency_format($price->amount->value(), $price->currency_code->value(), $entity);
  } catch (EntityMetadataWrapperException $exc) {
    $amount = '$0.00';
  }

  $subscription = empty($wrapper->field_training_subtype) ? array()
    : $wrapper->field_training_subtype->value();

  $location_date = empty($wrapper->field_location_date) ? array()
    : $wrapper->field_location_date->value();

  $addtocart = field_view_field('node', $entity, 'field_link', array('label' => 'hidden'));

  $output = array(
    'left' => array(
      '#type' => 'container',
      'title' => array(
        '#type' => 'markup',
        '#markup' =>  '<h1>' . drupal_get_title() . '</h1>',
        '#weight' => 0,
      ),
      'location_date' => array(
        '#markup' => '<div class="location-date">' . $location_date . '</div>',
        '#weight' => 1,
      ),
      'body' => array(
        field_view_field('node', $entity, 'body', array('label' => 'hidden')),
        '#weight' => 2,
      ),
      '#attributes' => array('class' => array(
        'content-left', drupal_html_class('training'),
      )),
    ),
    'right' => array(
      '#type' => 'container',
      'product' => array(
        $addtocart,
        '#weight' => 4,
      ),
      '#attributes' => array('class' => array('cart-left')),
    ),
  );

  return $output;
}

/**
 * Implements hook_preprocess_node()
 * Add Image as title prefix to product teasers.
 */
function compliance_commerce_preprocess_node(&$variables) {
  // Plans.
  if (!empty($variables['type']) && $variables['type'] == 'plan' &&
     $variables['view_mode'] == 'teaser') {

    $field_image = field_get_items('node', $variables['elements']['#node'], 'field_image');
    if (!empty($field_image)) {
      $image = field_view_value(
        'node', $variables['elements']['#node'], 'field_image', $field_image[0],
        array('type' => 'image',
              'settings' => array(
                'image_style' => 'icon', //place your image style here
                ),
            )
      );
    }

    $renderedImage = empty($image) ? '' : render($image);
    $variables['title_prefix']['image'] = array('#markup' => $renderedImage);
  }

  // Product/Service.
  if (!empty($variables['type']) && $variables['type'] == 'service'
    && !empty($variables['content']['compliance_commerce_restricted'])) {
    $variables['node_url'] = $variables['content']['compliance_commerce_restricted']['#markup'];
  }

  // Remove link so it does not get rendered on the page.
  unset($variables['content']['compliance_commerce_restricted']);
}
