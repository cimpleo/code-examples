<?php
/**
 * @file
 * compliance_commerce.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function compliance_commerce_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'commerce_detail_header';
  $context->description = 'Allow for a header to be added that does not conform to the 2 column layout.';
  $context->tag = 'FTLF';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'plan' => 'plan',
        'service' => 'service',
        'training' => 'training',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'compliance_commerce-commerce_detail_header' => array(
          'module' => 'compliance_commerce',
          'delta' => 'commerce_detail_header',
          'region' => 'preface_first',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Allow for a header to be added that does not conform to the 2 column layout.');
  t('FTLF');
  $export['commerce_detail_header'] = $context;

  return $export;
}
