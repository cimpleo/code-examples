<?php

class estate_view_actualprice_handler extends views_handler_field_numeric {
  
  function query() {
    
    $table = $this->query->ensure_table('ff_webserverdat');
    $sql = "SELECT COALESCE(Miete, Kaufpreis) FROM {ff_webserverdat} ffw WHERE ffw.DSN = ff_webserverdat.DSN";

    $this->query->add_field('', "($sql)", 'actualprice');
    $this->field_alias = 'actualprice';
  }

  function render($values) {
    $price = $values->actualprice;
    
    if ($price) {
        return $price;
    } else {
        return parent::render($values);
    }
  }
}
