<?php

/**
 * @file
 * Views integration for the custom view module
 */

/**
 * Implements hook_views_data()
 *
 */
function estate_view_views_data() 
{
    /*
     * IMPEX_USER TABLE
     */
    $data['impex_user']['table']['group'] = t('Agent fields');
    $data['impex_user']['table']['base'] = array(
      'field' => 'ip_usr_id',
      'title' => t('Agents'),
      'help' => t('ip_usr_id'),
      'database' => 'external',
      'weight' => -10,
    );
       
    
    $data['impex_user']['ip_usr_id'] = array(
      'title' => t('Agent id'),
      'help' => t('ip_usr_id'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['table']['join'] = array(
        'impex_anbieter' => array(
            'left_table' => 'impex_anbieter',
            'left_field' => 'anbieter', 
            'field' => 'ip_usr_email',
        ) 
    ); 
    
    $data['impex_user']['ip_usr_email'] = array(
      'title' => t('Agent email'),
      'help' => t('ip user email'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
      'relationship' => array(
            'base' => 'impex_anbieter',
            'base field' => 'email',
            'field' => 'ip_usr_email',
            'handler' => 'views_handler_relationship',        
        ),  
    );
    
    $data['impex_user']['ip_usr_firstname'] = array(
      'title' => t('Agent firstname'),
      'help' => t('ip_usr_firstname'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_lastname'] = array(
      'title' => t('Agent lastname'),
      'help' => t('ip_usr_lastname'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_photoUrl'] = array(
      'title' => t('Agent photo URL'),
      'help' => t('ip_usr_photoUrl'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_mobilePhone'] = array(
      'title' => t('Agent mobilePhone'),
      'help' => t('ip_usr_mobilePhone'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_vorname'] = array(
      'title' => t('Agent photo Vorname'),
      'help' => t('ip_usr_vorname'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_nachname'] = array(
      'title' => t('Agent photo Nachname'),
      'help' => t('ip_usr_nachname'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_description'] = array(
      'title' => t('Agent description'),
      'help' => t('ip_usr_description'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_fachwissen'] = array(
      'title' => t('Agent photo Fachwissen'),
      'help' => t('ip_usr_fachwissen'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_user']['ip_usr_referenzen'] = array(
      'title' => t('Agent photo Referenzen'),
      'help' => t('ip_usr_referenzen'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    $data['impex_user']['ip_usr_funktion'] = array(
      'title' => t('Agent function'),
      'help' => t('ip_usr_funktion'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    /*
     * IMPEX_ANBIETER TABLE
     */
    $data['impex_anbieter']['table']['group'] = t('Impex anbieter');
    $data['impex_anbieter']['table']['base'] = array(
      'field' => 'anbieter',
      'title' => t('Anbieter'),
      'help' => t('Anbieter'),
      'database' => 'external',
      'weight' => -10,
    );
       
    $data['impex_anbieter']['buero'] = array(
      'title' => t('impex anbieter buero'),
      'help' => t('impex_anbieter buero'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );   
    
    $data['impex_anbieter']['pictureUrl'] = array(
      'title' => t('impex anbieter picture'),
      'help' => t('impex_anbieter picture'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );   
    
    
    $data['impex_anbieter']['address_postalCode'] = array(
      'title' => t('impex anbieter address_postalCode'),
      'help' => t('impex_anbieter address_postalCode'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    
    $data['impex_anbieter']['address_city'] = array(
      'title' => t('impex anbieter address_city'),
      'help' => t('impex_anbieter address_city'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    $data['impex_anbieter']['address_street'] = array(
      'title' => t('impex anbieter address_street'),
      'help' => t('Anbieter address street'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
     
    $data['impex_anbieter']['phone'] = array(
      'title' => t('impex_anbieter phone'),
      'help' => t('Anbieter phone'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    $data['impex_anbieter']['fax'] = array(
      'title' => t('impex_anbieter fax'),
      'help' => t('Anbieter fax'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    
    $data['impex_anbieter']['anbieter'] = array(
      'title' => t('impex_anbieter anbieter'),
      'help' => t('Anbieter anbieter'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    ); 
    /*
     * FF_WEBSERVERPIC TABLE
     */
    
    $data['ff_webserverpic']['table']['group'] = t('Object images');
    $data['ff_webserverpic']['table']['base'] = array(
      'field' => 'URL',
      'title' => t('Object images'),
      'help' => t('Object images'),
      'database' => 'external',
      'weight' => -10,
    );
       
    $data['ff_webserverpic']['URL'] = array(
      'title' => t('URL'),
      'help' => t('Databasepic URL'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );   
    
    $data['ff_webserverpic']['DSN'] = array(
      'title' => t('DSN'),
      'help' => t('Databasepic DSN'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );   
    
    /*
     * FF_WEBSERVERDAT TABLE
     */
    
    $data['ff_webserverdat']['table']['group'] = t('Object estate');
    $data['ff_webserverdat']['table']['base'] = array(
      'field' => 'DSN',
      'title' => t('Estate from external db'),
      'help' => t('Estate from external db'),
      'database' => 'external',
      'weight' => -10,
    );
    
    
     $data['ff_webserverdat']['table']['join'] = array(
        'pic' => array(
            'left_table' => 'ff_webserverpic',
            'left_field' => 'URL', 
            'field' => 'DSN',
        ),        
        'impex_user' => array(
            'left_table' => 'impex_user',
            'left_field' => 'ip_usr_id', 
            'field' => 'Webserver___EMail_P',
        ),
        'impex_anbieter' => array(
            'left_table' => 'impex_anbieter',
            'left_field' => 'anbieter', 
            'field' => 'Webserver___EMail_P',
        ) 
    );
     
    $data['ff_webserverdat']['DSN'] = array(
      'title' => t('DSN'),
      'help' => t('Estate DSN'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
      'relationship' => array(
            'base' => 'ff_webserverpic',
            'base field' => 'DSN',
            'field' => 'DSN',
            'handler' => 'views_handler_relationship',          
        ),  
    );

    
    $data['ff_webserverdat']['Webserver___EMail_P'] = array(
      'title' => t('Email'),
      'help' => t('Impex user table by email'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
      'relationship' => array(
            'base' => 'impex_user',
            'base field' => 'ip_usr_email',
            'field' => 'Webserver___EMail_P',
            'handler' => 'views_handler_relationship', 
        ),  
    );
    
    
    $data['ff_webserverdat']['Land_PLZ_Ort'] = array(
      'title' => t('Land PLZ Ort'),
      'help' => t('Estate Land PLZ Ort'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___Vor_Nachname'] = array(
      'title' => t('Vor Nachname'),
      'help' => t('Estate Vor Nachname'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___Mobil'] = array(
      'title' => t('Mobil'),
      'help' => t('Estate Mobil'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___Buero'] = array(
      'title' => t('Buero'),
      'help' => t('Estate Buero'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___Strasse'] = array(
      'title' => t('Strasse'),
      'help' => t('Estate Strasse'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___PLZ_Ort'] = array(
      'title' => t('PLZ Ort'),
      'help' => t('Estate PLZ Ort'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___Telefon'] = array(
      'title' => t('Webserver Telefon'),
      'help' => t('Estate Telefon'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Webserver___Telefax'] = array(
      'title' => t('Webserver Telefax'),
      'help' => t('Estate Telefax'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['INTERN'] = array(
      'title' => t('INTERN'),
      'help' => t('Estate INTERN'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Kaufpreis'] = array(
      'title' => t('Kaufpreis'),
      'help' => t('Estate Kaufpreis'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_numeric',
      ),
    );
    
    
   $data['ff_webserverdat']['Miete'] = array(
      'title' => t('Miete'),
      'help' => t('Estate Miete'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_numeric',
      ),
    ); 
    
   
   $data['ff_webserverdat']['actualprice'] = array(
      'title' => t('Actual price'),
      'help' => t('Actual price'),
      'field' => array(
         'handler' => 'estate_view_actualprice_handler',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'estate_view_actualprice_filter',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_numeric',
      ),
    ); 
    
    $data['ff_webserverdat']['Zimmer'] = array(
      'title' => t('Zimmer'),
      'help' => t('Estate Zimmer'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['Wohnflaeche'] = array(
      'title' => t('Wohnflaeche'),
      'help' => t('Estate Wohnflaeche'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['TEXTOBJEKT'] = array(
      'title' => t('TEXTOBJEKT'),
      'help' => t('Estate TEXTOBJEKT'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['TEXTAUSSTATTUNG'] = array(
      'title' => t('TEXTAUSSTATTUNG'),
      'help' => t('Estate TEXTAUSSTATTUNG'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['TEXTLAGE'] = array(
      'title' => t('TEXTLAGE'),
      'help' => t('Estate TEXTLAGE'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );  
    
    
    $data['ff_webserverdat']['Objektart'] = array(
      'title' => t('Objektart'),
      'help' => t('Estate Objektart'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );  
    
    
    $data['ff_webserverdat']['LNG'] = array(
      'title' => t('LNG'),
      'help' => t('Estate LNG'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    
    
    $data['ff_webserverdat']['LAT'] = array(
      'title' => t('LAT'),
      'help' => t('Estate LAT'),
      'field' => array(
         'handler' => 'views_handler_field',
         'click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
    

     
    return $data;
}

function estate_view_views_handlers() {
    return array(
        'handlers' => array(
            'estate_view_actualprice_handler' => array(
                'parent' => 'views_handler_field_numeric',
                'path' => drupal_get_path('module', 'estate_view'),
            ),
            
            'estate_view_actualprice_filter' => array(
                'parent' => 'views_handler_field_numeric',
                'path' => drupal_get_path('module', 'estate_view'),
            ),
        ),
    );
}