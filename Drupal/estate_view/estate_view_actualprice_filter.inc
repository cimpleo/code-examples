<?php

class estate_view_actualprice_filter extends views_handler_filter_numeric {
  
  function op_between($field) {
      if ($this->operator == 'between') {
          $this->query->add_having($this->options['group'], 'actualprice', array($this->value['min'], $this->value['max']), 'BETWEEN');     
      } else {
          $this->query->add_having($this->options['group'], db_or()->condition('actualprice', $this->value['min'], '<=')->condition('actualprice', $this->value['max'], '>='));
      }
  }
}
