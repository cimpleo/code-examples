<?php
class order_ip_log_views_handler_field_log_date extends views_handler_field_date
{
  public function query() {
    $this->field_alias = $this->query->add_field(
      NULL,
      "unix_timestamp(date_format(from_unixtime(log_created), '%Y-%m-%d 00:00:00'))",
      'log_date'
    );
  }

}