<?php

class views_handler_field_ban_actions extends views_handler_field {
  public function query() {

  }

  public function render($values) {
    $options = array(
      'query' => drupal_get_destination()
    );
    if ($values->ip_banned) {
      return l('Unban', 'admin/order_ip_log/unban/' . $values->order_ip_log_ip, $options);
    }
    else {
      return l('Ban', 'admin/order_ip_log/ban/' . $values->order_ip_log_ip, $options);
    }

  }
}