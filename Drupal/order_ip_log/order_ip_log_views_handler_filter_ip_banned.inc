<?php
class order_ip_log_views_handler_filter_ip_banned extends views_handler_filter_boolean_operator
{
  public function query() {
    $sql = '(SELECT COUNT(ip) FROM order_banned_ip WHERE ip = order_ip_log.ip)';
    if ($this->value != ''){
      if ($this->value == 1){
        $sql .= ' > 0';
      }
      else{
        $sql .= ' = 0';
      }
      $this->query->add_where_expression($this->options['group'], $sql);
    }
  }

}