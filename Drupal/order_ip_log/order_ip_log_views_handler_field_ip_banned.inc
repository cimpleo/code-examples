<?php
class order_ip_log_views_handler_field_ip_banned extends views_handler_field_boolean
{
  function init(&$view, &$options) {
    parent::init($view, $options);
    $module_path = drupal_get_path('module', 'order_ip_log');
    $default_formats = array(
      'yes-no' => array(t('Yes'), t('No')),
      'true-false' => array(t('True'), t('False')),
      'on-off' => array(t('On'), t('Off')),
      'enabled-disabled' => array(t('Enabled'), t('Disabled')),
      'boolean' => array(1, 0),
      'unicode-yes-no' => array('✔', '✖'),
      'unbaned-baned' => array('<img src="/' . $module_path . '/images/ip_banned.png">', ''),
    );
    $output_formats = isset($this->definition['output formats']) ? $this->definition['output formats'] : array();
    $custom_format = array('custom' => array(t('Custom')));
    $this->formats = array_merge($default_formats, $output_formats, $custom_format);
  }

  public function query(){
    $this->field_alias = $this->query->add_field(
      NULL,
      "IF((SELECT count(ip) FROM order_banned_ip WHERE order_ip_log.ip=ip) > 0, TRUE, FALSE)",
      'ip_banned'
    );
  }

}