<?php
class order_ip_log_views_handler_field_ip_count extends views_handler_field_numeric
{
  public function query() {
    $this->field_alias = $this->query->add_field(
      NULL,
      "(SELECT COUNT(*) FROM {order_ip_log} oil WHERE oil.ip = order_ip_log.ip AND from_unixtime(oil.log_created) BETWEEN date_format(from_unixtime(order_ip_log.log_created), '%Y-%m-%d 00:00:00') and date_format(from_unixtime(order_ip_log.log_created), '%Y-%m-%d 23:59:59'))",
      'ip_count'
    );
  }

}