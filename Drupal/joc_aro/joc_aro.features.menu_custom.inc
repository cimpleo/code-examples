<?php
/**
 * @file
 * joc_aro.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function joc_aro_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-header-menu.
  $menus['menu-header-menu'] = array(
    'menu_name' => 'menu-header-menu',
    'title' => 'Header Menu',
    'description' => 'Header menu',
  );
  // Exported menu: menu-taxonomy-category-menu.
  $menus['menu-taxonomy-category-menu'] = array(
    'menu_name' => 'menu-taxonomy-category-menu',
    'title' => 'Taxonomy Category Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Header Menu');
  t('Header menu');
  t('Taxonomy Category Menu');


  return $menus;
}
