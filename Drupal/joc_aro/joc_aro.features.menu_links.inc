<?php
/**
 * @file
 * joc_aro.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function joc_aro_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-header-menu_annual-review--outlook-2015:aro
  $menu_links['menu-header-menu_annual-review--outlook-2015:aro'] = array(
    'menu_name' => 'menu-header-menu',
    'link_path' => 'aro',
    'router_path' => 'aro',
    'link_title' => 'Annual Review & Outlook 2015',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-header-menu_annual-review--outlook-2015:aro',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -38,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Annual Review & Outlook 2015');


  return $menu_links;
}
