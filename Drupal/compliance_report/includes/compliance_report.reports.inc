<?php
/**
 * @file
 * compliance_report.reports.inc
 */

/**
 * [compliance_report description]
 *
 * @return [type] [description]
 */
function compliance_report_reports() {
  $headers = array(
    'filename' => 'Filename',
    'destination' => 'Destination',
    'type' => 'Type',
    'filesize' => 'Filesize',
    'exported' => 'Exported',
    'operations' => 'Operations',
  );

  $form['header'] = array(
    '#type' =>'container',
  );

  // $form['header']['filters'] = compliance_report_order_filters($parameters);

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => array(),
    '#empty' => '<p>No results found</p>',
  );

  // Load data.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'file')
    ->propertyCondition('filename', '%_report.csv', 'LIKE')
    ->propertyOrderBy('timestamp', 'DESC')
    ->range(0, 30)
    ->addMetaData('account', user_load(1)); // Run the query as user 1.

  $results = $query->execute();
  if (empty($results)) {
    return $form;
  }

  // Get all file ids.
  $ids = array_keys($results['file']);

  $files = entity_load('file', $ids);
  foreach ($files as $file) {
    $type = str_replace('.csv', '', explode('-', $file->filename));
    $type = end($type);

    $operations = array(
      'download' => l('download', file_create_url($file->uri)),
      'delete' => l('delete', 'admin/content/ftlf/' . $file->fid . '/delete'),
    );

    $timestamp = explode('-', $file->filename);
    $timestamp = reset($timestamp);

    $form['table']['#rows'][] = array(
      'filename' => $file->filename,
      'destination' => $file->uri,
      'type' => ucwords(str_replace('_', ' ', $type)),
      'filesize' => format_size($file->filesize),
      'exported' => format_date($timestamp, 'custom', 'F d, Y - h:ia'),
      'operations' => implode(' ', $operations),
    );
  }

  // Output the pager to the page.
  $form['pager'] = array(
    '#type' => 'markup',
    '#markup' => theme('pager'),
  );

  return $form;
}

/**
 * [compliance_report_delete_report description]
 *
 * @param  [type] $file [description]
 *
 * @return [type]       [description]
 */
function compliance_report_delete_report($file) {
  file_delete($file);
  drupal_goto('admin/content/ftlf/reports');
}
