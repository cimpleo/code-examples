<?php
/**
 * @file
 * compliance_report.batch.inc
 */

/**
 * Export and Create CSV File.
 * @param string $parameters
 *   Raw String with Query Resutls.
 *
 * @param array $context
 *   Array of Context regarding then batch process.
 *
 */
function compliance_report_export_csv($file, $entities, $arguments, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $arguments['total'];
    $context['results']['file'] = $file;
  }

  // Prepare File.
  $file = fopen($file, 'a');

  // Write Entity Information to CSV file.
  foreach ($entities as $entity) {
    if (!empty($context['results'])
      && array_key_exists($entity->order_id, $context['results'])) {
      continue;
    }

    $context['message'] = t('Exporting ' . $arguments['entity_count'] . ' orders in
      groups of ' . $arguments['total'] . '.');

    $output = compliance_report_build_data(array($entity), 'commerce_order', $arguments['headers'], TRUE);
    foreach ($output as $line) {
      fputcsv($file, $line);
    }

    $context['results'][$entity->order_id] = $entity;
  }

  $context['sandbox']['progress']++;
  $entities = array();

  fclose($file);

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Export Finish Callback.
 * @param boolen $success
 *   A boolean indicating whether the batch has completed successfully.
 *
 * @param array $results
 *   The value set in $context['results'] by callback_batch_operation().
 *
 * @param arary $operations
 *   If $success is FALSE, contains the operations that remained unprocessed.
 *
 */
function compliance_report_finished_callback($success, $results, $operations) {
  global $user;

  // Redirect on Success.
  if (!$success) {
    drupal_set_message(t('There was an error with the export please try again.'), 'error');
    return drupal_goto('admin/content/ftlf/order-report');
  }

  // Create File Object.
  $file = new stdClass();
  $file->uid = $user->uid;
  $file->filename = basename($results['file']);
  $file->uri = $results['file'];
  $file->filemime = file_get_mimetype($results['file']);
  $file->status = FILE_STATUS_PERMANENT;

  // Move File and add to Database.
  $exports = 'private://reports/order';
  $location = str_replace('public://', 'private://', $results['file']);
  file_prepare_directory($exports, FILE_CREATE_DIRECTORY);
  file_move($file, $location, FILE_EXISTS_REPLACE);

  $download = l(basename($results['file']), file_create_url($location));

  drupal_set_message(t('Export ' . $download . ' was successful.'), 'status');
  return drupal_goto('admin/content/ftlf/reports');
}
