<?php
/**
 * @file
 * compliance_report.page.inc
 */

/**
 * [compliance_report description]
 *
 * @return [type] [description]
 */
function compliance_report_orders_form($form, &$form_state, $entity_type = 'commerce_order', $export = FALSE) {
  // Working Parameters
  $parameters = drupal_get_query_parameters();

  $form_state['entity_type'] = $entity_type;

  $headers = array(
    'unique' => 'OID/PID',
    'order' => 'Order',
    'uid' => 'UID',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'company' => 'Company',
    'email' => 'Email',
    'street_address' => 'Street Address',
    'city' => 'City',
    'state' => 'State',
    'zip' => 'Zip',
    'products' => 'Products',
    'coupons' => 'Coupons',
    'paid' => 'Paid',
    'license' => 'License State',
    'activated' => 'Activated',
    'expiration' => 'Expiration',
  );

  $form_state['headers'] = $headers;

  $form['header'] = array(
    '#type' =>'container',
  );

  $form['header']['filters'] = compliance_report_order_filters($parameters);

  $form['header']['export'] = array(
    '#type' => 'submit',
    '#value' => t('Export Data'),
  );

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $headers,
    '#rows' => array(),
    '#empty' => '<p>No results found</p>'
  );

  // Load data.
  $query = compliance_report_orders_query($entity_type, $parameters, $export);
  $results = $query->execute();
  if (empty($results)) {
    return $form;
  }

  // Get all order ids.
  $ids = array_keys($results[$entity_type]);

  $orders = entity_load($entity_type, $ids);
  $form['table']['#rows'] = compliance_report_build_data($orders, $entity_type, $headers, $export);

  // Output the pager to the page.
  $form['pager'] = array(
    '#type' => 'markup',
    '#markup' => theme('pager'),
  );

  return $form;
}

/**
 * [compliance_report_orders_form_submit description]
 *
 * @param  [type] $form        [description]
 * @param  [type] &$form_state [description]
 *
 * @return [type]              [description]
 */
function compliance_report_orders_form_submit($form, &$form_state) {
  $parameters = $form_state['values']['filters'];
  unset($parameters['filter']);

  // Execute Query.
  $query = compliance_report_orders_query($form_state['entity_type'], $parameters, TRUE);
  $results = $query->execute();
  if (empty($results)) {
    drupal_set_message('Sorry you can not export a report without any data.', 'warning');
    return $form;
  }

  // Total number of resuts.
  $total = count($query->ordered_results);

  // File Information.
  $filename = time() . '-order_report.csv';
  $destination = 'public://reports/order';

  // Prepare Destination.
  file_unmanaged_delete_recursive($destination);
  file_prepare_directory($destination, FILE_CREATE_DIRECTORY);
  $file = $destination .'/' . $filename;

  $headers = $form_state['headers'];

  // Write Headers to File.
  $csv = fopen($file, 'a');
  fputcsv($csv, $headers);
  fclose($csv);

  // Get all order ids.
  $ids = array_keys($results['commerce_order']);
  $orders = entity_load('commerce_order', $ids);

  // Content.
  $operations = array();
  $chunks = array_chunk($orders, '10');
  foreach ($chunks as $entities) {
    // Batch Arguments.
    $arguments = array(
      'headers' => $headers,
      'total' => count($chunks),
      'entity_count' => $total,
    );

    $operations[] = array('compliance_report_export_csv', array($file, $entities, $arguments));
  }

  // Execute Batch Process
  $batch = array(
    'title' => t('Exporting'),
    'operations' => $operations,
    'finished' => 'compliance_report_finished_callback',
    'file' => drupal_get_path('module', 'compliance_report') . '/includes/compliance_report.batch.inc',
  );

  batch_set($batch);
}

/**
 * [compliance_report_order_filters description]
 *
 * @return [type] [description]
 */
function compliance_report_order_filters($parameters) {
  $filters = array(
    '#type' => 'container',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );

  $filters['account'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#autocomplete_path' => 'compliance-report/autocomplete/name',
    '#default_value' => empty($parameters['account']) ? NULL : $parameters['account'],
  );

  $filters['company'] = array(
    '#type' => 'textfield',
    '#title' => t('Company'),
    '#autocomplete_path' => 'compliance-report/autocomplete/company',
    '#default_value' => empty($parameters['company']) ? NULL : $parameters['company'],
  );

  $default_state = array('all' => 'All');
  $states = commerce_license_status_options_list();
  // $filters['state'] = array(
  //   '#type' => 'select',
  //   '#title' => t('State'),
  //   '#options' => array_merge($default_state, $states),
  //   '#default_value' => empty($parameters['state']) ? 'all' : $parameters['state'],
  // );

  $filters['filter'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
    '#submit' => array('compliance_report_order_filters_submit'),
  );

  $filters['reset'] = array(
    '#type' => 'link',
    '#title' => t('Reset'),
    '#href' => request_path(),
  );

  return $filters;
}

/**
 * [compliance_report_order_filters_submit description]
 *
 * @param  [type] $form        [description]
 * @param  [type] &$form_state [description]
 *
 * @return [type]              [description]
 */
function compliance_report_order_filters_submit($form, &$form_state) {
  $filters = $form_state['values']['filters'];
  unset($filters['filter']);
  $form_state['redirect'] = array(request_path(), array(
    'query' => array_filter($filters),
  ));
}

/**
 * [compliance_report_orders_query description]
 *
 * @param  [type]  $entity_type [description]
 * @param  boolean $export      [description]
 *
 * @return [type]               [description]
 */
function compliance_report_orders_query($entity_type, $parameters = array(), $export = FALSE) {
  // Use the EFQ as the backbone to reports.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type)
    ->propertyCondition('status', 'completed', '=')
    ->propertyOrderBy('created', 'DESC')
    ->addTag('compliance_report_orders')
    ->addMetaData('account', user_load(1)); // Run the query as user 1.

  // Filter by User Account [Contact Name].
  if (!empty($parameters['account'])) {
    $query->propertyCondition('uid', $parameters['account'], '=');
  }

  // Filter by User Account [Company Name].
  if (!empty($parameters['company'])) {
    $query->propertyCondition('uid', $parameters['company'], '=');
  }

  // Filter by License State.
  if (!empty($parameters['state']) && $parameters['state'] !== 'all') {
    // $query->propertyCondition('uid', $parameters['state'], '=');
  }

  // Use pager only if this is not being exported to csv.
  if (empty($export)) {
    $query->pager(30);
  }

  // Gather the query results.
  return $query;
}
