<?php
function tms_settings_form($form, $form_state) {
    $form['tms_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('tms_username', '')
  );

  $form['tms_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('tms_password', '')
  );

  $form['tms_connection_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Connection timeout'),
    '#default_value' => variable_get('tms_connection_timeout', 30)
  );

  $form['tms_reup_codes'] = array(
    '#type' => 'textfield',
    '#title' => t('Error code for reup'),
    '#default_value' => variable_get('tms_reup_codes', '')
  );

  $form['tms_terminal_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Terminal ID'),
    '#default_value' => variable_get('tms_terminal_id', '')
  );

  $form['tms_enable_canceled_orders'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable cancellation of orders',
    '#default_value' => variable_get('tms_enable_canceled_orders', 0)
  );

  $form['tms_test_mode'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable test mode',
    '#default_value' => variable_get('tms_test_mode', 0)
  );

  return system_settings_form($form);
}

function tms_error_settings_form($form, $form_state){
  $tms_error_config = variable_get('tms_error_config', array());

  $form['#tree'] = TRUE;

  $form['tms_canceled_comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Default comment for canceled orders'),
    '#default_value' => variable_get('tms_canceled_comment', '')
  );

  $form['errors'] = array(
    '#tree' => TRUE
  );

  foreach ($tms_error_config as $error_code => $error_comment) {
    $form['errors'][$error_code] = array(
      '#tree' => TRUE
    );
    $form['errors'][$error_code]['comment'] = array(
      '#type' => 'textarea',
      '#default_value' => $error_comment
    );

  }

  $form['new_error_code'] = array(
    '#type' => 'textfield',
    '#default_value' => ''
  );

  $form['new_error_comment'] = array(
    '#type' => 'textarea',
    '#default_value' => ''
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

function tms_error_settings_form_submit(&$form, &$form_state) {
  $tms_error_config =  array();
  variable_set('tms_canceled_comment', $form_state['values']['tms_canceled_comment']);
  if (isset($form_state['values']['errors'])) {
    foreach ($form_state['values']['errors'] as $error_code => $comment_array) {
      $tms_error_config[$error_code] = $comment_array['comment'];
    }
  }

  if (!empty($form_state['values']['new_error_code'])) {
    $tms_error_config[$form_state['values']['new_error_code']] = $form_state['values']['new_error_comment'];
  }
  variable_set('tms_error_config', $tms_error_config);
  drupal_set_message('Data saved');
}

function tms_error_settings_delete($error_code) {
  $tms_error_config = variable_get('tms_error_config', array());
  unset($tms_error_config[$error_code]);
  variable_set('tms_error_config', $tms_error_config);
  drupal_set_message('Error code deleted');
  drupal_goto('admin/config/system/tms/errors_settings');
}