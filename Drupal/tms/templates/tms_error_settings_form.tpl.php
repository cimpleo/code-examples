<?php
$head = array('Error code', 'Comment text', '');
$rows = array();
foreach (element_children($form['errors']) as $id) {
  $rows[] = array(
    $id,
    drupal_render($form['errors'][$id]['comment']),
    l('Delete', "admin/config/system/tms/errors_settings/delete/{$id}")
  );
}
$rows[] = array(
  drupal_render($form['new_error_code']),
  drupal_render($form['new_error_comment']),
  ''
);

$output = theme('table', array(
  'header' => $head,
  'rows' => $rows,
));

$output .= drupal_render_children($form);

echo $output;