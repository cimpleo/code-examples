<?php

function nc_custom_privatemsg_tread_delete($form, $form_state, $thread) {
  global $user;
  $form['thread_id'] = array(
    '#type' => 'value',
    '#value' => $thread['thread_id'],
  );
  $form['delete_destination'] = array(
    '#type' => 'value',
    '#value' => 'messages/'.$user->uid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete this converation?'),
    isset($_GET['destination']) ? $_GET['destination'] : 'messages/'.$user->uid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function nc_custom_privatemsg_tread_delete_submit($form, &$form_state) {
  global $user;
  $account = clone $user;

  if ($form_state['values']['confirm']) {
    $query = privatemsg_sql_messages(array($form_state['values']['thread_id']), $account);
    $results = $query->execute()->fetchAll();

    $mids = array();
    foreach ($results as $result) {
      $mids[] = $result->mid;
    }

    if ($mids) {
      $delete_value = REQUEST_TIME;
      $update = db_update('pm_index')
        ->fields(array('deleted' => $delete_value))
        ->condition('mid', $mids);
      if ($account) {
        $update
          ->condition('recipient', $account->uid)
          ->condition('type', array('user', 'hidden'));
      }
      $update->execute();
    }
  }
  $form_state['redirect'] = $form_state['values']['delete_destination'];
}

function _nc_custom_translate() {
  global $user;
  $account = clone $user;
  $node = node_load(59);
  return
    t('Hello !username, welcome back. It important now that you leave a feedback on the profile page of Cicerones you met. Were they on time? Have they a good knowledge of the city? Do you suggest their tour to other Travelers? Let us know, please.', array('!username' => theme('username', array('account' => $account))))
    .t('Hello !username, you just received a feedback from !sender. View your profile to read the feedback. Please note that the feedback can be deleted only by the user who issued the feedback and by the staff that can intervene only in the case where the feedback is offensive. If the feedback  cause you troubles we suggest you to de-active the profile until our intervention.', array('!username' => theme('username', array('account' => $account)), '!sender' => theme('username', array('account' => $account))))
    .t('Hello !username, you just received a comment on your tour !tour.  Please note that the comments can be deleted only by the user who issued the feedback and by the staff that can intervene only if the comment is offensive. If the comment cause you troubles we suggest you to de-active the profile until our intervention.', array('!username' => theme('username', array('account' => $account)), '!tour' => l($node->title, 'node/'.$node->nid, array('html' => TRUE))))
    .t('Hello !username, welcome to the great community of Travelers and Cicerones. You can visit beautiful places, meet people from all around the world, learn languages ​​and even earn some money! We invite you to our Facebook page as well that reserves big surprises: !fb_link', array('!username' => theme('username', array('account' => $account)), '!fb_link' => l('www.facebook.com/nativecicerone', 'http://www.facebook.com/nativecicerone', array('external' => TRUE))));
}