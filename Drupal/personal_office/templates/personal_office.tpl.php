<?php
    $agent  = $variables['agent'];
    $menu = $variables['agent_menu'];
    global $user;
    
    if (empty($menu)) {
        $agent_menu = '<div class="empty-agent-menu">'.t('There is no any pages yet.').'</div>';
    } else {
        $agent_menu = '<ul class="agent-personal-menu">';
    
        foreach ($menu as $key => $node) {
            $agent_menu.= '<li class="agent-menu-link"><a id="'.$key.'" href="/'.$node['link'].'">'.$node['title'].'</a></li>';
        }
        
        $agent_menu.='</ul>';
    }
    
?>
<?php if ($user->uid == $agent->uid) : ?>
<div class="add-content-link">
    <a href="/node/add/agent-article"><?php print t('Add new page'); ?></a>
</div>
<?php endif; ?>

<div class="agent-menu">
    <?php print_r($agent_menu); ?>
</div>    

<div class="estate-objects">
    <label><?php print t('Also you can see our estate objects here:'); ?></label><a href="/<?php print variable_get('personal_office_url', '');?><?php print $agent->mail;?>"><?php print t('Our estate objects')?></a>
</div>

<div class="agent-user-info">
    
    <div class="name">
        <label><?php print t('Name:'); ?></label><?php print_r($agent->name); ?>
    </div>
    
    <div class="mail">
        <label><?php print t('Email:'); ?></label><?php print_r($agent->mail); ?>
    </div>
    
</div>