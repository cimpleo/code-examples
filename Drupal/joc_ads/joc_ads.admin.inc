<?php

function joc_ads_admin_config() {
  global $dS; global $dC;
  $multisiteTitle = $dC[$dS]['joc_ws_authentication_type'];

  $form = array();
  $form['joc_ads_main'] = array(
    '#type' => 'fieldset',
    '#title' => t($multisiteTitle.' Google Ads'),
    '#description' => '',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['joc_ads_main']['google_ad_zones'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main menu ad zones'),
    '#description' => 'Each main menu link has a Google ad zone assigned to it. <b>Each menu item below with no ad zone assigned will automatically use the \'Home zone\' from <a href="/admin/config/joc/joc_ads/settings">Settings</a> tab.</b><br />For any link that\'s not present in the main menu which needs to be used, please use <a href="/admin/config/joc/joc_ads/google_custom_adzones">Custom Ad Zones</a>.',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

/***********************************************/
/* Code for main menu ad zones */
/***********************************************/
  $menu_tree_all_data = menu_tree_all_data('main-menu');
  $menu_tree_keys = array_keys($menu_tree_all_data);
  for ($i = 0; $i < count($menu_tree_keys); $i++) {
    # Check first if the menu link is enabled
    if($menu_tree_all_data[$menu_tree_keys[$i]]['link']['hidden'] == 0) {
      # Then initialize the top menu item fieldset.
      # Top menu item means any menu link at the top of the hierarchy (e.g.
      # Special topics, Ports, Maritime, etc.)
      $field_set_title = '';
      # Check if it has child menu items
      if($menu_tree_all_data[$menu_tree_keys[$i]]['link']['has_children'] == 1) {
        # Get the link title
        $field_set_title = str_replace(' ', '_', strtolower($menu_tree_all_data[$menu_tree_keys[$i]]['link']['link_title']));
        # Get the link path alias
        $field_set_title_path_alias = drupal_get_path_alias($menu_tree_all_data[$menu_tree_keys[$i]]['link']['link_path']);
        # Build the fieldset
        $form['joc_ads_main']['google_ad_zones'][$field_set_title] = array(
          '#type' => 'fieldset',
          '#title' => t(str_replace('_', ' ', $field_set_title)),
          '#description' => 'Please set ad_zones in the following form: '. str_replace('/', '-', $field_set_title_path_alias),
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        );
        # Add the text field
        $form['joc_ads_main']['google_ad_zones'][$field_set_title]['joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $field_set_title_path_alias)] = array(
          '#type' => 'textfield',
          '#title' => ucwords(str_replace('_', ' ', $field_set_title)),
          '#description' => '',
          '#default_value' => variable_get('joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $field_set_title_path_alias), str_replace('/', '-', $field_set_title_path_alias)),
        );

        # Add the child text fields
        # Get children keys
        $children_keys = array_keys($menu_tree_all_data[$menu_tree_keys[$i]]['below']);
        # For each child key
        for ($j = 0; $j < count($children_keys); $j++) {
          # Check if it is enabled for display
          if($menu_tree_all_data[$menu_tree_keys[$i]]['below'][$children_keys[$j]]['link']['hidden'] == 0) {
            # Get the link title
            $child_title = str_replace(' ', '_', strtolower($menu_tree_all_data[$menu_tree_keys[$i]]['below'][$children_keys[$j]]['link']['link_title']));
            # Get the path alias
            $child_title_path_alias = drupal_get_path_alias($menu_tree_all_data[$menu_tree_keys[$i]]['below'][$children_keys[$j]]['link']['link_path']);
            # Build the field
            $form['joc_ads_main']['google_ad_zones'][$field_set_title]['joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $child_title_path_alias)] = array(
              '#type' => 'textfield',
              '#title' => ucwords(str_replace('_', ' ', $child_title)),
              '#description' => 'E.g.: ' . str_replace('/', '-', $child_title_path_alias),
              '#prefix' => '<div class="col-xs-12 col-sm-6 col-md-4">',
              '#suffix' => '</div>',
              '#default_value' => variable_get('joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $child_title_path_alias), ''),
            );
          }
        }

      } else { # If the current menu item being processed does NOT have child menu items under it
        # Grab the text field
        $text_field = str_replace(' ', '_', strtolower($menu_tree_all_data[$menu_tree_keys[$i]]['link']['link_title']));
        # Grab the URL path alias
        $text_field_alias = drupal_get_path_alias($menu_tree_all_data[$menu_tree_keys[$i]]['link']['link_path']);

        # Parse the /special-topics page
        if($text_field_alias == 'special-topics') {
          $html_obj = new simple_html_dom();
          $html_obj->load_file($GLOBALS['base_url'] . base_path() . 'special-topics');

          # Build the fieldset
          $form['joc_ads_main']['google_ad_zones']['special_topics'] = array(
            '#type' => 'fieldset',
            '#title' => t('Special topics'),
            '#description' => 'Please set ad_zones in the following form: special-topics, special-topics-top-100-us-exporters, etc.',
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );
          $form['joc_ads_main']['google_ad_zones']['special_topics']['joc_ads_ad_zone_special_topics'] = array(
            '#type' => 'textfield',
            '#title' => 'Special topics',
            '#description' => 'E.g.: special-topics',
            '#default_value' => variable_get('joc_ads_ad_zone_special_topics', 'special-topics'),
          );

          # Build the /special-topics child links ad zones
          foreach($html_obj->find('.view-special-topics-terms') as $view) {
            foreach($view->find('.views-field-name') as $field) {
              foreach($field->find('.field-content') as $content) {
                foreach($content->find('a') as $ahref) {
                  # Get the link and trim the slash at the end
                  $link = ltrim($ahref->href, '/');
                  # Get the text value
                  $value = $ahref->plaintext;
                  # Replace slashes with dashes for a complete ad zone (e.g. # special-topics-top-25-truckload-carriers)
                  $st_zone = str_replace('/', '-', $link);
                  $form['joc_ads_main']['google_ad_zones']['special_topics']['joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $st_zone)] = array(
                    '#type' => 'textfield',
                    '#title' => $value,
                    '#description' => 'E.g.: '.$st_zone,
                    '#prefix' => '<div class="col-xs-12 col-sm-6 col-md-4">',
                    '#suffix' => '</div>',
                    '#default_value' => variable_get('joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $st_zone), ''),
                  );
                }
              }
            }
          }
        } elseif ($text_field_alias == '<front>') {
          # Don't display anything here!
          # If the menu item is the home page, don't display it. The home page ad zone is already set in the Settings page.
        } else { # No parsing needed for the menu item page being loaded so, just build the field for it
          $form['joc_ads_main']['google_ad_zones']['joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $text_field_alias)] = array(
            '#type' => 'textfield',
            '#title' => ucwords(str_replace('_', ' ', $text_field)),
            '#description' => 'Please set ad_zones in the following form: '.$text_field_alias,
            '#prefix' => '<div class="col-xs-12 col-sm-6 col-md-4">',
            '#suffix' => '</div>',
            '#default_value' => variable_get('joc_ads_ad_zone_' . str_replace(array('-', '/'), '_', $text_field_alias), str_replace('/', '-', $text_field_alias)),
          );
        }
      }
    }
  }
/*==============================================*/
/* End Of Code for main menu ad zones */
/*==============================================*/

  $form['joc_ads_interstitial'] = array(
    '#type' => 'fieldset',
    '#title' => t('Interstitial Ads'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['joc_ads_interstitial']['joc_ads_interstitial_dtl'] = array(
    '#type' => 'textfield',
    '#title' => 'Interstitial Days to Live',
    '#description' => 'Lifetime in days of cookie that prevents interstitial ad page from showing.',
    '#default_value' => variable_get('joc_ads_interstitial_dtl', '3'),
  );

  $form['joc_ads_interstitial']['joc_ads_interstitial_time'] = array(
    '#type' => 'textfield',
    '#title' => 'Interstitial Time',
    '#description' => 'Number of seconds to show the interstitial ad.',
    '#default_value' => variable_get('joc_ads_interstitial_time', '15'),
  );

  return system_settings_form($form);
}
