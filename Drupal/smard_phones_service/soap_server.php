<?php

define('NUSOAP_NAME_SPACE', 'soap_phone_service');
$nu_soap_path = 'lib/nusoap.php';
if (!file_exists($nu_soap_path)) {
    die('An error has occurred initializing the soap server.');
}
require_once ($nu_soap_path);
$server = new nusoap_server();
$server->configureWSDL(NUSOAP_NAME_SPACE, 'urn:' . NUSOAP_NAME_SPACE);
$server->wsdl->addComplexType(
        'person', 'complexType', 'struct', 'all', '', array(
    'firstName' => array(
        'name' => 'firstName',
        'type' => 'xsd:string',
    ),
    'lastName' => array(
        'name' => 'lastName',
        'type' => 'xsd:string',
    ),
        )
);

$server->register('ping', array('phone_id' => 'xsd:string'), array('return' => 'xsd:string'), 'uri:' . NUSOAP_NAME_SPACE, 'uri:' . NUSOAP_NAME_SPACE . '#ping', 'rpc', 'encoded', 'Ping'
);
$server->register('phone_block', array('phone_id' => 'xsd:integer'), array('return' => 'xsd:integer'), 'uri:' . NUSOAP_NAME_SPACE, 'uri:' . NUSOAP_NAME_SPACE . '#phone_block', 'rpc', 'encoded', 'Phone block function'
);
$server->register('phone_unblock', array('phone_id' => 'xsd:integer'), array('return' => 'xsd:integer'), 'uri:' . NUSOAP_NAME_SPACE, 'uri:' . NUSOAP_NAME_SPACE . '#phone_unblock', 'rpc', 'encoded', 'Phone unblock function'
);
$server->register('phone_delete', array('phone_id' => 'xsd:integer'), array('return' => 'xsd:integer'), 'uri:' . NUSOAP_NAME_SPACE, 'uri:' . NUSOAP_NAME_SPACE . '#phone_delete', 'rpc', 'encoded', 'Phone delete function'
);
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);

function ping($stringparam) {
    $testString = $stringparam;
    return json_encode(array('status' => true, 'data' => 'Rcvd: ' . $testString . '(' . strlen($testString) . ')'));
}

function init() {
    define('DRUPAL_ROOT', $_SERVER['DOCUMENT_ROOT']);
    require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    require_once('smard_phones_service.module');
}

function phone_block($phone_id) {
    init();
    return smard_phones_service_phone_block($phone_id);
}

function phone_unblock($phone_id) {
    init();
    return smard_phones_service_phone_unblock($phone_id);
}

function phone_delete($phone_id) {
    init();
    return smard_phones_service_phone_delete($phone_id);
}

?>