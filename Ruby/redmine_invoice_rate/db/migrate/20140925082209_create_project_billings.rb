class CreateProjectBillings < ActiveRecord::Migration
  def change
    create_table :project_billings do |t|
		t.belongs_to 	:user,  index: true
    	t.belongs_to	:project,  index: true
    	t.integer		:price
    	t.timestamps
    end
  end
end
