class CreateProjectUserPrices < ActiveRecord::Migration
  def change
    create_table :project_user_prices do |t|
    	t.integer 	:user_id
    	t.integer	:price
    end
  end
end
