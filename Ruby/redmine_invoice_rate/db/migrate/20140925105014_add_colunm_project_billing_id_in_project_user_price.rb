class AddColunmProjectBillingIdInProjectUserPrice < ActiveRecord::Migration
  def up
  	add_column :project_user_prices, :project_billing_id, :integer
  end

  def down
  	remove_column :project_user_prices, :project_billing_id
  end
end
