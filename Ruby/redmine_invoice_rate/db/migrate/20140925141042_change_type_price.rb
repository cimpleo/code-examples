class ChangeTypePrice < ActiveRecord::Migration
  def up
  	change_column :project_user_prices, :price ,:float
  	remove_column :project_billings, :prices
  	remove_column :project_billings, :user_id
  end

  def down
  end
end
