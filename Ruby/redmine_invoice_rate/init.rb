Redmine::Plugin.register :redmine_invoice_rate do
  name 'Developer Rate plugin'
  author 'CimpleO Software'
  description 'Simple plugin for developer rates management'
  version '0.0.1'
  url ''
  author_url 'http://www.cimpleo.com'

  requires_redmine_plugin :redmine_contacts_invoices, :version_or_higher => '3.1.4'

  project_module :redmine_invoice_rate do
    permission :redmine_invoice_rate, :project_billings => :index
    permission :redmine_invoice_rate, :project_invoice => :new
  end

  menu :project_menu, :redmine_invoice_rate, 
    { :controller => 'project_billings', :action => 'index' }, 
    :caption => 'Rates', 
    :param => :project_id,
    :if => Proc.new{
      User.current.admin?
    }
    
  
end


if Redmine::Plugin.installed?(:redmine_contacts_invoices)
  require 'redmine_invoice_rate'
end
