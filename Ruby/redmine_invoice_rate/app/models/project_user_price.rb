class ProjectUserPrice < ActiveRecord::Base
  unloadable

  belongs_to :project_billing 
  belongs_to :user, :class_name => "User", :foreign_key => "user_id"

  delegate :billing, :to => :project_billing

  def self.get_user_on_project user_id, project_billing_id
  	self.where("user_id = ? and project_billing_id = ?", user_id, project_billing_id).first
  end
  
end
