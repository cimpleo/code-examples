json.array!(@project_billings) do |project_billing|
  json.extract! project_billing, :id
  json.url project_billing_url(project_billing, format: :json)
end
