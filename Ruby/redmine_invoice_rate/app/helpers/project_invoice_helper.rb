module ProjectInvoiceHelper

  include RedmineInvoices::InvoiceReports
  include Redmine::I18n

  def collection_invoice_statuses_rate_plugin
    Invoice::STATUSES.map{|k, v| [l(v), k]}
  end

  def collection_invoice_statuses_for_select_rate_plugin
    collection_invoice_statuses_rate_plugin.select{|s| s[1] != Invoice::PAID_INVOICE}
  end

  def assignable_users
    collection_invoice_statuses_rate_plugin.select{|s| s[1] != Invoice::PAID_INVOICE}
  end

  def collection_for_discount_types_select_rate_plugin
    [:percent, :amount].each_with_index.collect{|l, index| [l("label_invoice_#{l.to_s}".to_sym), index]}
  end

  def invoice_lang_options_for_select_rate_plugin(has_blank=true)
    (has_blank ? [["(auto)", ""]] : []) +
        RedmineInvoices.available_locales.collect{|lang| [ ll(lang.to_s, :general_lang_name), lang.to_s]}.sort{|x,y| x.last <=> y.last }
  end

  def link_to_remove_invoice_fields_rate_plugin(name, f, options={})
    f.hidden_field(:_destroy) + link_to_function(name, "remove_invoice_fields(this)", options)
  end

  def link_to_add_invoice_fields_rate_plugin(name, f, association, options={})
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_invoice_fields(this, '#{association}', '#{escape_javascript(fields)}')", options={})
  end

end
