class ProjectBillingsController < ApplicationController
  unloadable

  before_filter :auth_index, :only => [:index]

  before_filter :set_project_billing, only: [:show, :edit, :update, :destroy]

  before_filter :auth_edit, :only => [:show, :edit, :update, :destroy]

  before_filter :find_project_by_project_id, :only => [:index, :new, :create]
  # GET /project_billings
  # GET /project_billings.json
  def index
    @project_billings = ProjectBilling.find_by_project_id @project.id
  end

  # GET /project_billings/1
  # GET /project_billings/1.json
  def show
  end

  # GET /project_billings/new
  def new
    if @project.users.length > 0
      @project_billing = ProjectBilling.new
      @project_billing.project = @project
      @project.users.each do |user|
        user_price = ProjectUserPrice.new(:user => user, :price => 0)
        @project_billing.project_user_prices << user_price
      end
    else
      flash[:notice] = "No users on the project"
      redirect_to_home
    end
  end

  # GET /project_billings/1/edit
  def edit
  end

  # POST /project_billings
  # POST /project_billings.json
  def create
    @project_billing = ProjectBilling.new
    @project_billing.project = @project
    project_billing_params.each do |user_id, price|
      user_price = ProjectUserPrice.new(:user => User.find(user_id), :price => price || 0.0)
      if user_price.save
        @project_billing.project_user_prices << user_price
      end
    end
    if @project_billing.save
      flash[:notice] = "save succeful"
      redirect_to_home
    else
      flash[:notice] = "save failed"
    end
  end

  # PATCH/PUT /project_billings/1
  # PATCH/PUT /project_billings/1.json
  def update
    project_billing_params.each do |user_id, price|
      if user = ProjectUserPrice.get_user_on_project(user_id, @project_billing.id)
        unless user.price == price
          if user.update_attributes(:price => price)
            flash[:notice] = "save succeful"
          else
            flash[:notice] = "save failed "
          end
        end
      else
        user_price = ProjectUserPrice.new(:user => User.find(user_id), :price => price || 0.0)
        if user_price.save
          @project_billing.project_user_prices << user_price
        end
      end
    end
    if @project_billing.save
      redirect_to_home
    end 
  end

  # DELETE /project_billings/1
  # DELETE /project_billings/1.json
  def destroy
    @project_billing.destroy
    respond_to do |format|
      format.html { redirect_to project_billings_url, notice: 'Project billing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def auth_index
      unless @project
        @project = Project.find(params[:project_id])
      end
      check_permission @project   
    end

    def auth_edit
      if @project_billing
        check_permission @project_billing.project
      else
        render_404
      end
    end

    def check_permission project
      if !User.current.allowed_to?(:redmine_invoice_rate, project)
        render_404
      end
    end

    def redirect_to_home
      redirect_to project_billings_path(:project_id => @project_billing.project.identifier)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_project_billing
      @project_billing = ProjectBilling.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_billing_params
      params[:project_billing]
    end
end
