(function () {
    'use strict';

    angular
        .module('hub')
        .factory('authService', authService);

    function authService($q, $http, $rootScope, PermRoleStore, PermPermissionStore) {
        var that = {
            login: login,
            logout: logout,
            currentUser: currentUser
        };

        PermRoleStore.defineManyRoles({
            "ANONYMOUS": isAnonymous,
            "USER": hasRole,
            "ADMIN": hasRole,
            "ADMINISTRATOR": hasRole,
            "MANAGER": hasRole,
            "SECURITY_ENGINEER": hasRole,
            "DEVELOPER": hasRole
        });


        var _currentUser = undefined;
        return that;

        function isAnonymous() {
            if (angular.isDefined(_currentUser)) {
                return _currentUser.roles.indexOf("ANONYMOUS") != -1;
            }
            return true;
        }

        function hasRole(role) {
            if (!angular.isDefined(_currentUser)) return false;

            return _currentUser.roles.indexOf(role) != -1;
        }

        function currentUser(force) {
            var deferred = $q.defer();

            if (force === true) _currentUser = undefined;

            if (angular.isDefined(_currentUser)) {
                deferred.resolve(_currentUser);
                return deferred.promise;
            }
            $http.get($rootScope.apiBaseUrl + '/auth/user',
                {ignoreErrors: true, withCredentials: true})
                .success(function (data) {
                    _currentUser = data;
                    $rootScope.isAuthorized = !isAnonymous();
                    if(!isAnonymous()){
                        $rootScope.currentUser = _currentUser;
                    } else {
                        $rootScope.currentUser = undefined;
                    }
                    deferred.resolve(_currentUser);
                })
                .error(function () {
                    _currentUser = undefined;
                    deferred.resolve(_currentUser);
                });

            return deferred.promise;
        }

        function login(username, password) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: $rootScope.apiBaseUrl + '/auth/login',
                headers: {"Content-Type": "application/x-www-form-urlencoded", "X-Login-Ajax-Call": 'true'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                withCredentials: true,
                data: {username: username, password: password}
            })
                .then(function (response) {
                    that.currentUser(true).then(function (user) {
                        deferred.resolve("OK");
                    });
            }, function (response) {
                    if(response.status === 401){
                        deferred.reject("Bad credentials");
                    } else {
                        deferred.reject("Server return: " + response.status + ": " + response.statusText);
                    }
            });

            return deferred.promise;
        }

        function logout() {
            var deferred = $q.defer();
            $http.post($rootScope.apiBaseUrl + '/auth/logout')
                .then(function () {
                    $rootScope.isAuthorized = false;
                    $rootScope.currentUser = undefined;
                    that.currentUser(true).then(function (user) {
                        deferred.resolve(user);
                    });
                })
                .catch(function(){
                    deferred.reject();
                });
            return deferred.promise;
        }
    }
})();